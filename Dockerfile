FROM node:8-alpine as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
ARG configuration=production
RUN npm run build -- --engine=browser  


FROM nginx:1.13.3-alpine
# COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/www /usr/share/nginx/html
