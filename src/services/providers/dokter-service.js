var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { addConsultationQuery, addInformConsentApprovalQuery, addReferenceQuery, addTaggingOperationQuery, checkICD10, doctorByNodokQuery, ExamineByIdQuery, examineDataExchangeQuery, ExamineLast1Query, getConsultantNodok1Query, getConsultantTujuanQuery, getCTByDate, getDataDoctor, getDataObat, getIcd9, getIcdx, getInformationPatientQuery, getInformConsentQuery, getIntegratedNote1Query, getIntegratedNoteByIdQuery, getIntegratedNoteNodok1Query, getIntegratedNoteQuery, getPatientQuery, getReviewByIdQuery, InformConsentAnestesiQuery, InformConsentInfoQuery, IntegratedNoteQuery, kidReviewByMedrecQuery, notificationPatientQuery, PatientByIdQuery, setExaminationFlagQuery, updateConsultationAnswerQuery, updateDiagExamQuery, updateFlagInformQuery, updateFlagReviewQuery, updateKidFlagReviewQuery, QueueTodayQuery, reviewDoctorAssessmentQuery, setQueueFlagQuery, informTypeByNameQuery, MasterDataLabQuery, MasterDataRadioQuery, addLabQuery, addRadiologyQuery, addFisioQuery, } from './../gqlquery/gqlquery';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
var DokterService = /** @class */ (function () {
    function DokterService(apollo) {
        this.apollo = apollo;
    }
    DokterService.prototype.getNotificationPatientSub = function (nodok) {
        return this.apollo.subscribe({
            query: examineDataExchangeQuery,
            variables: {
                nodok: nodok,
                flag: 0
            }
        });
    };
    DokterService.prototype.getListPatient = function (nodok) {
        return this.apollo.watchQuery({
            query: QueueTodayQuery,
            variables: {
                nodok: nodok
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.getDataDoctor = function (nodok) {
        return this.apollo.watchQuery({
            query: doctorByNodokQuery,
            variables: {
                nodok: nodok
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.getReviewById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getReviewByIdQuery,
            variables: {
                no_medrec: no_medrec
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.getKidReviewByMedrec = function (no_medrec) {
        return this.apollo.watchQuery({
            query: kidReviewByMedrecQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    DokterService.prototype.getDataPatient = function (nodok) {
        return this.apollo.watchQuery({
            query: getPatientQuery,
            variables: {
                nodok: nodok
            }
        });
    };
    DokterService.prototype.notificationDataPatient = function (nodok) {
        return this.apollo.watchQuery({
            query: notificationPatientQuery,
            variables: {
                nodok: nodok,
            },
            fetchPolicy: 'network-only',
            notifyOnNetworkStatusChange: true
        });
    };
    DokterService.prototype.getDataICDX = function (penyakit, offset, limit) {
        return this.apollo.watchQuery({
            query: getIcdx,
            variables: {
                penyakit: penyakit,
                offset: offset,
                limit: limit
            }
        });
    };
    DokterService.prototype.getDataICD9 = function (deskripsi, offset, limit) {
        return this.apollo.watchQuery({
            query: getIcd9,
            variables: {
                deskripsi: deskripsi,
                offset: offset,
                limit: limit
            }
        });
    };
    DokterService.prototype.getDataObat = function (nama, offset, limit) {
        return this.apollo.watchQuery({
            query: getDataObat,
            variables: {
                nama: nama,
                offset: offset,
                limit: limit
            }
        });
    };
    DokterService.prototype.getDataPatientById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: ExamineByIdQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    DokterService.prototype.getDataIntegratedNote = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getIntegratedNoteQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    DokterService.prototype.getDataInformationPatient = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getInformationPatientQuery,
            variables: {
                no_medrec: no_medrec
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.getDataIntegratedNoteById = function (id_ct) {
        return this.apollo.watchQuery({
            query: getIntegratedNoteByIdQuery,
            variables: {
                id_ct: id_ct
            }
        });
    };
    DokterService.prototype.getDataIntegratedNote1 = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getIntegratedNote1Query,
            variables: {
                no_medrec: no_medrec
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.getDataIntegratedNoteNodok1 = function (nodok, no_medrec) {
        return this.apollo.watchQuery({
            query: getIntegratedNoteNodok1Query,
            variables: {
                nodok: nodok,
                no_medrec: no_medrec
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.saveIntegratedNote = function (formarray) {
        console.log(formarray);
        return this.apollo.mutate({
            mutation: IntegratedNoteQuery,
            variables: {
                id_ct: formarray.id_ct,
                no_medrec: formarray.no_medrec,
                nama_pasien: formarray.nama_pasien,
                nodok: formarray.nodok,
                nama_dok: formarray.nama_dok,
                no_periksa: formarray.no_periksa,
                jaminan: formarray.jaminan,
                tgl_lahir: formarray.tgl_lahir,
                umur: formarray.umur,
                kelamin: formarray.kelamin,
                kelas: formarray.kelas,
                tgl_ct: formarray.tgl_ct,
                s_description: formarray.s_description,
                s_stroke: formarray.s_stroke,
                o_description: formarray.o_description,
                o_stroke: formarray.o_stroke,
                a_description: formarray.a_description,
                a_stroke: formarray.a_stroke,
                a_icd9: formarray.a_icd9,
                a_icdx: formarray.a_icdx,
                p_lab_description: formarray.p_lab_description,
                p_lab_stroke: formarray.p_lab_stroke,
                p_rad_description: formarray.p_rad_description,
                p_rad_stroke: formarray.p_rad_stroke,
                p_fisio_description: formarray.p_fisio_description,
                p_fisio_stroke: formarray.p_fisio_stroke,
                p_tindakan_description: formarray.p_tindakan_description,
                p_tindakan_stroke: formarray.p_tindakan_stroke,
                p_obat_description: formarray.p_obat_description,
                p_obat_stroke: formarray.p_obat_stroke,
                p_obat_nama: formarray.p_obat_nama,
                p_obat_alergi: formarray.p_obat_alergi,
                penyakit: formarray.penyakit
            }
        });
    };
    DokterService.prototype.setFlagExamination = function (no_medrec, flag) {
        return this.apollo.mutate({
            mutation: setExaminationFlagQuery,
            variables: {
                no_medrec: no_medrec,
                flag: flag
            }
        });
    };
    DokterService.prototype.updateFlagReview = function (no_medrec, flagKaji) {
        return this.apollo.mutate({
            mutation: updateFlagReviewQuery,
            variables: {
                no_medrec: no_medrec,
                flagKaji: flagKaji
            }
        });
    };
    DokterService.prototype.updateKidFlagReview = function (no_medrec, flagKaji) {
        return this.apollo.mutate({
            mutation: updateKidFlagReviewQuery,
            variables: {
                no_medrec: no_medrec,
                flagKaji: flagKaji
            }
        });
    };
    DokterService.prototype.updateDiagExam = function (no_periksa, diagnosa, diagnosa_stroke, kasus) {
        if (diagnosa == null) {
            diagnosa = "";
        }
        return this.apollo.mutate({
            mutation: updateDiagExamQuery,
            variables: {
                no_periksa: no_periksa,
                diagnosa: diagnosa,
                diagnosa_stroke: diagnosa_stroke,
                kasus: kasus
            }
        });
    };
    DokterService.prototype.saveInformConsentInfo = function (formarray) {
        return this.apollo.mutate({
            mutation: InformConsentInfoQuery,
            variables: {
                no_inform: formarray.no_inform,
                no_medrec: formarray.no_medrec,
                pelaksana_tindakan: formarray.pelaksana_tindakan,
                pemberi_info: formarray.pemberi_info,
                penerima_info: formarray.penerima_info,
                info_diag_kerja: formarray.info_diag_kerja,
                checklist_info_diag_kerja: formarray.checklist_info_diag_kerja,
                dasar_diag: formarray.dasar_diag,
                checklist_dasar_diag: formarray.checklist_dasar_diag,
                tindakan_dokter: formarray.tindakan_dokter,
                checklist_tindakan_dokter: formarray.checklist_tindakan_dokter,
                indikasi_tindakan: formarray.indikasi_tindakan,
                checklist_indikasi_tindakan: formarray.checklist_indikasi_tindakan,
                tatacara: formarray.tatacara,
                checklist_tatacara: formarray.checklist_tatacara,
                tujuan: formarray.tujuan,
                checklist_tujuan: formarray.checklist_tujuan,
                risiko: formarray.risiko,
                checklist_risiko: formarray.checklist_risiko,
                komplikasi: formarray.komplikasi,
                checklist_komplikasi: formarray.checklist_komplikasi,
                prognosis: formarray.prognosis,
                checklist_prognosis: formarray.checklist_prognosis,
                alternatif: formarray.alternatif,
                checklist_alternatif: formarray.checklist_alternatif,
                lain: formarray.lain,
                checklist_lain: formarray.checklist_lain,
                tt_dokter: formarray.tt_dokter,
                tt_pasien: formarray.tt_pasien,
                tt_saksi1: formarray.tt_saksi1,
                tt_saksi2: formarray.tt_saksi2
            }
        });
    };
    DokterService.prototype.saveInformConsentAnestesi = function (formarray) {
        return this.apollo.mutate({
            mutation: InformConsentAnestesiQuery,
            variables: {
                no_inform: formarray.no_inform,
                no_medrec: formarray.no_medrec,
                a_u_tindakan_dokter: formarray.a_u_tindakan_dokter,
                a_u_tatacara: formarray.a_u_tatacara,
                a_u_tujuan: formarray.a_u_tujuan,
                a_u_risiko: formarray.a_u_risiko,
                a_u_komplikasi: formarray.a_u_komplikasi,
                a_r_tindakan_dokter: formarray.a_r_tindakan_dokter,
                a_r_tatacara: formarray.a_r_tatacara,
                a_r_tujuan: formarray.a_r_tujuan,
                a_r_risiko: formarray.a_r_risiko,
                a_r_komplikasi: formarray.a_r_komplikasi,
                a_r_alternatif: formarray.a_r_alternatif,
                a_l_tindakan_dokter: formarray.a_l_tindakan_dokter,
                a_l_tatacara: formarray.a_l_tatacara,
                a_l_tujuan: formarray.a_l_tujuan,
                a_l_risiko: formarray.a_l_risiko,
                a_l_komplikasi: formarray.a_l_komplikasi,
                a_l_alternatif: formarray.a_l_alternatif,
                sedasi_tindakan_dokter: formarray.sedasi_tindakan_dokter,
                sedasi_tatacara: formarray.sedasi_tatacara,
                sedasi_tujuan: formarray.sedasi_tujuan,
                sedasi_risiko: formarray.sedasi_risiko,
                sedasi_komplikasi: formarray.sedasi_komplikasi,
                sedasi_prognosis: formarray.sedasi_prognosis,
                sedasi_lain: formarray.sedasi_lain,
                tt_dokter: formarray.tt_dokter,
                tt_pasien: formarray.tt_pasien,
                tt_saksi1: formarray.tt_saksi1,
                tt_saksi2: formarray.tt_saksi2
            }
        });
    };
    DokterService.prototype.getIntegratedNoteByDateQuery = function (no_medrec, tgl_ct) {
        return this.apollo.watchQuery({
            query: getCTByDate,
            variables: {
                no_medrec: no_medrec,
                tgl_ct: tgl_ct
            }
        });
    };
    DokterService.prototype.getCheckICD10 = function (no_medrec, a_icdx) {
        return this.apollo.watchQuery({
            query: checkICD10,
            variables: {
                no_medrec: no_medrec,
                a_icdx: a_icdx
            }
        });
    };
    DokterService.prototype.getDataDoctorLike = function (nama_dok, offset, limit) {
        return this.apollo.watchQuery({
            query: getDataDoctor,
            variables: {
                nama_dok: nama_dok,
                offset: offset,
                limit: limit
            }
        });
    };
    DokterService.prototype.saveConsultation = function (formarray) {
        return this.apollo.mutate({
            mutation: addConsultationQuery,
            variables: {
                no_konsultasi: formarray.no_konsultasi,
                no_medrec: formarray.no_medrec,
                nodok: formarray.nodok,
                id_ct: formarray.id_ct,
                alasan: formarray.alasan,
                nodok_tujuan: formarray.nodok_tujuan
            }
        });
    };
    DokterService.prototype.getDataConsultationNodok1 = function (no_medrec, nodok) {
        return this.apollo.watchQuery({
            query: getConsultantNodok1Query,
            variables: {
                no_medrec: no_medrec,
                nodok: nodok
            }
        });
    };
    DokterService.prototype.getDataConsultationTujuan = function (nodok_tujuan) {
        return this.apollo.watchQuery({
            query: getConsultantTujuanQuery,
            variables: {
                nodok_tujuan: nodok_tujuan,
            }
        });
    };
    DokterService.prototype.updateConsultataion = function (formarray) {
        return this.apollo.mutate({
            mutation: updateConsultationAnswerQuery,
            variables: {
                no_konsultasi: formarray.no_konsultasi,
                id_ct_jawaban: formarray.id_ct_jawaban,
                jawaban: formarray.jawaban
            }
        });
    };
    DokterService.prototype.getInformConsentQuery = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getInformConsentQuery,
            variables: {
                no_medrec: no_medrec
            },
            fetchPolicy: 'network-only'
        });
    };
    DokterService.prototype.getInformConsent0Query = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getInformConsentQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    DokterService.prototype.updateInformConsentApproval = function (formarray) {
        return this.apollo.mutate({
            mutation: addInformConsentApprovalQuery,
            variables: {
                no_inform: formarray.no_inform,
                nama_ortu: formarray.nama_ortu,
                umur_ortu: formarray.umur_ortu,
                kelamin_ortu: formarray.kelamin_ortu,
                alamat: formarray.alamat,
                ktp: formarray.ktp,
                tgl_inform: formarray.tgl_inform,
                jam_inform: formarray.jam_inform,
                yg_menyatakan: formarray.yg_menyatakan,
                dokter_operator: formarray.dokter_operator,
                saksi1: formarray.saksi1,
                saksi2: formarray.saksi2,
                tindakan_operasi: formarray.tindakan_operasi,
                tipe_tindakan: formarray.tipe_tindakan
            }
        });
    };
    DokterService.prototype.updateTaggingOperation = function (formarray) {
        return this.apollo.mutate({
            mutation: addTaggingOperationQuery,
            variables: {
                no_inform: formarray.no_inform,
                penandaan_operasi: formarray.penandaan_operasi
            }
        });
    };
    DokterService.prototype.updateFlagInform = function (no_inform, flag) {
        return this.apollo.mutate({
            mutation: updateFlagInformQuery,
            variables: {
                no_inform: no_inform,
                flag: flag
            }
        });
    };
    DokterService.prototype.getTypeInform = function (tipe_inform) {
        return this.apollo.watchQuery({
            query: informTypeByNameQuery,
            variables: {
                tipe_inform: tipe_inform
            }
        });
    };
    DokterService.prototype.addReferencePatient = function (formarray) {
        return this.apollo.mutate({
            mutation: addReferenceQuery,
            variables: {
                id_rujukan: formarray.id_rujukan,
                nama_pasien: formarray.nama_pasien,
                no_medrec: formarray.no_medrec,
                umur: formarray.umur,
                kelamin: formarray.kelamin,
                tgl: formarray.tgl,
                nama_dok: formarray.nama_dok,
                jaminan: formarray.jaminan,
                kelas: formarray.kelas,
                tipe_rujukan: formarray.tipe_rujukan,
                diagnosa: formarray.diagnosa,
                indikasi: formarray.indikasi,
                keterangan: formarray.keterangan,
                lab_hematologi: formarray.lab_hematologi,
                lab_urinalisa: formarray.lab_urinalisa,
                lab_faeces: formarray.lab_faeces,
                lab_gula_darah: formarray.lab_gula_darah,
                lab_kimia_darah: formarray.lab_kimia_darah,
                lab_berologi: formarray.lab_berologi,
                lab_mikrobiologi: formarray.lab_mikrobiologi,
                lab_lain: formarray.lab_lain,
                rad_tanpa_kontras: formarray.rad_tanpa_kontras,
                rad_kontras: formarray.rad_kontras,
                fisio_tindakan: formarray.fisio_tindakan
            }
        });
    };
    DokterService.prototype.loadPatientExamination1 = function (nodok) {
        return this.apollo.watchQuery({
            query: ExamineLast1Query,
            variables: {
                nodok: nodok
            },
            fetchPolicy: 'network-only',
            notifyOnNetworkStatusChange: true
        });
    };
    DokterService.prototype.loadPatientExamination = function (nodok) {
        return this.apollo.watchQuery({
            query: getPatientQuery,
            variables: {
                nodok: nodok
            }
        });
    };
    DokterService.prototype.getPatientById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: PatientByIdQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    DokterService.prototype.updateDoctorReview = function (formarray) {
        console.log(formarray);
        return this.apollo.mutate({
            mutation: reviewDoctorAssessmentQuery,
            variables: {
                no_kaji: formarray.no_kaji,
                nama_dok: formarray.nama_dok,
                nodok: formarray.nodok,
                anamnesa_keluhan: formarray.anamnesa_keluhan,
                anamnesa_riwayat_penyakit: formarray.anamnesa_riwayat_penyakit,
                fisik_kepala: formarray.fisik_kepala,
                fisik_leher: formarray.fisik_leher,
                fisik_mulut: formarray.fisik_mulut,
                fisik_paru: formarray.fisik_paru,
                fisik_genitalia: formarray.fisik_genitalia,
                fisik_mata: formarray.fisik_mata,
                fisik_tht: formarray.fisik_tht,
                fisik_jantung: formarray.fisik_jantung,
                fisik_abdomen: formarray.fisik_abdomen,
                fisik_ekstremitas: formarray.fisik_ekstremitas,
                status_gizi: formarray.status_gizi,
                pemeriksaan_gigi: formarray.pemeriksaan_gigi,
                pemeriksaan_gigi_ket: formarray.pemeriksaan_gigi_ket,
                status_lokalis: formarray.status_lokalis,
                pemeriksaan_penunjang: formarray.pemeriksaan_penunjang,
                diagnosis: formarray.diagnosis,
                rencana: formarray.rencana,
                dirujuk: formarray.dirujuk,
                dirujuk_lainnya: formarray.dirujuk_lainnya,
                tt_dokter: formarray.tt_dokter
            }
        });
    };
    DokterService.prototype.setFlagPatient = function (no_medrec, flag) {
        return this.apollo.mutate({
            mutation: setQueueFlagQuery,
            variables: {
                no_medrec: no_medrec,
                flag: flag
            }
        });
    };
    DokterService.prototype.loadDataMasterLab = function () {
        return this.apollo.watchQuery({
            query: MasterDataLabQuery
        });
    };
    DokterService.prototype.loadDataMasterRadiology = function () {
        return this.apollo.watchQuery({
            query: MasterDataRadioQuery
        });
    };
    DokterService.prototype.addLab = function (formarray) {
        return this.apollo.mutate({
            mutation: addLabQuery,
            variables: {
                id_integrasi: formarray.id_integrasi,
                no_medrec: formarray.no_medrec,
                indikasi: formarray.indikasi,
                diagnosis: formarray.diagnosis,
                lab: formarray.lab,
                tgl_permintaan: formarray.tgl_permintaan
            }
        });
    };
    DokterService.prototype.addRadio = function (formarray) {
        return this.apollo.mutate({
            mutation: addRadiologyQuery,
            variables: {
                id_integrasi: formarray.id_integrasi,
                no_medrec: formarray.no_medrec,
                indikasi: formarray.indikasi,
                diagnosis: formarray.diagnosis,
                radiologi: formarray.radiologi,
                tgl_permintaan: formarray.tgl_permintaan
            }
        });
    };
    DokterService.prototype.addFisio = function (formarray) {
        return this.apollo.mutate({
            mutation: addFisioQuery,
            variables: {
                id_integrasi: formarray.id_integrasi,
                no_medrec: formarray.no_medrec,
                indikasi: formarray.indikasi,
                diagnosis: formarray.diagnosis,
                fisioterapi: formarray.fisioterapi,
                tgl_permintaan: formarray.tgl_permintaan
            }
        });
    };
    DokterService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Apollo])
    ], DokterService);
    return DokterService;
}());
export { DokterService };
//# sourceMappingURL=dokter-service.js.map