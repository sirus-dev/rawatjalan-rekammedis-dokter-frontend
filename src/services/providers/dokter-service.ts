import {
    addConsultationQuery,
    addInformConsentApprovalQuery,
    addReferenceQuery,
    addTaggingOperationQuery,
    checkICD10,
    doctorByNodokQuery,
    ExamineByIdQuery,
    examineDataExchangeQuery,
    ExamineLast1Query,
    getConsultantNodok1Query,
    getConsultantTujuanQuery,
    getCTByDate,
    getDataDoctor,
    getDataObat,
    getIcd9,
    getIcdx,
    getInformationPatientQuery,
    getInformConsentQuery,
    getIntegratedNote1Query,
    getIntegratedNoteByIdQuery,
    getIntegratedNoteNodok1Query,
    getIntegratedNoteQuery,
    getPatientQuery,
    getReviewByIdQuery,
    InformConsentAnestesiQuery,
    InformConsentInfoQuery,
    IntegratedNoteQuery,
    kidReviewByMedrecQuery,
    notificationPatientQuery,
    PatientByIdQuery,
    QueryChekICD10,
    QueryCTByDate,
    QueryCTById,
    QueryCTById1,
    QueryCTByIdCT,
    QueryCTByNodok1,
    QueryDataConsulNodok1,
    QueryDataConsulTujuan,
    QueryDataDoctor,
    QueryDoctorByNodok,
    QueryExamineById,
    QueryExamineLast,
    QueryExamineLast1,
    QueryGetInformation,
    QueryICD9,
    QueryICDX,
    QueryInformConsent,
    QueryKidReviewByMedrec,
    QueryObat,
    QueryPatient,
    QueryPatientById,
    setExaminationFlagQuery,
    updateConsultationAnswerQuery,
    updateDiagExamQuery,
    updateFlagInformQuery,
    updateFlagReviewQuery,
    updateKidFlagReviewQuery,
    QueryQueueToday,
    QueueTodayQuery,
    reviewDoctorAssessmentQuery,
    setQueueFlagQuery,
    informTypeByNameQuery,
    QueryInformTypeByName,
    QueryMasterDataIntegrated,
    MasterDataLabQuery,
    MasterDataRadioQuery,
    addLabQuery,
    addRadiologyQuery,
    addFisioQuery,
} from './../gqlquery/gqlquery';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { QueryReviewById } from '../gqlquery/gqlquery';

@Injectable()
export class DokterService {

constructor(public apollo: Apollo) {}

public getNotificationPatientSub(nodok: string) {
  return this.apollo.subscribe({
    query: examineDataExchangeQuery,
    variables: {
      nodok: nodok,
      flag: 0
    }
  });
}

public getListPatient(nodok: string) {
  return this.apollo.watchQuery<QueryQueueToday>({
    query: QueueTodayQuery,
    variables: {
      nodok: nodok
    },
    fetchPolicy: 'network-only'
  });
}

public getDataDoctor(nodok: string) {
  return this.apollo.watchQuery<QueryDoctorByNodok>({
    query: doctorByNodokQuery,
    variables: {
      nodok: nodok
    },
    fetchPolicy: 'network-only'
  })
}

public getReviewById(no_medrec: string) {
  return this.apollo.watchQuery<QueryReviewById>({
    query: getReviewByIdQuery,
    variables: {
      no_medrec: no_medrec
    },
    fetchPolicy: 'network-only'
  });
}

public getKidReviewByMedrec(no_medrec: string) {
  return this.apollo.watchQuery<QueryKidReviewByMedrec>({
    query: kidReviewByMedrecQuery,
    variables: {
      no_medrec: no_medrec
    }
  });
}

public getDataPatient(nodok: string) {
  return this.apollo.watchQuery<QueryPatient>({
    query: getPatientQuery,
    variables: {
      nodok: nodok
    }
  });
}

public notificationDataPatient(nodok: string) {
  return this.apollo.watchQuery<QueryPatient>({
    query: notificationPatientQuery,
    variables: {
      nodok: nodok,
    },
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange : true
  });
}

public getDataICDX(penyakit: string, offset: number, limit: number) {
  return this.apollo.watchQuery<QueryICDX>({
    query: getIcdx,
    variables: {
      penyakit: penyakit,
      offset: offset,
      limit: limit
    }
  });
}

public getDataICD9(deskripsi: string, offset: number, limit: number) {
  return this.apollo.watchQuery<QueryICD9>({
    query: getIcd9,
    variables: {
      deskripsi: deskripsi,
      offset: offset,
      limit: limit
    }
  });
}

public getDataObat(nama: string, offset: number, limit: number) {
  return this.apollo.watchQuery<QueryObat>({
    query: getDataObat,
    variables: {
      nama: nama,
      offset: offset,
      limit: limit
    }
  });
}

public getDataPatientById(no_medrec: string) {
  return this.apollo.watchQuery<QueryExamineById>({
    query: ExamineByIdQuery,
    variables: {
      no_medrec: no_medrec
    }
  });
}

public getDataIntegratedNote(no_medrec: string) {
  return this.apollo.watchQuery<QueryCTById>({
    query: getIntegratedNoteQuery,
    variables: {
      no_medrec: no_medrec
    }
  });
}

public getDataInformationPatient(no_medrec: string) {
  return this.apollo.watchQuery<QueryGetInformation>({
    query: getInformationPatientQuery,
    variables: {
      no_medrec: no_medrec
    },
    fetchPolicy: 'network-only'
  });
}

public getDataIntegratedNoteById(id_ct: string) {
  return this.apollo.watchQuery<QueryCTByIdCT>({
    query: getIntegratedNoteByIdQuery,
    variables: {
      id_ct: id_ct
    }
  });
}

public getDataIntegratedNote1(no_medrec: string) {
  return this.apollo.watchQuery<QueryCTById1>({
    query: getIntegratedNote1Query,
    variables: {
      no_medrec: no_medrec
    },
    fetchPolicy: 'network-only'
  });
}

public getDataIntegratedNoteNodok1(nodok: string, no_medrec: string) {
  return this.apollo.watchQuery<QueryCTByNodok1>({
    query: getIntegratedNoteNodok1Query,
    variables: {
      nodok: nodok,
      no_medrec: no_medrec
    },
    fetchPolicy: 'network-only'
  });
}

public saveIntegratedNote(formarray: any) {
  console.log(formarray);
  return this.apollo.mutate({
    mutation: IntegratedNoteQuery,
    variables: {
      id_ct: formarray.id_ct,
      no_medrec: formarray.no_medrec,
      nama_pasien: formarray.nama_pasien,
      nodok: formarray.nodok,
      nama_dok: formarray.nama_dok,
      no_periksa: formarray.no_periksa,
      jaminan: formarray.jaminan,
      tgl_lahir: formarray.tgl_lahir,
      umur: formarray.umur,
      kelamin: formarray.kelamin,
      kelas: formarray.kelas,
      tgl_ct: formarray.tgl_ct,
      s_description: formarray.s_description,
      s_stroke: formarray.s_stroke,
      o_description: formarray.o_description,
      o_stroke: formarray.o_stroke,
      a_description: formarray.a_description,
      a_stroke: formarray.a_stroke,
      a_icd9: formarray.a_icd9,
      a_icdx: formarray.a_icdx,
      p_lab_description: formarray.p_lab_description,
      p_lab_stroke: formarray.p_lab_stroke,
      p_rad_description: formarray.p_rad_description,
      p_rad_stroke: formarray.p_rad_stroke,
      p_fisio_description: formarray.p_fisio_description,
      p_fisio_stroke: formarray.p_fisio_stroke,
      p_tindakan_description: formarray.p_tindakan_description,
      p_tindakan_stroke: formarray.p_tindakan_stroke,
      p_obat_description: formarray.p_obat_description,
      p_obat_stroke: formarray.p_obat_stroke,
      p_obat_nama: formarray.p_obat_nama,
      p_obat_alergi: formarray.p_obat_alergi,
      penyakit: formarray.penyakit
    }
  });
}

public setFlagExamination(no_medrec: string, flag: number) {
  return this.apollo.mutate({
    mutation: setExaminationFlagQuery,
    variables: {
      no_medrec: no_medrec,
      flag: flag
    }
  });
}

public updateFlagReview(no_medrec: string, flagKaji: number) {
  return this.apollo.mutate({
    mutation: updateFlagReviewQuery,
    variables: {
      no_medrec: no_medrec,
      flagKaji: flagKaji
    }
  });
}

public updateKidFlagReview(no_medrec: string, flagKaji: string) {
  return this.apollo.mutate({
    mutation: updateKidFlagReviewQuery,
    variables: {
      no_medrec: no_medrec,
      flagKaji: flagKaji
    }
  });
}

public updateDiagExam(no_periksa: string, diagnosa: string, diagnosa_stroke: any, kasus: string) {

  if (diagnosa == null) {
    diagnosa = "";
  }
  return this.apollo.mutate({
    mutation: updateDiagExamQuery,
    variables: {
      no_periksa: no_periksa,
      diagnosa: diagnosa,
      diagnosa_stroke: diagnosa_stroke,
      kasus: kasus
    }
  });
}

public saveInformConsentInfo(formarray: any) {
  return this.apollo.mutate({
    mutation: InformConsentInfoQuery,
    variables: {
      no_inform: formarray.no_inform,
      no_medrec: formarray.no_medrec,
      pelaksana_tindakan: formarray.pelaksana_tindakan,
      pemberi_info: formarray.pemberi_info,
      penerima_info: formarray.penerima_info,
      info_diag_kerja: formarray.info_diag_kerja,
      checklist_info_diag_kerja: formarray.checklist_info_diag_kerja,
      dasar_diag: formarray.dasar_diag,
      checklist_dasar_diag: formarray.checklist_dasar_diag,
      tindakan_dokter: formarray.tindakan_dokter,
      checklist_tindakan_dokter: formarray.checklist_tindakan_dokter,
      indikasi_tindakan: formarray.indikasi_tindakan,
      checklist_indikasi_tindakan: formarray.checklist_indikasi_tindakan,
      tatacara: formarray.tatacara,
      checklist_tatacara: formarray.checklist_tatacara,
      tujuan: formarray.tujuan,
      checklist_tujuan: formarray.checklist_tujuan,
      risiko: formarray.risiko,
      checklist_risiko: formarray.checklist_risiko,
      komplikasi: formarray.komplikasi,
      checklist_komplikasi: formarray.checklist_komplikasi,
      prognosis: formarray.prognosis,
      checklist_prognosis: formarray.checklist_prognosis,
      alternatif: formarray.alternatif,
      checklist_alternatif: formarray.checklist_alternatif,
      lain: formarray.lain,
      checklist_lain: formarray.checklist_lain,
      tt_dokter: formarray.tt_dokter,
      tt_pasien: formarray.tt_pasien,
      tt_saksi1: formarray.tt_saksi1,
      tt_saksi2: formarray.tt_saksi2
    }
  });
}

public saveInformConsentAnestesi(formarray: any) {
  return this.apollo.mutate({
    mutation: InformConsentAnestesiQuery,
    variables: {
      no_inform: formarray.no_inform,
      no_medrec: formarray.no_medrec,
      a_u_tindakan_dokter: formarray.a_u_tindakan_dokter,
      a_u_tatacara: formarray.a_u_tatacara,
      a_u_tujuan: formarray.a_u_tujuan,
      a_u_risiko: formarray.a_u_risiko,
      a_u_komplikasi: formarray.a_u_komplikasi,
      a_r_tindakan_dokter: formarray.a_r_tindakan_dokter,
      a_r_tatacara: formarray.a_r_tatacara,
      a_r_tujuan: formarray.a_r_tujuan,
      a_r_risiko: formarray.a_r_risiko,
      a_r_komplikasi: formarray.a_r_komplikasi,
      a_r_alternatif: formarray.a_r_alternatif,
      a_l_tindakan_dokter: formarray.a_l_tindakan_dokter,
      a_l_tatacara: formarray.a_l_tatacara,
      a_l_tujuan: formarray.a_l_tujuan,
      a_l_risiko: formarray.a_l_risiko,
      a_l_komplikasi: formarray.a_l_komplikasi,
      a_l_alternatif: formarray.a_l_alternatif,
      sedasi_tindakan_dokter: formarray.sedasi_tindakan_dokter,
      sedasi_tatacara: formarray.sedasi_tatacara,
      sedasi_tujuan: formarray.sedasi_tujuan,
      sedasi_risiko: formarray.sedasi_risiko,
      sedasi_komplikasi: formarray.sedasi_komplikasi,
      sedasi_prognosis: formarray.sedasi_prognosis,
      sedasi_lain: formarray.sedasi_lain,
      tt_dokter: formarray.tt_dokter,
      tt_pasien: formarray.tt_pasien,
      tt_saksi1: formarray.tt_saksi1,
      tt_saksi2: formarray.tt_saksi2
    }
  });
}

public getIntegratedNoteByDateQuery(no_medrec: string, tgl_ct: string) {
  return this.apollo.watchQuery<QueryCTByDate>({
    query: getCTByDate,
    variables: {
      no_medrec: no_medrec,
      tgl_ct: tgl_ct
    }
  });
}

public getCheckICD10(no_medrec: string, a_icdx: string) {
  return this.apollo.watchQuery<QueryChekICD10>({
    query: checkICD10,
    variables: {
      no_medrec: no_medrec,
      a_icdx: a_icdx
    }
  });
}

public getDataDoctorLike(nama_dok: string, offset: number, limit: number) {
  return this.apollo.watchQuery<QueryDataDoctor>({
    query: getDataDoctor,
    variables: {
      nama_dok: nama_dok,
      offset: offset,
      limit: limit
    }
  });
}

public saveConsultation(formarray: any) {
  return this.apollo.mutate({
    mutation: addConsultationQuery,
    variables: {
      no_konsultasi: formarray.no_konsultasi,
      no_medrec: formarray.no_medrec,
      nodok: formarray.nodok,
      id_ct: formarray.id_ct,
      alasan: formarray.alasan,
      nodok_tujuan: formarray.nodok_tujuan
    }
  });
}

public getDataConsultationNodok1(no_medrec: string, nodok: string) {
  return this.apollo.watchQuery<QueryDataConsulNodok1>({
    query: getConsultantNodok1Query,
    variables: {
      no_medrec: no_medrec,
      nodok: nodok
    }
  });
}

public getDataConsultationTujuan(nodok_tujuan: string) {
  return this.apollo.watchQuery<QueryDataConsulTujuan>({
    query: getConsultantTujuanQuery,
    variables: {
      nodok_tujuan: nodok_tujuan,
    }
  });
}

public updateConsultataion(formarray: any) {
  return this.apollo.mutate({
    mutation: updateConsultationAnswerQuery,
    variables: {
      no_konsultasi: formarray.no_konsultasi,
      id_ct_jawaban: formarray.id_ct_jawaban,
      jawaban: formarray.jawaban
    }
  });
}

public getInformConsentQuery(no_medrec: string) {
  return this.apollo.watchQuery<QueryInformConsent>({
    query: getInformConsentQuery,
    variables: {
      no_medrec: no_medrec
    },
    fetchPolicy: 'network-only'
  });
}

public getInformConsent0Query(no_medrec: string) {
  return this.apollo.watchQuery<QueryInformConsent>({
    query: getInformConsentQuery,
    variables: {
      no_medrec: no_medrec
    }
  });
}

public updateInformConsentApproval(formarray: any) {
  return this.apollo.mutate({
    mutation: addInformConsentApprovalQuery,
    variables: {
      no_inform: formarray.no_inform,
      nama_ortu: formarray.nama_ortu,
      umur_ortu: formarray.umur_ortu,
      kelamin_ortu: formarray.kelamin_ortu,
      alamat: formarray.alamat,
      ktp: formarray.ktp,
      tgl_inform: formarray.tgl_inform,
      jam_inform: formarray.jam_inform,
      yg_menyatakan: formarray.yg_menyatakan,
      dokter_operator: formarray.dokter_operator,
      saksi1: formarray.saksi1,
      saksi2: formarray.saksi2,
      tindakan_operasi: formarray.tindakan_operasi,
      tipe_tindakan: formarray.tipe_tindakan
    }
  });
}

public updateTaggingOperation(formarray: any) {
  return this.apollo.mutate({
    mutation: addTaggingOperationQuery,
    variables: {
      no_inform: formarray.no_inform,
      penandaan_operasi: formarray.penandaan_operasi
    }
  });
}

public updateFlagInform(no_inform: string, flag: number) {
  return this.apollo.mutate({
    mutation: updateFlagInformQuery,
    variables: {
      no_inform: no_inform,
      flag: flag
    }
  });
}

public getTypeInform(tipe_inform: string) {
  return this.apollo.watchQuery<QueryInformTypeByName>({
    query: informTypeByNameQuery,
    variables: {
      tipe_inform: tipe_inform
    }
  });
}

public addReferencePatient(formarray: any) {
  return this.apollo.mutate({
    mutation: addReferenceQuery,
    variables: {
      id_rujukan: formarray.id_rujukan,
      nama_pasien: formarray.nama_pasien,
      no_medrec: formarray.no_medrec,
      umur: formarray.umur,
      kelamin: formarray.kelamin,
      tgl: formarray.tgl,
      nama_dok: formarray.nama_dok,
      jaminan: formarray.jaminan,
      kelas: formarray.kelas,
      tipe_rujukan: formarray.tipe_rujukan,
      diagnosa: formarray.diagnosa,
      indikasi: formarray.indikasi,
      keterangan: formarray.keterangan,
      lab_hematologi: formarray.lab_hematologi,
      lab_urinalisa: formarray.lab_urinalisa,
      lab_faeces: formarray.lab_faeces,
      lab_gula_darah: formarray.lab_gula_darah,
      lab_kimia_darah: formarray.lab_kimia_darah,
      lab_berologi: formarray.lab_berologi,
      lab_mikrobiologi: formarray.lab_mikrobiologi,
      lab_lain: formarray.lab_lain,
      rad_tanpa_kontras: formarray.rad_tanpa_kontras,
      rad_kontras: formarray.rad_kontras,
      fisio_tindakan: formarray.fisio_tindakan
    }
  });
}

public loadPatientExamination1(nodok: string) {
  return this.apollo.watchQuery<QueryExamineLast1>({
    query: ExamineLast1Query,
    variables: {
      nodok: nodok
    },
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange : true
  });
}

public loadPatientExamination(nodok: string) {
  return this.apollo.watchQuery<QueryExamineLast>({
    query: getPatientQuery,
    variables: {
      nodok: nodok
    }
  });
}

public getPatientById(no_medrec: string) {
  return this.apollo.watchQuery<QueryPatientById>({
    query: PatientByIdQuery,
    variables: {
      no_medrec: no_medrec
    }
  });
}

public updateDoctorReview(formarray: any) {
  console.log(formarray);
  return this.apollo.mutate({
    mutation: reviewDoctorAssessmentQuery,
    variables: {
      no_kaji: formarray.no_kaji,
      nama_dok: formarray.nama_dok,
      nodok: formarray.nodok,
      anamnesa_keluhan: formarray.anamnesa_keluhan,
      anamnesa_riwayat_penyakit: formarray.anamnesa_riwayat_penyakit,
      fisik_kepala: formarray.fisik_kepala,
      fisik_leher: formarray.fisik_leher,
      fisik_mulut: formarray.fisik_mulut,
      fisik_paru: formarray.fisik_paru,
      fisik_genitalia: formarray.fisik_genitalia,
      fisik_mata: formarray.fisik_mata,
      fisik_tht: formarray.fisik_tht,
      fisik_jantung: formarray.fisik_jantung,
      fisik_abdomen: formarray.fisik_abdomen,
      fisik_ekstremitas: formarray.fisik_ekstremitas,
      status_gizi: formarray.status_gizi,
      pemeriksaan_gigi: formarray.pemeriksaan_gigi,
      pemeriksaan_gigi_ket: formarray.pemeriksaan_gigi_ket,
      status_lokalis: formarray.status_lokalis,
      pemeriksaan_penunjang: formarray.pemeriksaan_penunjang,
      diagnosis: formarray.diagnosis,
      rencana: formarray.rencana,
      dirujuk: formarray.dirujuk,
      dirujuk_lainnya: formarray.dirujuk_lainnya,
      tt_dokter: formarray.tt_dokter
    }
  });
}

public setFlagPatient(no_medrec: string, flag: number) {
  return this.apollo.mutate({
    mutation: setQueueFlagQuery,
    variables: {
      no_medrec: no_medrec,
      flag: flag
    }
  });
}

public loadDataMasterLab() {
  return this.apollo.watchQuery<QueryMasterDataIntegrated>({
    query: MasterDataLabQuery
  });
}

public loadDataMasterRadiology() {
  return this.apollo.watchQuery<QueryMasterDataIntegrated>({
    query: MasterDataRadioQuery
  });
}

public addLab(formarray:any) {
  return this.apollo.mutate({
    mutation: addLabQuery,
    variables: {
      id_integrasi: formarray.id_integrasi,
      no_medrec: formarray.no_medrec, 
      indikasi: formarray.indikasi,
      diagnosis: formarray.diagnosis,
      lab: formarray.lab,
      tgl_permintaan: formarray.tgl_permintaan    
    }
  });
}

public addRadio(formarray:any) {
  return this.apollo.mutate({
    mutation: addRadiologyQuery,
    variables: {
      id_integrasi: formarray.id_integrasi,
      no_medrec: formarray.no_medrec, 
      indikasi: formarray.indikasi,
      diagnosis: formarray.diagnosis,
      radiologi: formarray.radiologi,
      tgl_permintaan: formarray.tgl_permintaan    
    }
  });
}

public addFisio(formarray:any) {
  return this.apollo.mutate({
    mutation: addFisioQuery,
    variables: {
      id_integrasi: formarray.id_integrasi,
      no_medrec: formarray.no_medrec, 
      indikasi: formarray.indikasi,
      diagnosis: formarray.diagnosis,
      fisioterapi: formarray.fisioterapi,
      tgl_permintaan: formarray.tgl_permintaan    
    }
  });
}

}

