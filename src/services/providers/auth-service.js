var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { LoadingController, AlertController } from 'ionic-angular';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import { Storage } from "@ionic/storage";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
var AuthService = /** @class */ (function () {
    function AuthService(http, storage, alertCtrl, loadingCtrl) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.API = "/auth/authentication";
        this.APIDATA = "/auth/data";
        this.contentHeader = new Headers({ "Content-Type": "application/json" });
        this.jwtHelper = new JwtHelper();
        storage.ready().then(function () {
            storage.get('email').then(function (email) {
                _this.user = JSON.parse(email);
            }).catch(console.log);
        });
    }
    AuthService.prototype.login = function (credentials) {
        if (credentials.email === null || credentials.password === null) {
            return Observable.throw('Pelase Insert Credentials');
        }
        else {
            return this.http.post(this.API, JSON.stringify(credentials), { headers: this.contentHeader }).map(function (res) { return res.json(); });
        }
    };
    AuthService.prototype.forgetPass = function (credentials) {
        if (credentials.email === null) {
            return Observable.throw('Pelase Insert Credentials');
        }
        else {
            return this.http.get(this.API + "/forget/" + credentials.email, { headers: this.contentHeader })
                .map(function (res) { return res.json(); });
        }
    };
    AuthService.prototype.getDataDoctor = function (credentials) {
        if (credentials.email === null || credentials.password === null) {
            throw ('Pelase Insert Credentials');
        }
        else {
            return this.http.get(this.APIDATA + "/" + credentials.email, { headers: this.contentHeader }).map(function (res) { return res.json(); }).toPromise();
        }
    };
    // for save token to storage
    AuthService.prototype.saveToken = function (data, credentials) {
        var _this = this;
        this.user = this.jwtHelper.decodeToken(data.token).email;
        // return data from data doctor
        return Observable.fromPromise(
        // save token to storage
        this.storage.set('token', data.token)
            .then(function () {
            return _this.storage.get('token');
        })
            .then(function (data) {
            if (data) {
                return _this.getDataDoctor(credentials);
            }
            else {
                throw ("No Token");
            }
        })
            .then(function (data) {
            return _this.dataDoctor(data);
        }));
    };
    AuthService.prototype.dataDoctor = function (data) {
        if (data[0].role == "dokter") {
            return Promise.all([
                this.storage.set('role', data[0].role),
                this.storage.set('nodok', data[0].nodok)
            ]).then(function (_a) {
                var data = _a[0];
                return data;
            });
        }
        throw ("Anda Bukan Dokter");
    };
    AuthService.prototype.sendEmailSuccess = function (data) {
        if (data.success == true) {
            return true;
        }
        else {
            return this.showError("Email yang anda masukkan salah");
        }
    };
    AuthService.prototype.getToken = function () {
        if (this.saveToken == null) {
            return null;
        }
        else {
            this.storage.get('token');
        }
    };
    AuthService.authenticated = function () {
        return tokenNotExpired('/_ionickv/token');
    };
    AuthService.prototype.register = function (credentials) {
        if (credentials.email === null || credentials.password === null) {
            return Observable.throw('Pelase Insert Credentials');
        }
        else {
            return Observable.create(function (observer) {
                //simpan ke API
                observer.next(true);
                observer.complete();
            });
        }
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        return Observable.create(function (observer) {
            var removeToken = _this.storage.remove('token');
            _this.storage.remove('nodok');
            _this.storage.remove('role');
            observer.next(removeToken);
            observer.complete();
        });
    };
    AuthService.prototype.showError = function (error) {
        var alert = this.alertCtrl.create({
            title: 'Gagal Kirim Email',
            subTitle: error,
            buttons: ['OK']
        });
        alert.present();
    };
    AuthService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Storage, AlertController, LoadingController])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth-service.js.map