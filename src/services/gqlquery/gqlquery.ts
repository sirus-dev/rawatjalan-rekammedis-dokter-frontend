import gql from 'graphql-tag';

export interface QueryReviewById{
  reviewByMedrec;
}

export interface QueryICDX{
  icdxPagination;
}

export interface QueryICD9 {
  icd9Pagination;
}

export interface QueryObat{
  tb001Pagination;
}

export interface QueryPatient{
  examineLast;
  doctorByNodok;
}

export interface QueryExamineLast {
  examineLast;
}

export interface QueryExamineLast1 {
  examineLast1;
}

export interface QueryExamineById {
  examineById;
}

export interface QueryCTById {
  ctByMedrec;
}

export interface QueryGetInformation {
  ctByMedrec;
  reviewByMedrec;
}

export interface QueryCTById1 {
  ctByMedrec1;
}

export interface QueryCTByNodok1 {
  ctByNodok1;
}

export interface QueryDoctorByNodok {
  doctorByNodok;
}

export interface QueryKidReviewByMedrec {
  kidReviewByMedrec;
}

export interface QueryCTByDate {
  ctByDate;
}

export interface QueryCTByIdCT {
  ctById;
}

export interface QueryChekICD10 {
  checkICD10;
}

export interface QueryDataDoctor {
  doctorByName;
}

export interface QueryDataConsulNodok1 {
  consultationByNodok1;
}

export interface QueryDataConsulTujuan1 {
  consultationByTujuan1;
}

export interface QueryDataConsulTujuan {
  consultationByTujuan;
}

export interface QueryInformConsent {
  informConsent0;
}

export interface QueryInformTypeByName {
  informTypeByName;
}

export interface QueryPatientById {
  patientByNorm;
}

export interface QueryQueueToday {
  queueAllToday
}

export interface QueryMasterDataIntegrated {
  masterLabAll
  masterRadiologiAll
}

export const QueueTodayQuery = gql`
query QueueTodayQuery($nodok:String!) {
  queueAllToday(nodok:$nodok) {
    id_antrian
    kode_unit
    poli
    nodok
    nama_dok
    no_registrasi
    no_medrec
    nama_pasien
    no_antrian
    jaminan
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    tgl_antri
    flag
  }
}
`;

export const doctorByNodokQuery = gql`
query doctorByNodokQuery($nodok:String!){
  doctorByNodok(nodok:$nodok) {
    nodok
    nama_dok
    specialis
  }
}
`;

export const getPatientQuery = gql`
query getPatientQuery($nodok:String!){
  examineLast(nodok:$nodok){
    no_periksa,
    poli,
    nama_pasien,
    no_medrec,
    nodok,
    nama_dok,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const notificationPatientQuery = gql`
query notificationPatientQuery($nodok:String!){
  examineLast(nodok:$nodok){
    no_periksa,
    poli,
    nama_pasien,
    no_medrec,
    nodok,
    nama_dok,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }

  doctorByNodok(nodok:$nodok) {
    nodok
    nama_dok
    specialis
  }
}
`;

export const ExamineLast1Query = gql`
query ExamineLast1Query($nodok:String!){
  examineLast1(nodok:$nodok){
    no_periksa,
    poli,
    nodok,
    nama_dok,
    no_registrasi,
    no_medrec,
    nama_pasien,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const getReviewByIdQuery = gql`
query getReviewByIdQuery($no_medrec: String!) {
  reviewByMedrec(no_medrec: $no_medrec) {
    keluhan
    td,
    nadi,
    pernafasan,
    suhu,
    tb,
    bb
  }
}
`;

export const getIcdx = gql`
query getIcdx($penyakit:String!, $offset:Int, $limit:Int){
  icdxPagination(penyakit:$penyakit, offset:$offset, limit:$limit){
    icd,
    dtd,
    parent,
    penyakit
  }
}
`;

export const getIcd9 = gql`
query getIcd9($deskripsi:String!, $offset:Int, $limit:Int){
  icd9Pagination(deskripsi:$deskripsi, offset:$offset, limit:$limit){
    kode,
    deskripsi,
    klasifikasi,
    kelompok
  }
}
`;

export const getDataObat = gql`
query getDataObat($nama:String!, $offset:Int, $limit:Int){
  tb001Pagination(nama:$nama, offset:$offset, limit:$limit){
    kdbr,
    nama,
    kdsp,
    satuan
  }
}
`;

export const ExamineByIdQuery = gql`
query ExamineByIdQuery($no_medrec:String!) {
  examineById(no_medrec: $no_medrec) {
    no_periksa,
    poli,
    nama_pasien,
    no_medrec,
    nodok,
    no_antrian,
    jaminan,
    umur,
    tgl_lahir,
    kelamin,
    status_pasien,
    kaji,
    flag
  }
}
`;

export const IntegratedNoteQuery = gql`
mutation IntegratedNoteQuery(
  $id_ct: String,
  $no_medrec: String,
  $nama_pasien: String,
  $nodok: String,
  $nama_dok: String,
  $no_periksa: String,
  $jaminan: String,
  $tgl_lahir: String,
  $umur: Int,
  $kelamin: String,
  $kelas: String,
  $tgl_ct: String,
  $s_stroke: String,
  $s_description: String,
  $o_stroke: String,
  $o_description: String,
  $a_stroke: String,
  $a_description: String,
  $a_icdx: String,
  $p_lab_stroke: String,
  $p_lab_description: String,
  $p_rad_stroke: String,
  $p_rad_description: String,
  $p_fisio_stroke: String,
  $p_fisio_description: String,
  $p_tindakan_stroke: String,
  $p_tindakan_description: String,
  $p_obat_stroke: String,
  $p_obat_description: String,
  $p_obat_nama: String,
  $p_obat_alergi: String,
  $penyakit: String
){
  addCt(
    id_ct: $id_ct,
    no_medrec: $no_medrec,
    nama_pasien: $nama_pasien,
    nodok:$nodok,
    nama_dok: $nama_dok,
    no_periksa:$no_periksa,
    jaminan: $jaminan,
    tgl_lahir: $tgl_lahir,
    umur: $umur,
    kelamin: $kelamin,
    kelas: $kelas,
    tgl_ct: $tgl_ct,
    s_stroke: $s_stroke,
    s_description: $s_description,
    o_stroke: $o_stroke,
    o_description: $o_description,
    a_stroke: $a_stroke,
    a_description: $a_description,
    a_icdx: $a_icdx,
    p_lab_stroke: $p_lab_stroke,
    p_lab_description: $p_lab_description,
    p_rad_stroke: $p_rad_stroke,
    p_rad_description: $p_rad_description,
    p_fisio_stroke: $p_fisio_stroke,
    p_fisio_description: $p_fisio_description,
    p_tindakan_stroke: $p_tindakan_stroke,
    p_tindakan_description: $p_tindakan_description,
    p_obat_stroke: $p_obat_stroke,
    p_obat_description: $p_obat_description,
    p_obat_nama: $p_obat_nama,
    p_obat_alergi: $p_obat_alergi,
    penyakit: $penyakit
  ){
    id_ct,
    no_medrec,
    nama_pasien,
    nodok,
    nama_dok,
    no_periksa,
    jaminan,
    tgl_lahir,
    umur,
    kelamin,
    kelas,
    tgl_ct,
    s_stroke,
    s_description,
    o_stroke,
    o_description,
    a_stroke,
    a_description,
    a_icdx,
    p_lab_stroke,
    p_lab_description,
    p_rad_stroke,
    p_rad_description,
    p_fisio_stroke,
    p_fisio_description,
    p_tindakan_stroke,
    p_tindakan_description,
    p_obat_stroke,
    p_obat_description,
    p_obat_nama,
    p_obat_alergi,
    penyakit
  }
}
`;

export const getInformationPatientQuery = gql`
query getInformationPatientQuery(
  $no_medrec: String!
){
  ctByMedrec(
    no_medrec: $no_medrec,
  ){
    a_icdx
    p_obat_alergi
    penyakit
  }

  reviewByMedrec(no_medrec: $no_medrec) {
    no_kaji
    no_medrec
    keluhan
    resikoA1
    resikoA2
    resikoB1
    resikoHasil1
    resikoHasil2
    resikoHasil3
    resikoTindakan1action
    resikoTindakan2action
    resikoTindakan3action
    resikoTindakan1
    resikoTindakan2
    resikoTindakan3
    td
    td1
    td2
    nadi
    nadi1
    nadi2
    pernafasan
    pernafasan1
    pernafasan2
    suhu
    tb
    bb
    nyeri
    resiko_jatuh_a
    resiko_jatuh_b
    gizi1
    gizi2
    gizi3
    gizi3_lainnya
    status_fungsional
    status_fungsional_ket
    anamnesa_keluhan
    anamnesa_riwayat_penyakit
    fisik_kepala
    fisik_leher
    fisik_mulut
    fisik_paru
    fisik_genitalia
    fisik_mata
    fisik_tht
    fisik_jantung
    fisik_abdomen
    fisik_ekstremitas
    status_gizi
    pemeriksaan_gigi
    pemeriksaan_gigi_ket
    status_lokalis
    pemeriksaan_penunjang
    diagnosis
    rencana
    dirujuk
    dirujuk_lainnya
    tt_perawat
    tt_dokter
    flagKaji
  }
}
`;

export const getIntegratedNoteQuery = gql`
query IntegratedNoteQuery(
  $no_medrec: String!
){
  ctByMedrec(
    no_medrec: $no_medrec,
  ){
    id_ct
    no_medrec
    nama_pasien
    nama_dok
    no_periksa
    jaminan
    tgl_lahir
    umur
    kelamin
    kelas
    tgl_ct
    s_description
    s_stroke
    o_description
    o_stroke
    a_description
    a_stroke
    a_icd9
    a_icdx
    p_lab_description
    p_lab_stroke
    p_rad_description
    p_rad_stroke
    p_fisio_description
    p_fisio_stroke
    p_tindakan_description
    p_tindakan_stroke
    p_obat_description
    p_obat_stroke
    p_obat_nama
    p_obat_alergi
  }
}
`;

export const getIntegratedNoteByIdQuery = gql`
query IntegratedNoteByIdQuery(
  $id_ct: String
){
  ctById(
    id_ct: $id_ct,
  ){
    id_ct
    no_medrec
    nama_pasien
    nodok
    nama_dok
    jaminan
    tgl_lahir
    umur
    kelamin
    kelas
    tgl_ct
    s_description
    s_stroke
    o_description
    o_stroke
    a_description
    a_stroke
    a_icd9
    a_icdx
    p_lab_description
    p_lab_stroke
    p_rad_description
    p_rad_stroke
    p_fisio_description
    p_fisio_stroke
    p_tindakan_description
    p_tindakan_stroke
    p_obat_description
    p_obat_stroke
    p_obat_nama
    p_obat_alergi
  }
}
`;

export const setExaminationFlagQuery = gql`
mutation setExaminationFlagQuery($no_medrec: String!, $flag: Int!) {
  setExaminationFlag(no_medrec: $no_medrec, flag: $flag) {
  no_periksa,
  poli,
  nama_pasien,
  no_medrec,
  nodok,
  no_antrian,
  jaminan,
  umur,
  kelamin,
  status_pasien,
  kaji,
  flag
}
}
`;

export const updateDiagExamQuery = gql`
mutation updateDiagExamQuery($no_periksa:String, $diagnosa:String, $diagnosa_stroke:String, $kasus:String) {
  updateDiagnosisExamination(no_periksa:$no_periksa, diagnosa:$diagnosa, diagnosa_stroke:$diagnosa_stroke, kasus:$kasus) {
    no_periksa
    kode_unit
    poli
    tgl
    nama_dok
    no_registrasi
    nama_pasien
    diagnosa
    diagnosa_stroke
    no_antrian
    kasus
    jaminan
    kunjungan
    rujuk
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    flag
  }
}
`;

export const kidReviewByMedrecQuery = gql`
query kidReviewByMedrecQuery($no_medrec: String!) {

    kidReviewByMedrec(no_medrec: $no_medrec){
      no_kaji
      no_medrec
      keluhan
      td1
      td2
      nadi
      pernafasan
      suhu
      tb
      bb
      riwayatAlergiObat
      riwayatAlergiObatKet
      riwayatAlergiMakanan
      riwayatAlergiMakananKet
      riwayatAlergiPantangan
      nilaiIMT
      keluhanNyeri
      nips
      nipsEkspresiWajah
      nipsEkspresiWajahHasil
      nipsMenangis
      nipsMenangisHasil
      nipsPernafasan
      nipsPernafasanHasil
      nipsPergelanganTangan
      nipsPergelanganTanganHasil
      nipsKaki
      nipsKakiHasil
      nipsKesadaran
      nipsKesadaranHasil
      flacc
      flaccWajah
      flaccWajahHasil
      flaccKaki
      flaccKakiHasil
      flaccAktifitas
      flaccAktifitasHasil
      flaccMenangis
      flaccMenangisHasil
      flaccBersuara
      flaccBersuaraHasil
      faces
      nyeri
      skalaNyeri
      penyebabNyeri
      mulaiNyeri
      tipeNyeri
      karakteristikNyeri
      karakteristikNyeriLainnya
      nyeriMenyebar
      nyeriMenyebarHasil
      frekuensiNyeri
      pengobatanNyeri
      pengobatanNyeriAkibat
      pengobatanNyeriAkibatLainnya
      kemampuanAktifitas
      statusPsikologis
      statusPsikologisLainnya
      statusMental
      statusMentalDisorientasi
      resikoA1
      resikoA2
      resikoB1
      resiko1Ket
      resiko2Ket
      resiko3Ket
    }
}
`;

export const InformConsentInfoQuery = gql`
mutation InformConsentInfoQuery(
  $no_inform: String
  $no_medrec: String
  $pelaksana_tindakan:  String
  $pemberi_info:  String
  $penerima_info:  String
  $info_diag_kerja:  String
  $checklist_info_diag_kerja: String
  $dasar_diag:  String
  $checklist_dasar_diag: String
  $tindakan_dokter:  String
  $checklist_tindakan_dokter: String
  $indikasi_tindakan:  String
  $checklist_indikasi_tindakan: String
  $tatacara:  String
  $checklist_tatacara: String
  $tujuan:  String
  $checklist_tujuan: String
  $risiko:  String
  $checklist_risiko: String
  $komplikasi:  String
  $checklist_komplikasi: String
  $prognosis:  String
  $checklist_prognosis: String
  $alternatif:  String
  $checklist_alternatif: String
  $lain:  String
  $checklist_lain: String
  $tt_dokter: String
  $tt_pasien: String
  $tt_saksi1: String
  $tt_saksi2: String
){
  addInformConsentInfo(
    no_inform: $no_inform
    no_medrec: $no_medrec
    pelaksana_tindakan:  $pelaksana_tindakan
    pemberi_info:  $pemberi_info
    penerima_info:  $penerima_info
    info_diag_kerja:  $info_diag_kerja
    checklist_info_diag_kerja: $checklist_info_diag_kerja
    dasar_diag:  $dasar_diag
    checklist_dasar_diag: $checklist_dasar_diag
    tindakan_dokter:  $tindakan_dokter
    checklist_tindakan_dokter: $checklist_tindakan_dokter
    indikasi_tindakan:  $indikasi_tindakan
    checklist_indikasi_tindakan: $checklist_indikasi_tindakan
    tatacara:  $tatacara
    checklist_tatacara: $checklist_tatacara
    tujuan:  $tujuan
    checklist_tujuan: $checklist_tujuan
    risiko:  $risiko
    checklist_risiko: $checklist_risiko
    komplikasi:  $komplikasi
    checklist_komplikasi: $checklist_komplikasi
    prognosis:  $prognosis
    checklist_prognosis: $checklist_prognosis
    alternatif:  $alternatif
    checklist_alternatif: $checklist_alternatif
    lain:  $lain
    checklist_lain: $checklist_lain
    tt_dokter: $tt_dokter
    tt_pasien: $tt_pasien
    tt_saksi1: $tt_saksi1
    tt_saksi2: $tt_saksi2
  ){
    no_inform
    no_medrec
  }
}
`;

export const InformConsentAnestesiQuery = gql`
mutation InformConsentAnestesiQuery(
  $no_inform: String
  $no_medrec: String
  $pelaksana_tindakan:  String
  $pemberi_info:  String
  $penerima_info:  String
  $a_u_tindakan_dokter:  String
  $a_u_tatacara: String
  $a_u_tujuan:  String
  $a_u_risiko: String
  $a_u_komplikasi:  String
  $a_r_tindakan_dokter: String
  $a_r_tatacara:  String
  $a_r_tujuan: String
  $a_r_risiko:  String
  $a_r_komplikasi: String
  $a_r_alternatif:  String
  $a_l_tindakan_dokter: String
  $a_l_tatacara:  String
  $a_l_tujuan: String
  $a_l_risiko:  String
  $a_l_komplikasi: String
  $a_l_alternatif:  String
  $sedasi_tindakan_dokter: String
  $sedasi_tatacara:  String
  $sedasi_tujuan: String
  $sedasi_risiko:  String
  $sedasi_komplikasi: String
  $sedasi_prognosis: String
  $sedasi_lain: String
  $tt_dokter: String
  $tt_pasien: String
  $tt_saksi1: String
  $tt_saksi2: String
){
  addInformConsentAnastesi(
    no_inform: $no_inform
    no_medrec: $no_medrec
    pelaksana_tindakan:  $pelaksana_tindakan
    pemberi_info:  $pemberi_info
    penerima_info:  $penerima_info
    a_u_tindakan_dokter:  $a_u_tindakan_dokter
    a_u_tatacara: $a_u_tatacara
    a_u_tujuan:  $a_u_tujuan
    a_u_risiko: $a_u_risiko
    a_u_komplikasi:  $a_u_komplikasi
    a_r_tindakan_dokter: $a_r_tindakan_dokter
    a_r_tatacara:  $a_r_tatacara
    a_r_tujuan: $a_r_tujuan
    a_r_risiko:  $a_r_risiko
    a_r_komplikasi: $a_r_komplikasi
    a_r_alternatif:  $a_r_alternatif
    a_l_tindakan_dokter: $a_l_tindakan_dokter
    a_l_tatacara:  $a_l_tatacara
    a_l_tujuan: $a_l_tujuan
    a_l_risiko:  $a_l_risiko
    a_l_komplikasi: $a_l_komplikasi
    a_l_alternatif:  $a_l_alternatif
    sedasi_tindakan_dokter: $sedasi_tindakan_dokter
    sedasi_tatacara:  $sedasi_tatacara
    sedasi_tujuan: $sedasi_tujuan
    sedasi_risiko:  $sedasi_risiko
    sedasi_komplikasi: $sedasi_komplikasi
    sedasi_prognosis: $sedasi_prognosis
    sedasi_lain: $sedasi_lain
    tt_dokter: $tt_dokter
    tt_pasien: $tt_pasien
    tt_saksi1: $tt_saksi1
    tt_saksi2: $tt_saksi2
  ){
    no_inform
    no_medrec
  }
}
`;

export const updateFlagReviewQuery = gql`
mutation updateFlagReviewQuery(
  $no_medrec: String
  $flagKaji: Int
){
  reviewFlagUpdate(
    no_medrec: $no_medrec
    flagKaji: $flagKaji
  ){
    no_medrec
  }
}
`;

export const updateKidFlagReviewQuery = gql`
mutation updateKidFlagReviewQuery(
  $no_medrec: String
  $flagKaji: String
){
  kidReviewFlagUpdate(
    no_medrec: $no_medrec
    flagKaji: $flagKaji
  ){
    no_medrec
  }
}
`;

export const getCTByDate = gql`
query getCTByDate($no_medrec:String, $tgl_ct:String){
  ctByDate(no_medrec:$no_medrec, tgl_ct:$tgl_ct) {
    id_ct
    no_medrec
    nama_pasien
    nodok
    nama_dok
    jaminan
    tgl_lahir
    umur
    kelamin
    kelas
    tgl_ct
    s_description
    s_stroke
    o_description
    o_stroke
    a_description
    a_stroke
    a_icd9
    a_icdx
    p_lab_description
    p_lab_stroke
    p_rad_description
    p_rad_stroke
    p_fisio_description
    p_fisio_stroke
    p_tindakan_description
    p_tindakan_stroke
    p_obat_description
    p_obat_stroke
    p_obat_nama
    p_obat_alergi
  }
}
`;

export const checkICD10 = gql`
query checkICD10($no_medrec:String, $a_icdx:String){
  checkICD10(no_medrec:$no_medrec, a_icdx:$a_icdx) {
    a_icdx,
    no_medrec
  }
}
`;


export const getIntegratedNote1Query = gql`
query IntegratedNote1Query(
  $no_medrec: String!
){
  ctByMedrec1(
    no_medrec: $no_medrec,
  ){
    id_ct
    no_medrec
    nama_pasien
    nodok
    nama_dok
    jaminan
    tgl_lahir
    umur
    kelamin
    kelas
    tgl_ct
    s_description
    s_stroke
    o_description
    o_stroke
    a_description
    a_stroke
    a_icd9
    a_icdx
    p_lab_description
    p_lab_stroke
    p_rad_description
    p_rad_stroke
    p_fisio_description
    p_fisio_stroke
    p_tindakan_description
    p_tindakan_stroke
    p_obat_description
    p_obat_stroke
    p_obat_nama
    p_obat_alergi
  }
}
`;

export const getIntegratedNoteNodok1Query = gql`
query getIntegratedNoteNodok1Query(
  $nodok: String
  $no_medrec: String
){
  ctByNodok1(
    nodok: $nodok,
    no_medrec: $no_medrec
  ){
    id_ct
    no_medrec
    nama_pasien
    nodok
    nama_dok
    jaminan
    tgl_lahir
    umur
    kelamin
    kelas
    tgl_ct
    s_description
    s_stroke
    o_description
    o_stroke
    a_description
    a_stroke
    a_icd9
    a_icdx
    p_lab_description
    p_lab_stroke
    p_rad_description
    p_rad_stroke
    p_fisio_description
    p_fisio_stroke
    p_tindakan_description
    p_tindakan_stroke
    p_obat_description
    p_obat_stroke
    p_obat_nama
    p_obat_alergi
  }
}
`;

export const getDataDoctor = gql`
query getDataDoctor($nama_dok:String!, $offset:Int, $limit:Int){
  doctorByName(nama_dok:$nama_dok, offset:$offset, limit:$limit){
    nodok,
    nama_dok,
    specialis
  }
}
`;


export const addConsultationQuery = gql`
mutation addConsultationQuery(
  $no_konsultasi: String,
  $no_medrec: String,
  $nodok: String,
  $id_ct: String,
  $alasan: String,
  $nodok_tujuan: String
){
  addConsultation(
    no_konsultasi: $no_konsultasi,
    no_medrec: $no_medrec,
    nodok:$nodok,
    id_ct: $id_ct,
    alasan:$alasan,
    nodok_tujuan: $nodok_tujuan,
  ){
    no_konsultasi
    no_medrec
    nodok
    id_ct
    alasan
    nodok_tujuan
  }
}
`;

export const getConsultantTujuan1Query = gql`
query getConsultantTujuan1Query($nodok_tujuan:String){
  consultationByTujuan1(nodok_tujuan:$nodok_tujuan) {
    no_konsultasi
    no_medrec
    nodok
    id_ct
    alasan
    nodok_tujuan
    id_ct_jawaban
    jawaban
  }
}
`;

export const getConsultantTujuanQuery = gql`
query getConsultantTujuanQuery($nodok_tujuan:String){
  consultationByTujuan(nodok_tujuan:$nodok_tujuan) {
    no_konsultasi
    no_medrec
    nodok
    id_ct
    alasan
    nodok_tujuan
    id_ct_jawaban
    jawaban
  }
}
`;

export const getConsultantNodok1Query = gql`
query getConsultantNodok1Query($no_medrec:String, $nodok:String){
  consultationByNodok1(no_medrec: $no_medrec, nodok:$nodok) {
    no_konsultasi
    no_medrec
    nodok
    id_ct
    alasan
    nodok_tujuan
    id_ct_jawaban
    jawaban
  }
}
`;

export const updateConsultationAnswerQuery = gql`
mutation updateConsultationAnswerQuery(
  $no_konsultasi: String,
  $id_ct_jawaban: String,
  $jawaban: String
){
  updateConsultationAnswer(
    no_konsultasi: $no_konsultasi,
    id_ct_jawaban: $id_ct_jawaban,
    jawaban:$jawaban
  ){
    no_konsultasi
    no_medrec
    nodok
    id_ct
    alasan
    nodok_tujuan
    id_ct_jawaban
    jawaban
  }
}
`;

export const getInformConsentQuery = gql`
query getInformConsentQuery($no_medrec:String){
  informConsent0(no_medrec:$no_medrec){
    no_inform
    no_medrec
    nama_ortu
    umur_ortu
    kelamin_ortu
    alamat
    ktp
    tgl_inform
    jam_inform
    yg_menyatakan
    file_yg_menyatakan
    dokter_operator
    file_dokter_operator
    saksi1
    file_saksi1
    saksi2
    file_saksi2
    tindakan_operasi
    tipe_tindakan
    pelaksana_tindakan
    pemberi_info
    penerima_info
    info_diag_kerja
    checklist_info_diag_kerja
    dasar_diag
    checklist_dasar_diag
    tindakan_dokter
    checklist_tindakan_dokter
    indikasi_tindakan
    checklist_indikasi_tindakan
    tatacara
    checklist_tatacara
    tujuan
    checklist_tujuan
    risiko
    checklist_risiko
    komplikasi
    checklist_komplikasi
    prognosis
    checklist_prognosis
    alternatif
    checklist_alternatif
    lain
    checklist_lain
    penandaan_operasi
    a_u_tindakan_dokter
    a_u_tatacara
    a_u_tujuan
    a_u_risiko
    a_u_komplikasi
    a_r_tindakan_dokter
    a_r_tatacara
    a_r_tujuan
    a_r_risiko
    a_r_komplikasi
    a_r_alternatif
    a_l_tindakan_dokter
    a_l_tatacara
    a_l_tujuan
    a_l_risiko
    a_l_komplikasi
    a_l_alternatif
    sedasi_tindakan_dokter
    sedasi_tatacara
    sedasi_tujuan
    sedasi_risiko
    sedasi_komplikasi
    sedasi_prognosis
    sedasi_lain
    flag
  }
}
`;

export const addInformConsentApprovalQuery = gql`
mutation addInformConsentApprovalQuery(
  $no_inform: String,
  $nama_ortu: String,
  $umur_ortu: String,
  $kelamin_ortu: String,
  $alamat:  String,
  $ktp: String,
  $tgl_inform: String,
  $jam_inform: Int,
  $yg_menyatakan:  String,
  $dokter_operator:  String,
  $saksi1:  String,
  $saksi2:  String,
  $tindakan_operasi:  String,
  $tipe_tindakan: String

){
  addInformConsentApproval(
    no_inform: $no_inform,
    nama_ortu: $nama_ortu,
    umur_ortu: $umur_ortu,
    kelamin_ortu: $kelamin_ortu,
    alamat:  $alamat,
    ktp: $ktp,
    tgl_inform: $tgl_inform,
    jam_inform: $jam_inform,
    yg_menyatakan:  $yg_menyatakan,
    dokter_operator:  $dokter_operator,
    saksi1:  $saksi1,
    saksi2:  $saksi2,
    tindakan_operasi:  $tindakan_operasi,
    tipe_tindakan: $tipe_tindakan

  ){
    no_inform
    nama_ortu
    umur_ortu
    kelamin_ortu
    alamat
    ktp
    tgl_inform
    jam_inform
    yg_menyatakan
    dokter_operator
    saksi1
    saksi2
    tindakan_operasi
    tipe_tindakan

  }
}
`;

export const addTaggingOperationQuery = gql`
mutation addTaggingOperationQuery(
  $no_inform: String,
  $penandaan_operasi:  String

){
  addTaggingOperation(
    no_inform: $no_inform,
    penandaan_operasi:  $penandaan_operasi

  ){
    no_inform
    penandaan_operasi
  }
}
`;

export const updateFlagInformQuery = gql`
mutation updateFlagInformQuery(
  $no_inform: String,
  $flag:  Int

){
  updateFlagInform(
    no_inform: $no_inform,
    flag:  $flag

  ){
    no_inform
    flag
  }
}
`;

export const informTypeByNameQuery = gql`
query informTypeByNameQuery($tipe_inform: String) {
  informTypeByName(tipe_inform: $tipe_inform) {
    id_type_inform
    tipe_inform
    diagnosis_kerja
    dasar_diagnosis
    tindakan_dokter
    indikasi_tindakan
    tatacara
    tujuan
    risiko
    komplikasi
    prognosis
    alternatif
    lainLain
  }
}
`;

export const addReferenceQuery = gql`
mutation addReferenceQuery(
  $id_rujukan: String,
  $nama_pasien: String,
  $no_medrec: String,
  $umur: String,
  $kelamin: String,
  $tgl: String,
  $nama_dok: String,
  $jaminan: String,
  $kelas: String,
  $tipe_rujukan: String,
  $diagnosa: String,
  $indikasi: String,
  $keterangan: String,
  $lab_hematologi: String,
  $lab_urinalisa: String,
  $lab_faeces: String,
  $lab_gula_darah: String,
  $lab_kimia_darah: String,
  $lab_berologi: String,
  $lab_mikrobiologi: String,
  $lab_lain: String,
  $rad_tanpa_kontras: String,
  $rad_kontras: String,
  $fisio_tindakan: String
){
  addReference(
    id_rujukan: $id_rujukan,
    nama_pasien: $nama_pasien,
    no_medrec: $no_medrec,
    umur: $umur,
    kelamin: $kelamin,
    tgl: $tgl,
    nama_dok: $nama_dok,
    jaminan: $jaminan,
    kelas: $kelas,
    tipe_rujukan: $tipe_rujukan,
    diagnosa: $diagnosa,
    indikasi: $indikasi,
    keterangan: $keterangan,
    lab_hematologi: $lab_hematologi,
    lab_urinalisa: $lab_urinalisa,
    lab_faeces: $lab_faeces,
    lab_gula_darah: $lab_gula_darah,
    lab_kimia_darah: $lab_kimia_darah,
    lab_berologi: $lab_berologi,
    lab_mikrobiologi: $lab_mikrobiologi,
    lab_lain: $lab_lain,
    rad_tanpa_kontras: $rad_tanpa_kontras,
    rad_kontras: $rad_kontras,
    fisio_tindakan: $fisio_tindakan
  ){
    id_rujukan
    nama_pasien
    no_medrec
    umur
    kelamin
    tgl
    nama_dok
    jaminan
    kelas
    tipe_rujukan
    diagnosa
    indikasi
    keterangan
    lab_hematologi
    lab_urinalisa
    lab_faeces
    lab_gula_darah
    lab_kimia_darah
    lab_berologi
    lab_mikrobiologi
    lab_lain
    rad_tanpa_kontras
    rad_kontras
    fisio_tindakan
  }
}
`;

export const PatientByIdQuery = gql`
query PatientByIdQuery($no_medrec:String!) {
  patientByNorm(no_medrec: $no_medrec) {
    no_medrec
    no_ktp2
    nama_penjamin
    jln2
    desa2
    no2
    rt2
    rw2
    kecamatan2
    kota2
    tgl2
    bln2
    tahun2
  }
}
`;

export const examineDataExchangeQuery = gql`
subscription examineDataExchangeQuery($nodok: String!, $flag: Int!) {
  examineDataExchange(nodok: $nodok, flag: $flag) {
    no_medrec
    nodok
  }
}
`;


export const reviewDoctorAssessmentQuery = gql`
mutation reviewDoctorAssessmentQuery(
    $no_kaji: String
    $nama_dok: String
    $nodok: String
    $anamnesa_keluhan: String
    $anamnesa_riwayat_penyakit: String
    $fisik_kepala: String
    $fisik_leher: String
    $fisik_mulut: String
    $fisik_paru: String
    $fisik_genitalia: String
    $fisik_mata: String
    $fisik_tht: String
    $fisik_jantung: String
    $fisik_abdomen: String
    $fisik_ekstremitas: String
    $status_gizi: String
    $pemeriksaan_gigi: String
    $pemeriksaan_gigi_ket: String
    $status_lokalis: String
    $pemeriksaan_penunjang: String
    $diagnosis: String
    $rencana: String
    $dirujuk: String
    $dirujuk_lainnya: String
    $tt_dokter: String
){
  reviewDoctorAssessment(
    no_kaji: $no_kaji
    nama_dok: $nama_dok
    nodok: $nodok
    anamnesa_keluhan: $anamnesa_keluhan
    anamnesa_riwayat_penyakit: $anamnesa_riwayat_penyakit
    fisik_kepala: $fisik_kepala
    fisik_leher: $fisik_leher
    fisik_mulut: $fisik_mulut
    fisik_paru: $fisik_paru
    fisik_genitalia: $fisik_genitalia
    fisik_mata: $fisik_mata
    fisik_tht: $fisik_tht
    fisik_jantung: $fisik_jantung
    fisik_abdomen: $fisik_abdomen
    fisik_ekstremitas: $fisik_ekstremitas
    status_gizi: $status_gizi
    pemeriksaan_gigi: $pemeriksaan_gigi
    pemeriksaan_gigi_ket: $pemeriksaan_gigi_ket
    status_lokalis: $status_lokalis
    pemeriksaan_penunjang: $pemeriksaan_penunjang
    diagnosis: $diagnosis
    rencana: $rencana
    dirujuk: $dirujuk
    dirujuk_lainnya: $dirujuk_lainnya
    tt_dokter: $tt_dokter
  )
{
    no_kaji
    no_medrec
  }
}
`;

export const setQueueFlagQuery = gql`
mutation setQueueFlagQuery($no_medrec: String!, $flag: Int!) {
setQueueFlag(no_medrec: $no_medrec, flag: $flag) {
  kode_unit,
  poli,
  nodok,
  nama_dok,
  no_registrasi,
  no_medrec,
  nama_pasien,
  no_antrian,
  jaminan,
  umur,
  tgl_lahir,
  kelamin,
  status_pasien,
  kaji,
  flag
}
}
`;

export const MasterDataLabQuery = gql`
query MasterDataLabQuery {
  masterLabAll {
    id_lab
    nama_pemeriksaan
    kelompok
  }
}
`;

export const MasterDataRadioQuery = gql`
query MasterDataRadioQuery {
  masterRadiologiAll {
    id_radiologi
    nama_pemeriksaan
    kelompok
  } 
}
`;

export const addLabQuery = gql`
mutation addLabQuery(
  $id_integrasi: String!,
  $no_medrec: String!, 
  $indikasi: String!,
  $diagnosis: String!,
  $lab: String!,
  $tgl_permintaan:String!) {
addLab(
  id_integrasi: $id_integrasi,
  no_medrec: $no_medrec, 
  indikasi: $indikasi,
  diagnosis: $diagnosis,
  lab: $lab,
  tgl_permintaan: $tgl_permintaan
) {
  no_medrec
  indikasi
  diagnosis
  lab
  tgl_permintaan
}
}
`;

export const addRadiologyQuery = gql`
mutation addRadiologyQuery(
  $id_integrasi: String!,
  $no_medrec: String!, 
  $indikasi: String!,
  $diagnosis: String!,
  $radiologi: String!,
  $tgl_permintaan:String!) {
addRadio(
  id_integrasi: $id_integrasi,
  no_medrec: $no_medrec, 
  indikasi: $indikasi,
  diagnosis: $diagnosis,
  radiologi: $radiologi,
  tgl_permintaan: $tgl_permintaan
) {
  no_medrec
  indikasi
  diagnosis
  radiologi
  tgl_permintaan
}
}
`;

export const addFisioQuery = gql`
mutation addFisioQuery(
  $id_integrasi: String!,
  $no_medrec: String!, 
  $indikasi: String!,
  $diagnosis: String!,
  $fisioterapi: String!,
  $tgl_permintaan:String!) {
addFisio(
  id_integrasi: $id_integrasi,
  no_medrec: $no_medrec, 
  indikasi: $indikasi,
  diagnosis: $diagnosis,
  fisioterapi: $fisioterapi,
  tgl_permintaan: $tgl_permintaan
) {
  no_medrec
  indikasi
  diagnosis
  fisioterapi
  tgl_permintaan
}
}
`;