var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AuthService } from './../services/providers/auth-service';
import { Component, ViewChild } from '@angular/core';
import { AlertController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth, storage, alertCtrl, menuCtrl) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
        this.rootPage = 'Login';
        // this.initializeApp();
        this.pages = [
            { title: 'Queue', icon: 'clipboard' },
            { title: 'HomePage', icon: 'eye' },
            { title: 'Logout', icon: 'log-out' }
        ];
    }
    /**
     * set statusbar with default and hide the splashscreen
     *
     * @memberof MyApp
     */
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    /**
     * switch menu for navigation drawer
     *
     * @param {any} page
     * @memberof MyApp
     */
    MyApp.prototype.openPage = function (page) {
        if (page == "Logout") {
            this.logout();
        }
        else {
            this.nav.setRoot(page);
        }
    };
    /**
     * function logout app with validation user
     *
     * @memberof MyApp
     */
    MyApp.prototype.logout = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            subTitle: "Apakah anda yakin ingin keluar?",
            buttons: [
                {
                    text: 'YA',
                    role: 'YA',
                    handler: function () {
                        _this.auth.logout().subscribe(function (out) {
                            _this.menuCtrl.enable(false);
                            _this.nav.setRoot('Login');
                        });
                    }
                },
                {
                    text: 'TIDAK',
                    role: 'TIDAK',
                }
            ]
        });
        alert.present();
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            AuthService,
            Storage,
            AlertController,
            MenuController])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map