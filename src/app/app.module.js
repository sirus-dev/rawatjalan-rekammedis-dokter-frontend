var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
//component
import { MyApp } from './app.component';
//ionic-native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//module
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
//service
import { DokterService } from './../services/providers/dokter-service';
import { AuthService } from './../services/providers/auth-service';
//apollo-client
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import { ApolloModule } from 'apollo-angular';
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';
//Create Apollo Network to Server
var networkInterface = createNetworkInterface({ uri: '/api/graphql',
    opts: { mode: 'cors' } });
// set ws protocol when using http and wss when using https
var protocol = window.location.protocol.replace('http', 'ws');
// get location host
var host = window.location.host;
//Create WebSocket client
var wsClient = new SubscriptionClient("ws://104.248.151.31:14113/subscriptions", {
    reconnect: true
});
//network handler
networkInterface.useAfter([{
        applyAfterware: function (_a, next) {
            var response = _a.response;
            if (response.status === 401) {
                console.log("Network Error");
            }
            next();
        }
    }]);
// Extend the network interface with the WebSocket
var networkInterfaceWithSubscriptions = addGraphQLSubscriptions(networkInterface, wsClient);
//create Apollo Client
var client = new ApolloClient({
    networkInterface: networkInterfaceWithSubscriptions
});
export function provideClient() {
    return client;
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                HttpModule,
                IonicModule.forRoot(MyApp),
                IonicImageViewerModule,
                IonicStorageModule.forRoot(),
                ApolloModule.forRoot(provideClient),
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp
            ],
            providers: [
                StatusBar,
                SplashScreen,
                // {provide: ErrorHandler, useClass: IonicErrorHandler},
                AuthService,
                DokterService
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map