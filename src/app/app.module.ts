//component
import { MyApp } from './app.component';
import { BandingRad } from '../pages/radiology/bandingrad';

//ionic-native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//module
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

//service
import { DokterService } from './../services/providers/dokter-service';
import { AuthService } from './../services/providers/auth-service';

//apollo-client
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import { ApolloModule } from 'apollo-angular';
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';

//Create Apollo Network to Server
const networkInterface = createNetworkInterface(
  { uri: '/api/graphql', 
    opts: { mode: 'cors'} });

// set ws protocol when using http and wss when using https
const protocol = window.location.protocol.replace('http', 'ws');
// get location host
const host = window.location.host;  
//Create WebSocket client
const wsClient = new SubscriptionClient(`ws://104.248.151.31:14113/subscriptions`, {
  reconnect: true
});

//network handler
networkInterface.useAfter([{
  applyAfterware({ response }, next) {
    if (response.status === 401) {
      console.log("Network Error");
    }
    next();
  }
}]);

// Extend the network interface with the WebSocket
const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
  networkInterface,
  wsClient
);

//create Apollo Client
const client = new ApolloClient({
  networkInterface: networkInterfaceWithSubscriptions
});

export function provideClient(): ApolloClient {
  return client;
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    IonicStorageModule.forRoot(),
    ApolloModule.forRoot(provideClient),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    DokterService
  ]
})
export class AppModule {}
