import { AuthService } from './../services/providers/auth-service';
import { Component, ViewChild } from '@angular/core';
import { AlertController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular/components/app/menu-controller';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title:string, icon:string}>;
  rootPage = 'Login';

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen, 
              private auth:AuthService,
              public storage: Storage,
              public alertCtrl: AlertController,
              public menuCtrl: MenuController) {
    // this.initializeApp();
    this.pages = [
      { title: 'Queue', icon: 'clipboard' },
      { title: 'HomePage', icon: 'eye' },
      { title: 'Logout',   icon: 'log-out' }
    ];
  }

  /**
   * set statusbar with default and hide the splashscreen
   * 
   * @memberof MyApp
   */
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * switch menu for navigation drawer
   * 
   * @param {any} page 
   * @memberof MyApp
   */
  openPage(page) {
    if(page == "Logout"){
      this.logout();
    }else{
      this.nav.setRoot(page);
    }
  }

  /**
   * function logout app with validation user
   * 
   * @memberof MyApp
   */
  logout(){
    let alert = this.alertCtrl.create({
      subTitle: "Apakah anda yakin ingin keluar?",
      buttons: [
        {
          text: 'YA',
          role: 'YA',
          handler: () => {
            this.auth.logout().subscribe(out => {
              this.menuCtrl.enable(false);
              this.nav.setRoot('Login');
            });
          }
        },
        {
          text: 'TIDAK',
          role: 'TIDAK',
        }
      ]
    });
    alert.present();
  }
}
 