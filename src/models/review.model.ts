export class Review {
    td: string;
    tb: string;
    bb: string;
    keluhan: string;
}

export class KidReview {
    td1:string;
    td2:string;
    tb:string;
    bb: string;
    riwayatAlergiObatKet: string;
    riwayatAlergiMakananKet: string;
    riwayatAlergiPantangan: string;
}