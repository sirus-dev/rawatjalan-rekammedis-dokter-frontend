export class Patient {
    no_periksa:string;
    poli:string;
    nama_pasien: string;
    no_medrec: string;
    nodok: string;
    // no_antrian: string;
    jaminan: string;
    umur: number;
    tgl_lahir: string;
    kelamin: string;
}