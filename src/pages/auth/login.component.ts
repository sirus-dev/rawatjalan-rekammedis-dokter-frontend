import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { AuthService } from './../../services/providers/auth-service';
import { AlertController, Loading, LoadingController, NavController, IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DokterService } from '../../services/providers/dokter-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.component.html'
})

export class Login {
  loading: Loading;
  registerCredentials = { email: '', password: '' };
  private loginForm: FormGroup;

  constructor(public nav: NavController,
    private auth: AuthService,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private formBuilder: FormBuilder,
    private dokterService: DokterService,
    private menuCtrl: MenuController) {

    this.loginForm = this.formBuilder.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required],
    });
  }

  /**
   * check if token is save to storage and the role is dokter
   * 
   * @memberof MyApp
   */
  ngOnInit() {
    this.storage.get('token')
      .then((token) => {
        if (token) {
          return this.storage.get('role');
        }
        return false;
      })
      .then((role) => {
        this.menuCtrl.enable(role);
        if (role) {
          this.nav.setRoot('Queue');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  /**
   * function validation email and password afterthat hit to api
   * 
   * @param {any} formData 
   * @memberof Login
   */
  login(formData) {
    if (this.loginForm.valid) {
      this.showLoading();
      this.auth.login(formData)
        .subscribe(
          data => this.isLogin(data),
          error => this.logError(error)
        );
    } else {
      this.showError("Mohon lengkapi email atau password");
    }
  }

  /**
   * check if login success
   * 
   * @param {any} data 
   * @returns 
   * @memberof Login
   */
  isLogin(data) {
    if (data.success) {
      this.auth.saveToken(data, this.registerCredentials).subscribe(
        data => this.checkRole(),
        error => this.showError("Anda bukan Dokter")
      );
    } else {
      return this.showError("Silakan cek kembali email dan password anda");
    }
  }

  /**
   * after save token and role check if role is dokter
   * 
   * @memberof Login
   */
  checkRole() {
    this.storage.get('token').then((data) => {
      if (data) {
        this.storage.get('role').then((data) => {
          if (data == "dokter") {
            return this.nav.setRoot('Queue');
          } else {
            return this.showError("Anda bukan Dokter");
          }
        });
      } else {
        return this.showError("Silakan cek kembali email dan password anda");
      }

    });
  }

  /**
   * template show loading
   * 
   * @memberof Login
   */
  private showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait . . . ',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  /**
   * template show error
   * 
   * @param {any} error 
   * @memberof Login
   */
  private showError(error) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Gagal Login',
      subTitle: error,
      buttons: ['OK']
    });
    alert.present();
  }

  /**
   * template log error
   * 
   * @param {any} data 
   * @returns 
   * @memberof Login
   */
  private logError(data) {
    return this.showError("Silakan cek kembali email dan password anda");
  }

  /**
   * redirect to forget page
   * 
   * @memberof Login
   */
  goToForgetPage() {
    this.nav.push('Forget');
  }

}