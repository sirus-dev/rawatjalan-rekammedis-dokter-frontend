import { AuthService } from '../../services/providers/auth-service';
import { Loading, NavController, AlertController, LoadingController, IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-forget',
  templateUrl: 'forget.component.html'
})

export class Forget {
  loading: Loading;
  forgetCredentials = { email: '' };

  constructor(public nav: NavController,
    private auth: AuthService,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public storage: Storage) { }

  /**
   * hit to api forget password
   * 
   * @memberof Forget
   */
  forgetPassword() {
    this.auth.forgetPass(this.forgetCredentials).subscribe(
      data => this.isSendEmail(data),
      error => console.log(error)
    );
  }

  /**
   * check if email forget password is valid
   * 
   * @param {any} data 
   * @memberof Forget
   */
  isSendEmail(data) {
    if (data.success) {
      let alert = this.alertCtrl.create({
        title: 'Berhasil Kirim Email',
        subTitle: 'Mohon untuk cek email anda untuk proses selanjutnya',
        buttons: [{
          text: 'OK',
          role: 'OK',
          handler: () => {
            this.goToLogin();
          }
        }]
      });
      alert.present();
    } else {
      this.auth.showError('Email yang anda masukkan tidak terdaftar');
    }
  }

  /**
   * redirect to login page
   * 
   * @memberof Forget
   */
  goToLogin() {
    this.nav.push('Login');
  }

}