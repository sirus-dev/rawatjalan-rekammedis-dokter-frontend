import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { Forget } from './forget.component';
@NgModule({
  declarations: [
    Forget
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Forget)
  ]
})
export class ForgetModule {}
