var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AuthService } from '../../services/providers/auth-service';
import { NavController, AlertController, LoadingController, IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
var Forget = /** @class */ (function () {
    function Forget(nav, auth, alertCtrl, loadingCtrl, storage) {
        this.nav = nav;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.forgetCredentials = { email: '' };
    }
    /**
     * hit to api forget password
     *
     * @memberof Forget
     */
    Forget.prototype.forgetPassword = function () {
        var _this = this;
        this.auth.forgetPass(this.forgetCredentials).subscribe(function (data) { return _this.isSendEmail(data); }, function (error) { return console.log(error); });
    };
    /**
     * check if email forget password is valid
     *
     * @param {any} data
     * @memberof Forget
     */
    Forget.prototype.isSendEmail = function (data) {
        var _this = this;
        if (data.success) {
            var alert_1 = this.alertCtrl.create({
                title: 'Berhasil Kirim Email',
                subTitle: 'Mohon untuk cek email anda untuk proses selanjutnya',
                buttons: [{
                        text: 'OK',
                        role: 'OK',
                        handler: function () {
                            _this.goToLogin();
                        }
                    }]
            });
            alert_1.present();
        }
        else {
            this.auth.showError('Email yang anda masukkan tidak terdaftar');
        }
    };
    /**
     * redirect to login page
     *
     * @memberof Forget
     */
    Forget.prototype.goToLogin = function () {
        this.nav.push('Login');
    };
    Forget = __decorate([
        IonicPage(),
        Component({
            selector: 'page-forget',
            templateUrl: 'forget.component.html'
        }),
        __metadata("design:paramtypes", [NavController,
            AuthService,
            AlertController,
            LoadingController,
            Storage])
    ], Forget);
    return Forget;
}());
export { Forget };
//# sourceMappingURL=forget.component.js.map