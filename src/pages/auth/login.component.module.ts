import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { Login } from './login.component';
@NgModule({
  declarations: [
    Login
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Login)
  ]
})
export class LoginModule {}
