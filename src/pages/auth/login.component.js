var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { AuthService } from './../../services/providers/auth-service';
import { AlertController, LoadingController, NavController, IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
import { DokterService } from '../../services/providers/dokter-service';
var Login = /** @class */ (function () {
    function Login(nav, auth, alertCtrl, loadingCtrl, storage, formBuilder, dokterService, menuCtrl) {
        this.nav = nav;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.dokterService = dokterService;
        this.menuCtrl = menuCtrl;
        this.registerCredentials = { email: '', password: '' };
        this.loginForm = this.formBuilder.group({
            'email': ['', Validators.required],
            'password': ['', Validators.required],
        });
    }
    /**
     * check if token is save to storage and the role is dokter
     *
     * @memberof MyApp
     */
    Login.prototype.ngOnInit = function () {
        var _this = this;
        this.storage.get('token')
            .then(function (token) {
            if (token) {
                return _this.storage.get('role');
            }
            return false;
        })
            .then(function (role) {
            _this.menuCtrl.enable(role);
            if (role) {
                _this.nav.setRoot('Queue');
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    /**
     * function validation email and password afterthat hit to api
     *
     * @param {any} formData
     * @memberof Login
     */
    Login.prototype.login = function (formData) {
        var _this = this;
        if (this.loginForm.valid) {
            this.showLoading();
            this.auth.login(formData)
                .subscribe(function (data) { return _this.isLogin(data); }, function (error) { return _this.logError(error); });
        }
        else {
            this.showError("Mohon lengkapi email atau password");
        }
    };
    /**
     * check if login success
     *
     * @param {any} data
     * @returns
     * @memberof Login
     */
    Login.prototype.isLogin = function (data) {
        var _this = this;
        if (data.success) {
            this.auth.saveToken(data, this.registerCredentials).subscribe(function (data) { return _this.checkRole(); }, function (error) { return _this.showError("Anda bukan Dokter"); });
        }
        else {
            return this.showError("Silakan cek kembali email dan password anda");
        }
    };
    /**
     * after save token and role check if role is dokter
     *
     * @memberof Login
     */
    Login.prototype.checkRole = function () {
        var _this = this;
        this.storage.get('token').then(function (data) {
            if (data) {
                _this.storage.get('role').then(function (data) {
                    if (data == "dokter") {
                        return _this.nav.setRoot('Queue');
                    }
                    else {
                        return _this.showError("Anda bukan Dokter");
                    }
                });
            }
            else {
                return _this.showError("Silakan cek kembali email dan password anda");
            }
        });
    };
    /**
     * template show loading
     *
     * @memberof Login
     */
    Login.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait . . . ',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    /**
     * template show error
     *
     * @param {any} error
     * @memberof Login
     */
    Login.prototype.showError = function (error) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Gagal Login',
            subTitle: error,
            buttons: ['OK']
        });
        alert.present();
    };
    /**
     * template log error
     *
     * @param {any} data
     * @returns
     * @memberof Login
     */
    Login.prototype.logError = function (data) {
        return this.showError("Silakan cek kembali email dan password anda");
    };
    /**
     * redirect to forget page
     *
     * @memberof Login
     */
    Login.prototype.goToForgetPage = function () {
        this.nav.push('Forget');
    };
    Login = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.component.html'
        }),
        __metadata("design:paramtypes", [NavController,
            AuthService,
            AlertController,
            LoadingController,
            Storage,
            FormBuilder,
            DokterService,
            MenuController])
    ], Login);
    return Login;
}());
export { Login };
//# sourceMappingURL=login.component.js.map