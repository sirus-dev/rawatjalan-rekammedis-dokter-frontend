import { Queue } from './queue.component';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Queue
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Queue)
  ]
})
export class QueueModule {}
