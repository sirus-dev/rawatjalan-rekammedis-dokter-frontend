import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { IonicPage, MenuController, NavParams, ToastController, ViewController, AlertController } from 'ionic-angular';
import moment from 'moment';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/providers/auth-service';

@IonicPage()
@Component({
  selector: 'queue-page',
  templateUrl: 'queue.component.html'
})

export class Queue {

  listQueue: Array<any> = [];
  myDate: String = new Date().toISOString();
  btnDisabled = false;
  clicked = false;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    private dokterService: DokterService,
    public params: NavParams,
    private storage: Storage,
    private menuCtrl: MenuController,
    private auth: AuthService,
    public alertCtrl: AlertController) {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.loadData();
  }

  /**
   * to load queue of patients who have been registered in the poly
   * 
   * @memberof Queue
   */
  loadData() {
    this.storage.get('nodok').then((data) => {
      this.dokterService.getListPatient(data).subscribe(({ data }) => {
        this.listQueue = data.queueAllToday;
      });
    });
  }

  /**
   * if the patient's flag is 4 or 5 in the list of patient will be disabled
   * 
   * @param {any} datapatient 
   * @returns 
   * @memberof Queue
   */
  statusDisabled(datapatient) {
    if (datapatient.flag == '4' || datapatient.flag == '5') {
      return true;
    }
    return false;
  }

  /**
   * template toast message
   * 
   * @param {string} message 
   * @param {number} duration 
   * @param {string} position 
   * @memberof Queue
   */
  toastMessage(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });
    toast.present(toast);
    this.viewCtrl.dismiss();
  }

  /**
   * template for mapping date
   * 
   * @private
   * @param {string} date 
   * @returns {string} 
   * @memberof Queue
   */
  private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
  }

  /**
   * if patient's flag 2 will be entered the home
   * 
   * @param {any} data 
   * @memberof Queue
   */
  goToHome(data) {
    if (data.flag == 2) {
      this.navCtrl.setRoot('HomePage');
    }
  }

  /**
   * for refreshing queue of patient
   * 
   * @memberof Queue
   */
  refreshData() {
    this.loadData();
  }

  /**
   * function logout app with validation user
   * 
   * @memberof Queue
   */
  logoutUser() {
    let alert = this.alertCtrl.create({
      subTitle: "Apakah anda yakin ingin keluar?",
      buttons: [
        {
          text: 'YA',
          role: 'YA',
          handler: () => {
            this.auth.logout().subscribe(out => {
              this.menuCtrl.enable(false);
              this.navCtrl.setRoot('Login');
            });
          }
        },
        {
          text: 'TIDAK',
          role: 'TIDAK',
        }
      ]
    });
    alert.present();
  }
}