var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { IonicPage, MenuController, NavParams, ToastController, ViewController, AlertController } from 'ionic-angular';
import moment from 'moment';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/providers/auth-service';
var Queue = /** @class */ (function () {
    function Queue(navCtrl, viewCtrl, toastCtrl, dokterService, params, storage, menuCtrl, auth, alertCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.params = params;
        this.storage = storage;
        this.menuCtrl = menuCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.listQueue = [];
        this.myDate = new Date().toISOString();
        this.btnDisabled = false;
        this.clicked = false;
        this.menuCtrl.enable(false);
    }
    Queue.prototype.ngOnInit = function () {
        this.loadData();
    };
    /**
     * to load queue of patients who have been registered in the poly
     *
     * @memberof Queue
     */
    Queue.prototype.loadData = function () {
        var _this = this;
        this.storage.get('nodok').then(function (data) {
            _this.dokterService.getListPatient(data).subscribe(function (_a) {
                var data = _a.data;
                _this.listQueue = data.queueAllToday;
            });
        });
    };
    /**
     * if the patient's flag is 4 or 5 in the list of patient will be disabled
     *
     * @param {any} datapatient
     * @returns
     * @memberof Queue
     */
    Queue.prototype.statusDisabled = function (datapatient) {
        if (datapatient.flag == '4' || datapatient.flag == '5') {
            return true;
        }
        return false;
    };
    /**
     * template toast message
     *
     * @param {string} message
     * @param {number} duration
     * @param {string} position
     * @memberof Queue
     */
    Queue.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
        this.viewCtrl.dismiss();
    };
    /**
     * template for mapping date
     *
     * @private
     * @param {string} date
     * @returns {string}
     * @memberof Queue
     */
    Queue.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    /**
     * if patient's flag 2 will be entered the home
     *
     * @param {any} data
     * @memberof Queue
     */
    Queue.prototype.goToHome = function (data) {
        if (data.flag == 2) {
            this.navCtrl.setRoot('HomePage');
        }
    };
    /**
     * for refreshing queue of patient
     *
     * @memberof Queue
     */
    Queue.prototype.refreshData = function () {
        this.loadData();
    };
    /**
     * function logout app with validation user
     *
     * @memberof Queue
     */
    Queue.prototype.logoutUser = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            subTitle: "Apakah anda yakin ingin keluar?",
            buttons: [
                {
                    text: 'YA',
                    role: 'YA',
                    handler: function () {
                        _this.auth.logout().subscribe(function (out) {
                            _this.menuCtrl.enable(false);
                            _this.navCtrl.setRoot('Login');
                        });
                    }
                },
                {
                    text: 'TIDAK',
                    role: 'TIDAK',
                }
            ]
        });
        alert.present();
    };
    Queue = __decorate([
        IonicPage(),
        Component({
            selector: 'queue-page',
            templateUrl: 'queue.component.html'
        }),
        __metadata("design:paramtypes", [NavController,
            ViewController,
            ToastController,
            DokterService,
            NavParams,
            Storage,
            MenuController,
            AuthService,
            AlertController])
    ], Queue);
    return Queue;
}());
export { Queue };
//# sourceMappingURL=queue.component.js.map