import { Lab } from './lab.component';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Lab
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Lab)
  ]
})
export class LabModule {}
