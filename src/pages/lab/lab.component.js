var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';
import groupBy from 'lodash.groupby';
var Lab = /** @class */ (function () {
    function Lab(viewCtrl, toastCtrl, dokterService, params) {
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.params = params;
        this.datapersonal = [];
        this.myDate = new Date().toISOString();
        this.diagnosa = false;
        this.listAddLab = [];
        this.checkedItems = [false];
        this.listRequestLab = [];
        if (params.get('icdx').length > 0) {
            this.listicd = params.get('icdx');
            this.diagnosa = true;
        }
        this.datapersonal = this.params.get('dataPatient');
        this.doctorname = this.params.get('doctorName');
    }
    Lab.prototype.ngOnInit = function () {
        var _this = this;
        this.dokterService.loadDataMasterLab().subscribe(function (_a) {
            var data = _a.data;
            var mapDataLab = groupBy(data.masterLabAll, 'kelompok');
            _this.listDataLab = mapDataLab;
            _this.keyDataLab = Object.keys(mapDataLab);
        });
    };
    Lab.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
        this.viewCtrl.dismiss();
    };
    Lab.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    Lab.prototype.addLab = function (data, id, i) {
        var isChecked = false;
        if (this.listAddLab.length > 0) {
            var findarray = this.listAddLab.indexOf(data) > -1;
            var indexarray = this.listAddLab.findIndex(function (x) { return x.id_lab == id; });
            if (findarray == true) {
                this.listAddLab.splice(indexarray, 1);
            }
            else {
                this.listAddLab.push(data);
            }
        }
        else {
            this.listAddLab.push(data);
        }
    };
    Lab.prototype.submitLab = function () {
        var _this = this;
        var datenow = moment().format('DD-MM-YYYY');
        var datenowstr = moment().format('DDMMYYYY');
        if (this.indikasi) {
            this.listRequestLab = [{
                    id_integrasi: this.datapersonal[0].no_medrec + datenowstr,
                    no_medrec: this.datapersonal[0].no_medrec,
                    indikasi: this.indikasi,
                    diagnosis: '',
                    lab: JSON.stringify(this.listAddLab),
                    tgl_permintaan: datenow
                }];
        }
        else if (this.listicd) {
            this.listRequestLab = [{
                    id_integrasi: this.datapersonal[0].no_medrec + datenowstr,
                    no_medrec: this.datapersonal[0].no_medrec,
                    indikasi: '',
                    diagnosis: JSON.stringify(this.listicd),
                    lab: JSON.stringify(this.listAddLab),
                    tgl_permintaan: datenow
                }];
        }
        console.log(this.listRequestLab);
        this.dokterService.addLab(this.listRequestLab[0]).subscribe(function (_a) {
            var data = _a.data;
            console.log(data);
            _this.toastMessage('Permintaan Lab berhasil terkirim', 4000, 'top');
        });
    };
    Lab.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Lab = __decorate([
        IonicPage(),
        Component({
            selector: 'lab-modal',
            templateUrl: 'lab.component.html'
        }),
        __metadata("design:paramtypes", [ViewController, ToastController, DokterService, NavParams])
    ], Lab);
    return Lab;
}());
export { Lab };
//# sourceMappingURL=lab.component.js.map