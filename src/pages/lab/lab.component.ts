import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';
import groupBy from 'lodash.groupby';

@IonicPage()
@Component({
    selector: 'lab-modal',
    templateUrl: 'lab.component.html'
})

export class Lab {
 datapersonal: Array<any> = [];
 doctorname: any;
 myDate: String = new Date().toISOString();
 personalTglLahir:string;
 listicd: any;
 diagnosa = false;
 indikasi: string;

 keyDataLab: any;
 listDataLab: any;

 listAddLab: Array<any> = [];
 
 checkedItems: boolean[] = [false];

 listRequestLab: Array<any> = [];

 constructor(public viewCtrl: ViewController, public toastCtrl: ToastController, private dokterService: DokterService, public params: NavParams ) {

  if(params.get('icdx').length > 0){
    this.listicd = params.get('icdx');
    this.diagnosa = true;
  }
  this.datapersonal = this.params.get('dataPatient');
  this.doctorname = this.params.get('doctorName');
 }

 ngOnInit() {
    this.dokterService.loadDataMasterLab().subscribe(({data}) => {
      const mapDataLab = groupBy(data.masterLabAll, 'kelompok');
      this.listDataLab = mapDataLab;
      this.keyDataLab = Object.keys(mapDataLab);
    });
 }

 toastMessage(message:string,duration:number,position:string){
    let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        position: position
    });
    toast.present(toast);
    this.viewCtrl.dismiss();
 }

 private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
 }

 addLab(data,id,i:number) {
   let isChecked = false;
   if(this.listAddLab.length > 0) {
   let findarray = this.listAddLab.indexOf(data) > -1;
   let indexarray = this.listAddLab.findIndex(x => x.id_lab == id);
    if(findarray == true) {
      this.listAddLab.splice(indexarray, 1);
    } else {
      this.listAddLab.push(data);
    }
   } else {
    this.listAddLab.push(data);
   }
 }

 submitLab() {
    const datenow = moment().format('DD-MM-YYYY');
    const datenowstr = moment().format('DDMMYYYY');
    if(this.indikasi){
      this.listRequestLab = [{
        id_integrasi:this.datapersonal[0].no_medrec+datenowstr,
        no_medrec:this.datapersonal[0].no_medrec,
        indikasi:this.indikasi,
        diagnosis: '',
        lab: JSON.stringify(this.listAddLab),
        tgl_permintaan: datenow
      }];
    } else if (this.listicd) {
      this.listRequestLab = [{
        id_integrasi:this.datapersonal[0].no_medrec+datenowstr,
        no_medrec:this.datapersonal[0].no_medrec,
        indikasi:'',
        diagnosis: JSON.stringify(this.listicd),
        lab: JSON.stringify(this.listAddLab),
        tgl_permintaan: datenow
      }];
    }
      console.log(this.listRequestLab);
      this.dokterService.addLab(this.listRequestLab[0]).subscribe(({data}) => {
        console.log(data);
        this.toastMessage('Permintaan Lab berhasil terkirim',4000,'top');
      });
 }

 dismiss() {
   this.viewCtrl.dismiss();
 }

}
