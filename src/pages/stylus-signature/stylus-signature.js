var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { InkGrabber } from '@sirus/stylus';
import { animate, Component, EventEmitter, Input, Output, ViewChild, style, transition, trigger, } from '@angular/core';
import { StylusComponent } from '@sirus/stylus';
var StylusSignature = /** @class */ (function () {
    function StylusSignature() {
        this.arrayStroke = [];
        this.tempX = [];
        this.tempY = [];
        this.tempT = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.boolUndo = false;
        this.savedDraw = false;
        this.stylusChangedTag = new EventEmitter();
        this.change = {
            canRedo: false,
            canUndo: false,
            redoLength: 0,
            undoLength: 0
        };
        this.pen = {
            color: '#000',
            width: 2
        };
        this.width = 570;
        this.height = 500;
        this.timeout = 1;
        this.components = [];
    }
    StylusSignature.prototype.clear = function () {
        this.stylus.clear();
    };
    StylusSignature.prototype.undo = function () {
        if (this.change.canUndo) {
            this.stylus.undo();
        }
    };
    StylusSignature.prototype.redo = function () {
        if (this.change.canRedo && this.change.canUndo) {
            this.stylus.redo();
        }
    };
    StylusSignature.prototype.writeDiagnosis = function (text) {
        this.diagnosis = text;
    };
    StylusSignature.prototype.inkChange = function (change) {
        if (this.change.undoLength == 0) {
            this.arrayX = [];
            this.arrayY = [];
            this.arrayT = [];
            this.tempX = [];
            this.tempY = [];
            this.tempT = [];
        }
        this.change = change;
        this.paper = this.stylus.paper;
        this.strokeX = this.paper.getInkGrabber().stroke.x;
        this.strokeY = this.paper.getInkGrabber().stroke.y;
        this.dateT = this.paper.getInkGrabber().stroke.t;
        if (this.tempX.length == (this.change.undoLength - 1)) {
            this.tempX.push(this.strokeX);
            this.tempY.push(this.strokeY);
            this.tempT.push(this.dateT);
            // console.log("X baru:" + this.strokeX);
        }
        else if (this.boolUndo == false) {
            this.tempX.splice((this.change.undoLength - 1), this.tempX.length - this.change.undoLength + 1, this.tempX[this.change.undoLength - 1]);
            this.tempY.splice((this.change.undoLength - 1), this.tempY.length - this.change.undoLength + 1, this.tempY[this.change.undoLength - 1]);
            this.tempT.splice((this.change.undoLength - 1), this.tempT.length - this.change.undoLength + 1, this.tempT[this.change.undoLength - 1]);
            //console.log("X replace:"+this.strokeX);
        }
        console.log(change);
        this.dataIndex = this.change.undoLength;
        this.boolUndo = false;
    };
    StylusSignature.prototype.save = function () {
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.components = [];
        for (var i = 0; i < this.dataIndex; i++) {
            this.arrayX = this.arrayX.concat(this.tempX[i]);
            this.arrayY = this.arrayY.concat(this.tempY[i]);
            this.arrayT = this.arrayT.concat(this.tempT[i]);
            this.arrayX = this.arrayX.concat(null);
            this.arrayY = this.arrayY.concat(null);
            this.arrayT = this.arrayT.concat(null);
        }
        console.log("X: " + this.arrayX);
        console.log("Y: " + this.arrayY);
        console.log("T: " + this.arrayT);
        this.arrayStroke.push(this.arrayX);
        this.arrayStroke.push(this.arrayY);
        this.arrayStroke.push(this.arrayT);
        var stylusString = JSON.stringify(this.arrayStroke);
        console.log(this.arrayStroke);
        this.stylusChangedTag.emit(stylusString);
        this.element = document.getElementById("drawcanvas");
        var imageRenderingCanvas = this.element;
        var ctx = imageRenderingCanvas.getContext('2d');
        ctx.strokeStyle = "#000000";
        this.inkGrabber = new InkGrabber(ctx);
        this.inkGrabber.startCapture(this.arrayX[0], this.arrayY[0], this.arrayT[0]);
        for (var i_1 = 1; i_1 < this.arrayX.length - 1; i_1++) {
            if (this.arrayX[i_1] == null) {
                this.inkGrabber.endCapture(this.arrayX[i_1 - 1], this.arrayY[i_1 - 1], this.arrayT[i_1 - 1]);
                var stroke_1 = this.inkGrabber.getStroke();
                //this.inkGrabber.clear();
                this.components.push(stroke_1);
                this.inkGrabber = new InkGrabber(ctx);
                this.inkGrabber.startCapture(this.arrayX[i_1 + 1], this.arrayY[i_1 + 1], this.arrayT[i_1 + 1]);
            }
            else {
                this.inkGrabber.continueCapture(this.arrayX[i_1], this.arrayY[i_1], this.arrayT[i_1]);
            }
        }
        this.inkGrabber.endCapture(this.arrayX[this.arrayX.length - 2], this.arrayY[this.arrayX.length - 2], this.arrayT[this.arrayX.length - 2]);
        var stroke = this.inkGrabber.getStroke();
        this.components.push(stroke);
        this.inkGrabberFinal = new InkGrabber(ctx);
        for (var i_2 = 0; i_2 < this.components.length; i_2++) {
            this.inkGrabberFinal.drawComponent(this.components[i_2]);
        }
        this.savedDraw = true;
    };
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], StylusSignature.prototype, "stylusChangedTag", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], StylusSignature.prototype, "status", void 0);
    __decorate([
        ViewChild('inkPaper'),
        __metadata("design:type", StylusComponent)
    ], StylusSignature.prototype, "stylus", void 0);
    StylusSignature = __decorate([
        Component({
            selector: 'stylus-signature',
            templateUrl: 'stylus-signature.html',
            animations: [
                trigger('enterAnimation', [
                    transition(':enter', [
                        style({ transform: 'translateX(100%)', opacity: 0 }),
                        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
                    ]),
                    transition(':leave', [
                        style({ transform: 'translateX(0)', opacity: 1 }),
                        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
                    ])
                ])
            ],
        })
    ], StylusSignature);
    return StylusSignature;
}());
export { StylusSignature };
//# sourceMappingURL=stylus-signature.js.map