import { AbstractComponent, InkGrabber, InkPaper } from '@sirus/stylus';
import { FormControl, FormGroup } from '@angular/forms';
import { animate, Component, ElementRef, EventEmitter, Input, Output, ViewChild, style, transition, trigger, } from '@angular/core';
import { StylusComponent, InkChangeData, PenParametersInput } from '@sirus/stylus';

@Component({
    selector: 'stylus-signature',
    templateUrl: 'stylus-signature.html',
    animations: [
      trigger(
        'enterAnimation', [
          transition(':enter', [
            style({transform: 'translateX(100%)', opacity: 0}),
            animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
          ]),
          transition(':leave', [
            style({transform: 'translateX(0)', opacity: 1}),
            animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
          ])
        ]
      )
    ],
})
export class StylusSignature {
  arrayStroke:Array<any> = [];
  tempX: number[] = [];
  tempY: number[] = [];
  tempT: number[] = [];

  arrayX: number[] = [];
  arrayY: number[] = [];
  arrayT: number[] = [];

  dataIndex: number;
  boolUndo: boolean = false;

  strokeX: any;
  strokeY: any;
  dateT: any;

  savedDraw = false;

  @Output() stylusChangedTag = new EventEmitter();

  @Input()
  status:any;

  diagnosis: string;
  change: InkChangeData = {
    canRedo: false,
    canUndo: false,
    redoLength: 0,
    undoLength: 0
  }
  pen: PenParametersInput = {
    color: '#000',
    width: 2
  }

  width: number = 570;
  height: number = 500;
  timeout: number = 1;

  @ViewChild('inkPaper')
  private stylus: StylusComponent;
  private paper: InkPaper;
  //private captureCanvas: HTMLCanvasElement;
  private inkGrabber: InkGrabber;
  private inkGrabberFinal: InkGrabber;
  private element: HTMLElement;
  private elementFinal: HTMLElement;
  private components: AbstractComponent[] = [];


  clear(){
      this.stylus.clear();
  }

  undo(){
    if(this.change.canUndo){
      this.stylus.undo();
    }
  }

  redo(){
    if (this.change.canRedo && this.change.canUndo) {
      this.stylus.redo();
    }

  }

  writeDiagnosis(text: string){
      this.diagnosis = text;
  }

  inkChange(change: InkChangeData){
    if (this.change.undoLength == 0) {
      this.arrayX = [];
      this.arrayY = [];
      this.arrayT = [];
      this.tempX = [];
      this.tempY = [];
      this.tempT = [];
    }

    this.change = change;
    this.paper = this.stylus.paper;

    this.strokeX = this.paper.getInkGrabber().stroke.x;
    this.strokeY = this.paper.getInkGrabber().stroke.y;
    this.dateT = this.paper.getInkGrabber().stroke.t;

    if (this.tempX.length == (this.change.undoLength - 1)) {
      this.tempX.push(this.strokeX);
      this.tempY.push(this.strokeY);
      this.tempT.push(this.dateT);

      // console.log("X baru:" + this.strokeX);
    
    }
    else if (this.boolUndo == false) {
      this.tempX.splice((this.change.undoLength - 1), this.tempX.length - this.change.undoLength + 1, this.tempX[this.change.undoLength - 1]);
      this.tempY.splice((this.change.undoLength - 1), this.tempY.length - this.change.undoLength + 1, this.tempY[this.change.undoLength - 1]);
      this.tempT.splice((this.change.undoLength - 1), this.tempT.length - this.change.undoLength + 1, this.tempT[this.change.undoLength - 1]);
      //console.log("X replace:"+this.strokeX);
    }

    console.log(change);
    this.dataIndex = this.change.undoLength;
    this.boolUndo = false;

  }

  save() {
    
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.components = [];
    
        for (var i = 0; i < this.dataIndex; i++) {

          this.arrayX = this.arrayX.concat(this.tempX[i]);
          this.arrayY = this.arrayY.concat(this.tempY[i]);
          this.arrayT = this.arrayT.concat(this.tempT[i]);
    
          this.arrayX = this.arrayX.concat(null);
          this.arrayY = this.arrayY.concat(null);
          this.arrayT = this.arrayT.concat(null);
        }
    
        console.log("X: " + this.arrayX);
        console.log("Y: " + this.arrayY);
        console.log("T: " + this.arrayT);

        this.arrayStroke.push(this.arrayX);
        this.arrayStroke.push(this.arrayY);
        this.arrayStroke.push(this.arrayT);

        var stylusString = JSON.stringify(this.arrayStroke);
        console.log(this.arrayStroke);
        this.stylusChangedTag.emit(stylusString);
    
        this.element = document.getElementById("drawcanvas");
        let imageRenderingCanvas = <HTMLCanvasElement>this.element;
        let ctx = imageRenderingCanvas.getContext('2d');
        ctx.strokeStyle = "#000000";
        this.inkGrabber = new InkGrabber(ctx);


        this.inkGrabber.startCapture(this.arrayX[0], this.arrayY[0], this.arrayT[0]);
        for (let i = 1; i < this.arrayX.length - 1; i++) {
    
          if (this.arrayX[i] == null) {
            this.inkGrabber.endCapture(this.arrayX[i-1], this.arrayY[i-1], this.arrayT[i-1]);
    
            let stroke = this.inkGrabber.getStroke();
            //this.inkGrabber.clear();
            this.components.push(stroke);
            
            this.inkGrabber = new InkGrabber(ctx);
    
            this.inkGrabber.startCapture(this.arrayX[i+1], this.arrayY[i+1], this.arrayT[i+1]);
          }
          else {
            this.inkGrabber.continueCapture(this.arrayX[i], this.arrayY[i], this.arrayT[i]);
          }
        }
        this.inkGrabber.endCapture(this.arrayX[this.arrayX.length - 2], this.arrayY[this.arrayX.length - 2], this.arrayT[this.arrayX.length - 2]);
    
        let stroke = this.inkGrabber.getStroke();
        this.components.push(stroke);
    
        this.inkGrabberFinal = new InkGrabber(ctx);
    
        for(let i = 0; i<this.components.length; i++){
          this.inkGrabberFinal.drawComponent(this.components[i]);
        }

        this.savedDraw = true;
      }
    

}