import { Physiotherapy } from './physio.component';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Physiotherapy
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Physiotherapy)
  ]
})
export class PhysiotherapyModule {}
