var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';
var Physiotherapy = /** @class */ (function () {
    function Physiotherapy(viewCtrl, toastCtrl, dokterService, params) {
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.params = params;
        this.datapersonal = [];
        this.myDate = new Date().toISOString();
        this.diagnosa = false;
        if (params.get('icdx').length > 0) {
            this.listicd = params.get('icdx');
            this.diagnosa = true;
        }
        this.datapersonal = this.params.get('dataPatient');
        this.doctorname = this.params.get('doctorName');
    }
    Physiotherapy.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
        this.viewCtrl.dismiss();
    };
    Physiotherapy.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    Physiotherapy.prototype.submitFisio = function () {
        this.toastMessage('Permintaan Fisioterapi berhasil terkirim', 4000, 'top');
    };
    Physiotherapy.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Physiotherapy = __decorate([
        IonicPage(),
        Component({
            selector: 'physio-modal',
            templateUrl: 'physio.component.html'
        }),
        __metadata("design:paramtypes", [ViewController, ToastController, DokterService, NavParams])
    ], Physiotherapy);
    return Physiotherapy;
}());
export { Physiotherapy };
//# sourceMappingURL=physio.component.js.map