import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'physio-modal',
    templateUrl: 'physio.component.html'
})

export class Physiotherapy {

datapersonal:Array<any> = [];
doctorname:any;
myDate: String = new Date().toISOString();
personalTglLahir:string;
listicd:any;
diagnosa = false;

  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController, private dokterService: DokterService, public params: NavParams ) {
      if(params.get('icdx').length > 0){
        this.listicd = params.get('icdx');
        this.diagnosa = true;
      }
      this.datapersonal = this.params.get('dataPatient');
      this.doctorname = this.params.get('doctorName');
  }

  toastMessage(message:string,duration:number,position:string){
    let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        position: position
    });
    toast.present(toast);
    this.viewCtrl.dismiss();
  }

  private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
 }

  submitFisio(){
    this.toastMessage('Permintaan Fisioterapi berhasil terkirim',4000,'top');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}