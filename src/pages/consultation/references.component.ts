import { InkGrabber, AbstractComponent } from '@sirus/stylus';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DokterService } from '../../services/providers/dokter-service';
import { Component, OnInit } from '@angular/core';
import { ToastController, NavParams, ViewController, ModalController, IonicPage } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'konsul-modal',
    templateUrl: 'references.component.html'
})

export class References implements OnInit {

datapersonal:Array<any> = [];
dokter: string;
doctorname: string;
personalTglLahir:string;
datact:Array<any> = [];
dataDoctor:Array<any> = [];
listicdx:Array<any> = [];
listobat:Array<any> = [];

autocompleteDoctor:boolean = false;
searchQueryDoctor;
doctorSpecialis:any;
nodokDoctorSpecialis:any;
statusICDX = false;
statusObat = false;

private inkGrabber: InkGrabber;
private inkGrabberFinal: InkGrabber;
private element: HTMLElement;
private components: AbstractComponent[] = [];
arrayX: number[] = [];
arrayY: number[] = [];
arrayT: number[] = [];

width: number = 800;
height: number = 400;
timeout: number = 1;
imgSource:any;

styluss:boolean[] = [false];
styluso:boolean[] = [false];
stylusa:boolean[] = [false];
styluspo:boolean[] = [false];
statusCT = false;

private formConsultation : FormGroup;

 constructor(public viewCtrl: ViewController, public toastCtrl: ToastController,public dokterService: DokterService, private navParams: NavParams, private formBuilder: FormBuilder, public modalCtrl: ModalController) {


  var salt = Math.floor(Math.random() * 10000);
  var no_konsultasi = salt+this.navParams.get('personalNoRM');

  this.formConsultation = this.formBuilder.group({
    'no_konsultasi': [no_konsultasi, Validators.required],
    'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
    'nodok':[this.navParams.get('personalNodok'), Validators.required],
    'id_ct': [''],
    'alasan': ['', Validators.required],
    'nodok_tujuan': ['']
});

 }

 ngOnInit(){
    this.datapersonal = this.navParams.get('dataPatient');
    this.doctorname = this.navParams.get('doctorName');

    this.dokterService.getDataIntegratedNoteNodok1(this.navParams.get('personalNodok'),this.navParams.get('personalNoRM')).subscribe(({data}) => {
      console.log(data);
      var str = JSON.stringify(data);
      var pars = JSON.parse(str);
      let ctpasien = pars.ctByNodok1;
      if(ctpasien.length > 0){
        this.datact = [ctpasien[0]];
        this.statusCT = true;
        if(ctpasien[0].a_icdx !== null){
          this.statusICDX = true;
          let parsicd = JSON.parse(ctpasien[0].a_icdx);
          this.listicdx = parsicd;
        }
        if(ctpasien[0].p_obat_name !== null){
          this.statusObat = true;
          let parsobat = JSON.parse(ctpasien[0].p_obat_nama);
          this.listobat = parsobat;
        }
      }else{
        this.statusICDX = false;
        this.statusObat = false;
        this.datact = [{
          id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
        }];
        this.statusCT = false;
      }
    });
 }

 toastMessage(message:string,duration:number,position:string){

    let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        position: position
    });
    toast.present(toast);

  }

 dismiss() {
  
     this.viewCtrl.dismiss();
  
 }

 searchDoctor(ev) {
  
      let val = ev.target.value;
  
      if(this.searchQueryDoctor !== ''){
  
        this.autocompleteDoctor = true;
  
        this.dokterService.getDataDoctorLike(val, 0, 10).subscribe(({data})=>{
                console.log(data);
                this.dataDoctor = [...data.doctorByName];
          
        });
  
      }else{
  
        this.autocompleteDoctor = false;
  
      }
  
    }

    addDoctor(ev){
          let val = this.dataDoctor[ev];
          this.doctorSpecialis = val.nama_dok;
          this.nodokDoctorSpecialis = val.nodok;
          this.searchQueryDoctor = '';
          this.autocompleteDoctor = false;
      
    }

    submitKonsul(formData){
      if(this.formConsultation.valid){
        if(this.datact[0].id_ct !== null){
          formData.id_ct = this.datact[0].id_ct;
        }
        formData.nodok_tujuan = this.nodokDoctorSpecialis;
        console.log(formData);
        this.dokterService.saveConsultation(formData).subscribe(({data})=>{
          if(data){
            this.toastMessage('Konsultasi ke Dokter Sejawat berhasil terkirim',4000,'top');
            this.dismiss();
          }else{
            this.toastMessage('Konsultasi ke Dokter Sejawat gagal terkirim',4000,'top');
          }
        });
      }else{
        this.toastMessage('Konsultasi ke Dokter Sejawat gagal terkirim',4000,'top');
      }

    }
    
    drawcanvas(index:any, strokedata:any, status:any){
      this.components = [];
      if(status == "s"){
        this.styluss[index] = !this.styluss[index];
      }else if(status == "o"){
        this.styluso[index] = !this.styluso[index];
      }else if(status == "a"){
        this.stylusa[index] = !this.stylusa[index];
      }else if(status == "po"){
        this.styluspo[index] = !this.styluspo[index];
      }
      let x: number[] = [];
      let y: number[] = [];
      let t: number[] = [];
    
    
      if(strokedata !== null){
        let datastroke = JSON.parse(strokedata);
        
          x = x.concat(datastroke[0]);
          y = y.concat(datastroke[1]);
          t = t.concat(datastroke[2]);
        
          x = x.concat(null);
          y = y.concat(null);
          t = t.concat(null);
          
          this.element = document.getElementById(index);
          let imageRenderingCanvas = <HTMLCanvasElement>this.element;
          let ctx = imageRenderingCanvas.getContext('2d');
          ctx.clearRect(0, 0, this.width, this.height);
          this.inkGrabber = new InkGrabber(ctx);
        
          this.inkGrabber.startCapture(x[0], y[0], t[0]);
          for (let i = 1; i < x.length - 1; i++) {
        
            if (x[i] == null) {
              this.inkGrabber.endCapture(x[i-1], y[i-1], t[i-1]);
        
              let stroke = this.inkGrabber.getStroke();
              //this.inkGrabber.clear();
              this.components.push(stroke);
              
              this.inkGrabber = new InkGrabber(ctx);
        
              this.inkGrabber.startCapture(x[i+1], y[i+1], t[i+1]);
            }
            else {
              this.inkGrabber.continueCapture(x[i], y[i], t[i]);
            }
          }
          this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
        
          let stroke = this.inkGrabber.getStroke();
          this.components.push(stroke);
        
          this.inkGrabberFinal = new InkGrabber(ctx);
        
          for(let i = 0; i<this.components.length; i++){
            this.inkGrabberFinal.drawComponent(this.components[i]);
          }
             
          let image = new Image();
          image.id = "pic"+index;
          let content = <HTMLCanvasElement>this.element;
          image.src = content.toDataURL(); 
          this.datact[index]["image"+status] = image.src;
          console.log(this.datact[index]);
          //console.log(image.src)
      }else{
        this.datact[index]["image"+status] = "assets/img/no_image.png";
      }
      
  }

  private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
  }

}