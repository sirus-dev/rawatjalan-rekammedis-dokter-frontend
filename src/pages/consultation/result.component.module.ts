import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { Result } from './result.component';
@NgModule({
  declarations: [
    Result
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Result)
  ]
})
export class ResultModule {}
