var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { InkGrabber } from '@sirus/stylus';
import { FormBuilder, Validators } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, NavParams, ToastController, IonicPage } from 'ionic-angular';
import moment from 'moment';
var Reply = /** @class */ (function () {
    function Reply(viewCtrl, dokterService, navParams, formBuilder, toastCtrl) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.dokterService = dokterService;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.datapersonal = [];
        this.datact = [];
        this.dataDoctor = [];
        this.listicdx = [];
        this.listobat = [];
        this.statusICDX = false;
        this.statusObat = false;
        this.statusICDX_jawaban = false;
        this.statusObat_jawaban = false;
        this.components = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.width = 800;
        this.height = 400;
        this.timeout = 1;
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        this.statusCT = false;
        this.formConsultation = this.formBuilder.group({
            'no_konsultasi': [''],
            'id_ct_jawaban': [''],
            'jawaban': ['', Validators.required]
        });
        this.datapersonal = this.navParams.get('dataPatient');
        this.doctorname = this.navParams.get('doctorName');
        this.dokterService.getDataConsultationTujuan(this.navParams.get('personalNodok')).subscribe(function (_a) {
            var data = _a.data;
            console.log(data);
            var datakonsul = data.consultationByTujuan.slice();
            if (datakonsul.length > 0) {
                _this.nokonsul = datakonsul[0].no_konsultasi;
                _this.reasonConsul = datakonsul[0].alasan;
                if (datakonsul[0].jawaban !== null) {
                    _this.resultAnswer = datakonsul[0].jawaban;
                }
                else {
                    _this.resultAnswer = "Belum ada Jawaban";
                }
                _this.dokterService.getDataDoctor(datakonsul[0].nodok_tujuan).subscribe(function (_a) {
                    var data = _a.data;
                    var doctorstr = JSON.stringify(data);
                    var doctorparse = JSON.parse(doctorstr);
                    var reviewdata = doctorparse.doctorByNodok;
                    _this.dokter_tujuan = reviewdata.nama_dok;
                });
                if (datakonsul[0].id_ct) {
                    _this.dokterService.getDataIntegratedNoteById(datakonsul[0].id_ct).subscribe(function (_a) {
                        var data = _a.data;
                        console.log(data);
                        var str = JSON.stringify(data);
                        var pars = JSON.parse(str);
                        var ctpasien = pars.ctById;
                        if (ctpasien) {
                            _this.datact = [ctpasien];
                            _this.statusCT = true;
                            if (ctpasien.a_icdx !== null) {
                                _this.statusICDX = true;
                                var parsicd = JSON.parse(ctpasien.a_icdx);
                                _this.listicdx = parsicd;
                            }
                            if (ctpasien.p_obat_name !== null) {
                                _this.statusObat = true;
                                var parsobat = JSON.parse(ctpasien.p_obat_nama);
                                _this.listobat = parsobat;
                            }
                        }
                        else {
                            _this.statusICDX = false;
                            _this.statusObat = false;
                            _this.datact = [{
                                    id_ct: '', s_description: 'Belum ada', o_description: 'Belum ada', a_description: 'Belum ada', p_obat_description: 'Belum ada'
                                }];
                            _this.statusCT = false;
                        }
                    });
                }
                else {
                    _this.statusICDX = false;
                    _this.statusObat = false;
                    _this.datact = [{
                            id_ct: '', s_description: 'Belum ada', o_description: 'Belum ada', a_description: 'Belum ada', p_obat_description: 'Belum ada'
                        }];
                    _this.statusCT = false;
                }
                _this.dokterService.getDataIntegratedNoteNodok1(_this.navParams.get('personalNodok'), _this.navParams.get('personalNoRM')).subscribe(function (_a) {
                    var data = _a.data;
                    console.log(data);
                    var str = JSON.stringify(data);
                    var pars = JSON.parse(str);
                    var ctpasien = pars.ctByNodok1;
                    if (ctpasien.length > 0) {
                        _this.datact_jawaban = [ctpasien[0]];
                        _this.statusCT = true;
                        if (ctpasien.a_icdx !== null) {
                            _this.statusICDX_jawaban = true;
                            var parsicd = JSON.parse(ctpasien[0].a_icdx);
                            _this.listicdx_jawaban = parsicd;
                        }
                        if (ctpasien.p_obat_name !== null) {
                            _this.statusObat_jawaban = true;
                            var parsobat = JSON.parse(ctpasien[0].p_obat_nama);
                            _this.listobat_jawaban = parsobat;
                        }
                    }
                    else {
                        _this.statusICDX_jawaban = false;
                        _this.statusObat_jawaban = false;
                        _this.datact_jawaban = [{
                                id_ct: '', s_description: 'Belum ada', o_description: 'Belum ada', a_description: 'Belum ada', p_obat_description: 'Belum ada'
                            }];
                        _this.statusCT = false;
                    }
                });
            }
            else {
                _this.statusICDX_jawaban = false;
                _this.statusObat_jawaban = false;
                _this.datact_jawaban = [{
                        id_ct: '', s_description: 'Belum ada', o_description: 'Belum ada', a_description: 'Belum ada', p_obat_description: 'Belum ada'
                    }];
                _this.statusCT = false;
            }
        });
    }
    Reply.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
    };
    Reply.prototype.submitJawaban = function (formData) {
        var _this = this;
        if (this.formConsultation.valid) {
            formData.id_ct_jawaban = this.datact_jawaban[0].id_ct;
            formData.no_konsultasi = this.nokonsul;
            console.log(formData);
            this.dokterService.updateConsultataion(formData).subscribe(function (_a) {
                var data = _a.data;
                console.log(data);
                if (data) {
                    _this.toastMessage("Jawaban anda berhasil disubmit", 4000, "top");
                    _this.dismiss();
                }
                else {
                    _this.toastMessage("Jawaban anda gagal disubmit", 4000, "top");
                }
            });
        }
        else {
            this.toastMessage("Mohon lengkapi jawaban anda", 4000, "top");
        }
    };
    Reply.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Reply.prototype.drawcanvas = function (index, strokedata, status) {
        this.components = [];
        if (status == "s") {
            this.styluss[index] = !this.styluss[index];
        }
        else if (status == "o") {
            this.styluso[index] = !this.styluso[index];
        }
        else if (status == "a") {
            this.stylusa[index] = !this.stylusa[index];
        }
        else if (status == "po") {
            this.styluspo[index] = !this.styluspo[index];
        }
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById(index);
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            this.inkGrabber = new InkGrabber(ctx);
            this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    var stroke_1 = this.inkGrabber.getStroke();
                    //this.inkGrabber.clear();
                    this.components.push(stroke_1);
                    this.inkGrabber = new InkGrabber(ctx);
                    this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            var stroke = this.inkGrabber.getStroke();
            this.components.push(stroke);
            this.inkGrabberFinal = new InkGrabber(ctx);
            for (var i = 0; i < this.components.length; i++) {
                this.inkGrabberFinal.drawComponent(this.components[i]);
            }
            var image = new Image();
            image.id = "pic" + index;
            var content = this.element;
            image.src = content.toDataURL();
            this.datact[index]["image" + status] = image.src;
            console.log(this.datact[index]);
            //console.log(image.src)
        }
        else {
            this.datact[index]["image" + status] = "assets/img/no_image.png";
        }
    };
    Reply.prototype.drawcanvasjawaban = function (index, strokedata, status) {
        this.components = [];
        if (status == "s") {
            this.styluss[index] = !this.styluss[index];
        }
        else if (status == "o") {
            this.styluso[index] = !this.styluso[index];
        }
        else if (status == "a") {
            this.stylusa[index] = !this.stylusa[index];
        }
        else if (status == "po") {
            this.styluspo[index] = !this.styluspo[index];
        }
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById(index);
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            this.inkGrabber = new InkGrabber(ctx);
            this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    var stroke_2 = this.inkGrabber.getStroke();
                    //this.inkGrabber.clear();
                    this.components.push(stroke_2);
                    this.inkGrabber = new InkGrabber(ctx);
                    this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            var stroke = this.inkGrabber.getStroke();
            this.components.push(stroke);
            this.inkGrabberFinal = new InkGrabber(ctx);
            for (var i = 0; i < this.components.length; i++) {
                this.inkGrabberFinal.drawComponent(this.components[i]);
            }
            var image = new Image();
            image.id = "pic" + index;
            var content = this.element;
            image.src = content.toDataURL();
            this.datact_jawaban[index]["image" + status] = image.src;
            console.log(this.datact_jawaban[index]);
            //console.log(image.src)
        }
        else {
            this.datact_jawaban[index]["image" + status] = "assets/img/no_image.png";
        }
    };
    Reply.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    Reply = __decorate([
        IonicPage(),
        Component({
            selector: 'reply-modal',
            templateUrl: 'reply.component.html'
        }),
        __metadata("design:paramtypes", [ViewController, DokterService, NavParams, FormBuilder, ToastController])
    ], Reply);
    return Reply;
}());
export { Reply };
//# sourceMappingURL=reply.component.js.map