import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { Reply } from './reply.component';
@NgModule({
  declarations: [
    Reply
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Reply)
  ]
})
export class ReplyModule {}
