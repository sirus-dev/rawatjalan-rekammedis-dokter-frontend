import { InkGrabber, AbstractComponent } from '@sirus/stylus';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, NavParams, ToastController, IonicPage } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'reply-modal',
    templateUrl: 'reply.component.html'
})

export class Reply {
  
datapersonal:Array<any> = [];
personalTglLahir:any;
doctorname:string;
datact:Array<any> = [];
dataDoctor:Array<any> = [];
listicdx:Array<any> = [];
listobat:Array<any> = [];

statusICDX = false;
statusObat = false;
reasonConsul:string;
dokter_tujuan:string;

datact_jawaban:Array<any>;
dataDoctor_jawaban:Array<any>;
listicdx_jawaban:Array<any>;
listobat_jawaban:Array<any>;

statusICDX_jawaban = false;
statusObat_jawaban = false;
resultAnswer:string;

nokonsul:string;

private inkGrabber: InkGrabber;
private inkGrabberFinal: InkGrabber;
private element: HTMLElement;
private components: AbstractComponent[] = [];
arrayX: number[] = [];
arrayY: number[] = [];
arrayT: number[] = [];

width: number = 800;
height: number = 400;
timeout: number = 1;
imgSource:any;

styluss:boolean[] = [false];
styluso:boolean[] = [false];
stylusa:boolean[] = [false];
styluspo:boolean[] = [false];
statusCT = false;

private formConsultation : FormGroup;

 constructor(public viewCtrl: ViewController, public dokterService: DokterService, private navParams: NavParams, private formBuilder: FormBuilder, public toastCtrl: ToastController) {

    this.formConsultation = this.formBuilder.group({
        'no_konsultasi': [''],
        'id_ct_jawaban': [''],
        'jawaban': ['', Validators.required]
    });

    this.datapersonal = this.navParams.get('dataPatient');
    this.doctorname = this.navParams.get('doctorName');

this.dokterService.getDataConsultationTujuan(this.navParams.get('personalNodok')).subscribe(({data}) => {
  console.log(data);
  let datakonsul = [...data.consultationByTujuan];
  if(datakonsul.length > 0){
    this.nokonsul = datakonsul[0].no_konsultasi;
    this.reasonConsul = datakonsul[0].alasan;
    if(datakonsul[0].jawaban !== null){
      this.resultAnswer = datakonsul[0].jawaban;
    }else{
      this.resultAnswer = "Belum ada Jawaban";
    }

    this.dokterService.getDataDoctor(datakonsul[0].nodok_tujuan).subscribe(({data}) => {
      var doctorstr = JSON.stringify(data);
      var doctorparse = JSON.parse(doctorstr);
      let reviewdata = doctorparse.doctorByNodok;
      this.dokter_tujuan = reviewdata.nama_dok;
    });

    if(datakonsul[0].id_ct){
      this.dokterService.getDataIntegratedNoteById(datakonsul[0].id_ct).subscribe(({data}) => {
        console.log(data);
        var str = JSON.stringify(data);
        var pars = JSON.parse(str);
        let ctpasien = pars.ctById;
        if(ctpasien){
          this.datact = [ctpasien];
          this.statusCT = true;
          if(ctpasien.a_icdx !== null){
            this.statusICDX = true;
            let parsicd = JSON.parse(ctpasien.a_icdx);
            this.listicdx = parsicd;
          }
          if(ctpasien.p_obat_name !== null){
            this.statusObat = true;
            let parsobat = JSON.parse(ctpasien.p_obat_nama);
            this.listobat = parsobat;
          }
        }else{
          this.statusICDX = false;
          this.statusObat = false;
          this.datact = [{
            id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
          }];
          this.statusCT = false;
        }
      });
    }else{
      this.statusICDX = false;
      this.statusObat = false;
      this.datact = [{
        id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
      }];
      this.statusCT = false;
    }

      this.dokterService.getDataIntegratedNoteNodok1(this.navParams.get('personalNodok'), this.navParams.get('personalNoRM')).subscribe(({data}) => {
        console.log(data);
        var str = JSON.stringify(data);
        var pars = JSON.parse(str);
        let ctpasien = pars.ctByNodok1;
        if(ctpasien.length > 0){
          this.datact_jawaban = [ctpasien[0]];
          this.statusCT = true;
          if(ctpasien.a_icdx !== null){
            this.statusICDX_jawaban = true;
            let parsicd = JSON.parse(ctpasien[0].a_icdx);
            this.listicdx_jawaban = parsicd;
          }
          if(ctpasien.p_obat_name !== null){
            this.statusObat_jawaban = true;
            let parsobat = JSON.parse(ctpasien[0].p_obat_nama);
            this.listobat_jawaban = parsobat;
          }
        }else{
          this.statusICDX_jawaban = false;
          this.statusObat_jawaban = false;
          this.datact_jawaban = [{
            id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
          }];
          this.statusCT = false;
        }
      });
    }else{
      this.statusICDX_jawaban = false;
      this.statusObat_jawaban = false;
      this.datact_jawaban = [{
        id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
      }];
      this.statusCT = false;
    }
});

}

toastMessage(message:string,duration:number,position:string){

      let toast = this.toastCtrl.create({
          message: message,
          duration: duration,
          position: position
      });
      toast.present(toast);

}

submitJawaban(formData){
    if(this.formConsultation.valid){

        formData.id_ct_jawaban = this.datact_jawaban[0].id_ct;
        formData.no_konsultasi = this.nokonsul;
        console.log(formData);

        this.dokterService.updateConsultataion(formData).subscribe(({data})=>{
            console.log(data);
            if(data){
                this.toastMessage("Jawaban anda berhasil disubmit",4000,"top");
                this.dismiss();
            }else{
                this.toastMessage("Jawaban anda gagal disubmit",4000,"top");
            }
        });
    }else{
        this.toastMessage("Mohon lengkapi jawaban anda",4000,"top");
    }
}

 dismiss() {

   this.viewCtrl.dismiss();

 }

 drawcanvas(index:any, strokedata:any, status:any){
  this.components = [];
  if(status == "s"){
    this.styluss[index] = !this.styluss[index];
  }else if(status == "o"){
    this.styluso[index] = !this.styluso[index];
  }else if(status == "a"){
    this.stylusa[index] = !this.stylusa[index];
  }else if(status == "po"){
    this.styluspo[index] = !this.styluspo[index];
  }
  let x: number[] = [];
  let y: number[] = [];
  let t: number[] = [];


  if(strokedata !== null){
    let datastroke = JSON.parse(strokedata);
    
      x = x.concat(datastroke[0]);
      y = y.concat(datastroke[1]);
      t = t.concat(datastroke[2]);
    
      x = x.concat(null);
      y = y.concat(null);
      t = t.concat(null);
      
      this.element = document.getElementById(index);
      let imageRenderingCanvas = <HTMLCanvasElement>this.element;
      let ctx = imageRenderingCanvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      this.inkGrabber = new InkGrabber(ctx);
    
      this.inkGrabber.startCapture(x[0], y[0], t[0]);
      for (let i = 1; i < x.length - 1; i++) {
    
        if (x[i] == null) {
          this.inkGrabber.endCapture(x[i-1], y[i-1], t[i-1]);
    
          let stroke = this.inkGrabber.getStroke();
          //this.inkGrabber.clear();
          this.components.push(stroke);
          
          this.inkGrabber = new InkGrabber(ctx);
    
          this.inkGrabber.startCapture(x[i+1], y[i+1], t[i+1]);
        }
        else {
          this.inkGrabber.continueCapture(x[i], y[i], t[i]);
        }
      }
      this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
    
      let stroke = this.inkGrabber.getStroke();
      this.components.push(stroke);
    
      this.inkGrabberFinal = new InkGrabber(ctx);
    
      for(let i = 0; i<this.components.length; i++){
        this.inkGrabberFinal.drawComponent(this.components[i]);
      }
         
      let image = new Image();
      image.id = "pic"+index;
      let content = <HTMLCanvasElement>this.element;
      image.src = content.toDataURL(); 
      this.datact[index]["image"+status] = image.src;
      console.log(this.datact[index]);
      //console.log(image.src)
  }else{
    this.datact[index]["image"+status] = "assets/img/no_image.png";
  }
  
 }

 drawcanvasjawaban(index:any, strokedata:any, status:any){
  this.components = [];
  if(status == "s"){
    this.styluss[index] = !this.styluss[index];
  }else if(status == "o"){
    this.styluso[index] = !this.styluso[index];
  }else if(status == "a"){
    this.stylusa[index] = !this.stylusa[index];
  }else if(status == "po"){
    this.styluspo[index] = !this.styluspo[index];
  }
  let x: number[] = [];
  let y: number[] = [];
  let t: number[] = [];


  if(strokedata !== null){
    let datastroke = JSON.parse(strokedata);
    
      x = x.concat(datastroke[0]);
      y = y.concat(datastroke[1]);
      t = t.concat(datastroke[2]);
    
      x = x.concat(null);
      y = y.concat(null);
      t = t.concat(null);
      
      this.element = document.getElementById(index);
      let imageRenderingCanvas = <HTMLCanvasElement>this.element;
      let ctx = imageRenderingCanvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      this.inkGrabber = new InkGrabber(ctx);
    
      this.inkGrabber.startCapture(x[0], y[0], t[0]);
      for (let i = 1; i < x.length - 1; i++) {
    
        if (x[i] == null) {
          this.inkGrabber.endCapture(x[i-1], y[i-1], t[i-1]);
    
          let stroke = this.inkGrabber.getStroke();
          //this.inkGrabber.clear();
          this.components.push(stroke);
          
          this.inkGrabber = new InkGrabber(ctx);
    
          this.inkGrabber.startCapture(x[i+1], y[i+1], t[i+1]);
        }
        else {
          this.inkGrabber.continueCapture(x[i], y[i], t[i]);
        }
      }
      this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
    
      let stroke = this.inkGrabber.getStroke();
      this.components.push(stroke);
    
      this.inkGrabberFinal = new InkGrabber(ctx);
    
      for(let i = 0; i<this.components.length; i++){
        this.inkGrabberFinal.drawComponent(this.components[i]);
      }
         
      let image = new Image();
      image.id = "pic"+index;
      let content = <HTMLCanvasElement>this.element;
      image.src = content.toDataURL(); 
      this.datact_jawaban[index]["image"+status] = image.src;
      console.log(this.datact_jawaban[index]);
      //console.log(image.src)
  }else{
    this.datact_jawaban[index]["image"+status] = "assets/img/no_image.png";
  }
  
 }

 private mapToMoment(date: string): string {
  if (moment(date).isValid()) {
    return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
  }
  return "-";
}

}