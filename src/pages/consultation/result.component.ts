import { AbstractComponent, InkGrabber } from '@sirus/stylus';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'result-modal',
    templateUrl: 'result.component.html'
})

export class Result {
  
datapersonal:Array<any> = [];
personalTglLahir:any;
doctorname:string;
datact:Array<any> = [];
dataDoctor:Array<any> = [];
listicdx:Array<any> = [];
listobat:Array<any> = [];

statusICDX = false;
statusObat = false;
reasonConsul:string;
dokter_tujuan:string;

datact_jawaban:Array<any>;
dataDoctor_jawaban:Array<any>;
listicdx_jawaban:Array<any>;
listobat_jawaban:Array<any>;

statusICDX_jawaban = false;
statusObat_jawaban = false;
resultAnswer:string;

private inkGrabber: InkGrabber;
private inkGrabberFinal: InkGrabber;
private element: HTMLElement;
private components: AbstractComponent[] = [];
arrayX: number[] = [];
arrayY: number[] = [];
arrayT: number[] = [];

width: number = 800;
height: number = 400;
timeout: number = 1;
imgSource:any;

styluss:boolean[]  = [false];
styluso:boolean[]  = [false];
stylusa:boolean[]  = [false];
styluspo:boolean[] = [false];
statusCT = false;

stylussj:boolean[]  = [false];
stylusoj:boolean[]  = [false];
stylusaj:boolean[]  = [false];
styluspoj:boolean[] = [false];
statusCTJwb = false;

 constructor(public viewCtrl: ViewController, public dokterService: DokterService, private navParams: NavParams) {

  this.datapersonal = this.navParams.get('dataPatient');
  this.doctorname = this.navParams.get('doctorName');

this.dokterService.getDataConsultationNodok1(this.navParams.get('personalNoRM'),this.navParams.get('personalNodok'))
.subscribe(({data}) => {
  console.log(data);
  let datakonsul = [...data.consultationByNodok1];
  if(datakonsul.length > 0){

    this.reasonConsul = datakonsul[0].alasan;
    if(datakonsul[0].jawaban !== null){
      this.resultAnswer = datakonsul[0].jawaban;
    }else{
      this.resultAnswer = "Belum ada Jawaban";
    }

    this.dokterService.getDataDoctor(datakonsul[0].nodok_tujuan).subscribe(({data}) => {
      var doctorstr = JSON.stringify(data);
      var doctorparse = JSON.parse(doctorstr);
      let reviewdata = doctorparse.doctorByNodok;
      this.dokter_tujuan = reviewdata.nama_dok;
    });

    if(datakonsul[0].id_ct){
      this.dokterService.getDataIntegratedNoteById(datakonsul[0].id_ct).subscribe(({data}) => {
        console.log(data);
        var str = JSON.stringify(data);
        var pars = JSON.parse(str);
        let ctpasien = pars.ctById;
        if(ctpasien){
          this.datact = [ctpasien];
          this.statusCT = true;
          console.log(this.datact);
          if(ctpasien.a_icdx !== null){
            this.statusICDX = true;
            let parsicd = JSON.parse(ctpasien.a_icdx);
            this.listicdx = parsicd;
          }
          if(ctpasien.p_obat_name !== null){
            this.statusObat = true;
            let parsobat = JSON.parse(ctpasien.p_obat_nama);
            this.listobat = parsobat;
          }
        }else{
          this.statusICDX = false;
          this.statusObat = false;
          this.datact = [{
            id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
          }];
          this.statusCT = false;
        }
      });
    }else{
      this.statusICDX = false;
      this.statusObat = false;
      this.datact = [{
        id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
      }];
      this.statusCT = false;
    }

    if(datakonsul[0].id_ct_jawaban){
      this.dokterService.getDataIntegratedNoteById(datakonsul[0].id_ct_jawaban).subscribe(({data}) => {
        var str = JSON.stringify(data);
        var pars = JSON.parse(str);
        let ctpasien = pars.ctById;
        if(ctpasien){
          this.datact_jawaban = [ctpasien];
          this.statusCTJwb = true;
          if(ctpasien.a_icdx !== null){
            this.statusICDX_jawaban = true;
            let parsicd = JSON.parse(ctpasien.a_icdx);
            this.listicdx_jawaban = parsicd;
          }
          if(ctpasien.p_obat_name !== null){
            this.statusObat_jawaban = true;
            let parsobat = JSON.parse(ctpasien.p_obat_nama);
            this.listobat_jawaban = parsobat;
          }
        }else{
          this.statusICDX_jawaban = false;
          this.statusObat_jawaban = false;
          this.datact_jawaban = [{
            id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
          }];
          this.statusCTJwb = false;
        }
      });
    }else{
      this.statusICDX_jawaban = false;
      this.statusObat_jawaban = false;
      this.datact_jawaban = [{
        id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
      }];
      this.statusCTJwb = false;
    }
  }else{
    this.reasonConsul = "Tidak ada Alasan";
    this.resultAnswer = "Belum ada Jawaban";
    this.statusICDX = false;
    this.statusObat = false;
    this.datact = [{
      id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
    }];
    this.statusCTJwb = false;
    this.statusICDX_jawaban = false;
    this.statusObat_jawaban = false;
    this.datact_jawaban = [{
      id_ct:'', s_description:'Belum ada', o_description:'Belum ada', a_description:'Belum ada', p_obat_description:'Belum ada'
    }];
    this.statusCTJwb = false;
  }
});

}

drawcanvas(index:any, strokedata:any, status:any){
  this.components = [];
  if(status == "s"){
    this.styluss[index] = !this.styluss[index];
  }else if(status == "o"){
    this.styluso[index] = !this.styluso[index];
  }else if(status == "a"){
    this.stylusa[index] = !this.stylusa[index];
  }else if(status == "po"){
    this.styluspo[index] = !this.styluspo[index];
  }
  let x: number[] = [];
  let y: number[] = [];
  let t: number[] = [];


  if(strokedata !== null){
    let datastroke = JSON.parse(strokedata);
    
      x = x.concat(datastroke[0]);
      y = y.concat(datastroke[1]);
      t = t.concat(datastroke[2]);
    
      x = x.concat(null);
      y = y.concat(null);
      t = t.concat(null);
      
      this.element = document.getElementById(index);
      let imageRenderingCanvas = <HTMLCanvasElement>this.element;
      let ctx = imageRenderingCanvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      this.inkGrabber = new InkGrabber(ctx);
    
      this.inkGrabber.startCapture(x[0], y[0], t[0]);
      for (let i = 1; i < x.length - 1; i++) {
    
        if (x[i] == null) {
          this.inkGrabber.endCapture(x[i-1], y[i-1], t[i-1]);
    
          let stroke = this.inkGrabber.getStroke();
          //this.inkGrabber.clear();
          this.components.push(stroke);
          
          this.inkGrabber = new InkGrabber(ctx);
    
          this.inkGrabber.startCapture(x[i+1], y[i+1], t[i+1]);
        }
        else {
          this.inkGrabber.continueCapture(x[i], y[i], t[i]);
        }
      }
      this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
    
      let stroke = this.inkGrabber.getStroke();
      this.components.push(stroke);
    
      this.inkGrabberFinal = new InkGrabber(ctx);
    
      for(let i = 0; i<this.components.length; i++){
        this.inkGrabberFinal.drawComponent(this.components[i]);
      }
         
      let image = new Image();
      image.id = "pic"+index;
      let content = <HTMLCanvasElement>this.element;
      image.src = content.toDataURL(); 
      this.datact[index]["image"+status] = image.src;
      console.log(this.datact[index]);
      //console.log(image.src)
  }else{
    this.datact[index]["image"+status] = "assets/img/no_image.png";
  }
  
 }

 drawcanvasjawaban(index:any, strokedata:any, status:any){
  this.components = [];
  if(status == "s"){
    this.stylussj[index] = !this.stylussj[index];
  }else if(status == "o"){
    this.stylusoj[index] = !this.stylusoj[index];
  }else if(status == "a"){
    this.stylusaj[index] = !this.stylusaj[index];
  }else if(status == "po"){
    this.styluspoj[index] = !this.styluspoj[index];
  }
  let x: number[] = [];
  let y: number[] = [];
  let t: number[] = [];


  if(strokedata !== null){
    let datastroke = JSON.parse(strokedata);
    
      x = x.concat(datastroke[0]);
      y = y.concat(datastroke[1]);
      t = t.concat(datastroke[2]);
    
      x = x.concat(null);
      y = y.concat(null);
      t = t.concat(null);
      
      this.element = document.getElementById(index);
      let imageRenderingCanvas = <HTMLCanvasElement>this.element;
      let ctx = imageRenderingCanvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      this.inkGrabber = new InkGrabber(ctx);
    
      this.inkGrabber.startCapture(x[0], y[0], t[0]);
      for (let i = 1; i < x.length - 1; i++) {
    
        if (x[i] == null) {
          this.inkGrabber.endCapture(x[i-1], y[i-1], t[i-1]);
    
          let stroke = this.inkGrabber.getStroke();
          //this.inkGrabber.clear();
          this.components.push(stroke);
          
          this.inkGrabber = new InkGrabber(ctx);
    
          this.inkGrabber.startCapture(x[i+1], y[i+1], t[i+1]);
        }
        else {
          this.inkGrabber.continueCapture(x[i], y[i], t[i]);
        }
      }
      this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
    
      let stroke = this.inkGrabber.getStroke();
      this.components.push(stroke);
    
      this.inkGrabberFinal = new InkGrabber(ctx);
    
      for(let i = 0; i<this.components.length; i++){
        this.inkGrabberFinal.drawComponent(this.components[i]);
      }
         
      let image = new Image();
      image.id = "pic"+index;
      let content = <HTMLCanvasElement>this.element;
      image.src = content.toDataURL(); 
      this.datact_jawaban[index]["image"+status] = image.src;
      console.log(this.datact_jawaban[index]);
      //console.log(image.src)
  }else{
    this.datact_jawaban[index]["image"+status] = "assets/img/no_image.png";
  }
  
 }

 dismiss() {
   this.viewCtrl.dismiss();
 }

 private mapToMoment(date: string): string {
  if (moment(date).isValid()) {
    return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
  }
  return "-";
}

}