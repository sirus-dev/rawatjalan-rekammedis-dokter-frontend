import { DokterService } from './../../services/providers/dokter-service';
import { ModalController, NavController, NavParams, ToastController, ViewController, IonicPage } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';

@IonicPage()
@Component({
    selector: 'consultation-modal',
    templateUrl: 'consultation.component.html'
})

export class Consultation implements OnInit {
    dataConsul =  false;
    listConsul:Array<any> = [];
    statusjawaban = false;
    nomedrec:string;
    
    constructor(public viewCtrl: ViewController, public modalCtrl: ModalController, public toastCtrl: ToastController, public dokterService: DokterService, private navParams: NavParams, public navCtrl:NavController) {

      this.nomedrec = this.navParams.get('personalNoRM');

     }

    ngOnInit() {

      this.dokterService.getDataConsultationTujuan(this.navParams.get('personalNodok')).subscribe(({data})=>{
        let listkonsultasi = [...data.consultationByTujuan];
        if(listkonsultasi){
          for(let i = 0; i < listkonsultasi.length; i++){
            if(listkonsultasi[i].jawaban == ""){
              this.statusjawaban = false;
            }else{
              this.statusjawaban = true;
            }
            this.dokterService.getDataDoctor(listkonsultasi[i].nodok).subscribe(({data}) => {
              var doctorstr = JSON.stringify(data);
              var doctorparse = JSON.parse(doctorstr);
              let dokdata = doctorparse.doctorByNodok;
              let namadok = dokdata.nama_dok;
              if(dokdata){
                  this.dataConsul =  true;
                  let message = "Konsultasi masuk dari Dokter "+namadok;
                  this.listConsul.push({nama_dok: namadok, message:message, no_medrec:listkonsultasi[i].no_medrec});
              }
            });
          }
        }
      });

     }

    dismiss() {
      this.viewCtrl.dismiss();
    }

    createModalWithParams(page:any, no_medrec:string){
      this.navCtrl.push(page, {personalNoRM: this.navParams.get('personalNoRM'), personalNodok: this.navParams.get('personalNodok'), dataPatient: this.navParams.get('dataPatient'), doctorName: this.navParams.get('doctorName')});
    }

    createModalWithParamsNoMedrec(page:any, no_medrec:string){
      this.navCtrl.push(page, {personalNoRM: no_medrec, personalNodok: this.navParams.get('personalNodok'), dataPatient: this.navParams.get('dataPatient'), doctorName: this.navParams.get('doctorName')});
    }

    toastMessage(message:string,duration:number,position:string){

      let toast = this.toastCtrl.create({
          message: message,
          duration: duration,
          position: position
      });
      toast.present(toast);

    }
    
      openModal(page:any, no_medrec:string) {
        if(no_medrec){
        switch(page){
    
          case 'References':
          return this.createModalWithParams('References', no_medrec);
    
          case 'Result':
          return this.createModalWithParams('Result', no_medrec);

          case 'Reply':
          return this.createModalWithParamsNoMedrec('Reply', no_medrec);
          
        }
      }else{
        this.toastMessage("Belum ada pasien yang anda periksa sekarang",4000,"top");
      }
    
      }
}