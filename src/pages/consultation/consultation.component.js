var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DokterService } from './../../services/providers/dokter-service';
import { ModalController, NavController, NavParams, ToastController, ViewController, IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
var Consultation = /** @class */ (function () {
    function Consultation(viewCtrl, modalCtrl, toastCtrl, dokterService, navParams, navCtrl) {
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.dataConsul = false;
        this.listConsul = [];
        this.statusjawaban = false;
        this.nomedrec = this.navParams.get('personalNoRM');
    }
    Consultation.prototype.ngOnInit = function () {
        var _this = this;
        this.dokterService.getDataConsultationTujuan(this.navParams.get('personalNodok')).subscribe(function (_a) {
            var data = _a.data;
            var listkonsultasi = data.consultationByTujuan.slice();
            if (listkonsultasi) {
                var _loop_1 = function (i) {
                    if (listkonsultasi[i].jawaban == "") {
                        _this.statusjawaban = false;
                    }
                    else {
                        _this.statusjawaban = true;
                    }
                    _this.dokterService.getDataDoctor(listkonsultasi[i].nodok).subscribe(function (_a) {
                        var data = _a.data;
                        var doctorstr = JSON.stringify(data);
                        var doctorparse = JSON.parse(doctorstr);
                        var dokdata = doctorparse.doctorByNodok;
                        var namadok = dokdata.nama_dok;
                        if (dokdata) {
                            _this.dataConsul = true;
                            var message = "Konsultasi masuk dari Dokter " + namadok;
                            _this.listConsul.push({ nama_dok: namadok, message: message, no_medrec: listkonsultasi[i].no_medrec });
                        }
                    });
                };
                for (var i = 0; i < listkonsultasi.length; i++) {
                    _loop_1(i);
                }
            }
        });
    };
    Consultation.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Consultation.prototype.createModalWithParams = function (page, no_medrec) {
        this.navCtrl.push(page, { personalNoRM: this.navParams.get('personalNoRM'), personalNodok: this.navParams.get('personalNodok'), dataPatient: this.navParams.get('dataPatient'), doctorName: this.navParams.get('doctorName') });
    };
    Consultation.prototype.createModalWithParamsNoMedrec = function (page, no_medrec) {
        this.navCtrl.push(page, { personalNoRM: no_medrec, personalNodok: this.navParams.get('personalNodok'), dataPatient: this.navParams.get('dataPatient'), doctorName: this.navParams.get('doctorName') });
    };
    Consultation.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
    };
    Consultation.prototype.openModal = function (page, no_medrec) {
        if (no_medrec) {
            switch (page) {
                case 'References':
                    return this.createModalWithParams('References', no_medrec);
                case 'Result':
                    return this.createModalWithParams('Result', no_medrec);
                case 'Reply':
                    return this.createModalWithParamsNoMedrec('Reply', no_medrec);
            }
        }
        else {
            this.toastMessage("Belum ada pasien yang anda periksa sekarang", 4000, "top");
        }
    };
    Consultation = __decorate([
        IonicPage(),
        Component({
            selector: 'consultation-modal',
            templateUrl: 'consultation.component.html'
        }),
        __metadata("design:paramtypes", [ViewController, ModalController, ToastController, DokterService, NavParams, NavController])
    ], Consultation);
    return Consultation;
}());
export { Consultation };
//# sourceMappingURL=consultation.component.js.map