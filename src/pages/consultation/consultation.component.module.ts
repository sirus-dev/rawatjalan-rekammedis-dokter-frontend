import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { Consultation } from './consultation.component';
@NgModule({
  declarations: [
    Consultation
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Consultation)
  ]
})
export class ConsultationModule {}
