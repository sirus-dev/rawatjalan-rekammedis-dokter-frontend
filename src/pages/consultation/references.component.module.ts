import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { References } from './references.component';
@NgModule({
  declarations: [
    References
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(References)
  ]
})
export class ReferencesModule {}
