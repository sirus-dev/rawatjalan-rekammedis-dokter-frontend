import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { History } from './history.component';

@NgModule({
  declarations: [
    History
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(History)
  ]
})
export class HistoryModule {}
