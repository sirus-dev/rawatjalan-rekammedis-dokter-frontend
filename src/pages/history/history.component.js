var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { InkGrabber } from '@sirus/stylus';
import { FormBuilder } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, NavParams, LoadingController, IonicPage } from 'ionic-angular';
import moment from 'moment';
var History = /** @class */ (function () {
    function History(viewCtrl, dokterService, params, formBuilder, loadingCtrl) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.dokterService = dokterService;
        this.params = params;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.icdfix = [];
        this.obatfix = [];
        this.myDate = new Date().toISOString();
        this.datact = true;
        this.components = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.width = 1000;
        this.height = 400;
        this.timeout = 1;
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        if (this.params.get('personalNoRM')) {
            this.styluss = [false];
            this.styluso = [false];
            this.stylusa = [false];
            this.styluspo = [false];
            this.datapersonal = this.params.get('dataPatient');
            this.doctorname = this.params.get('doctorName');
            this.dokterService.getDataIntegratedNote(params.get('personalNoRM')).subscribe(function (_a) {
                var data = _a.data;
                var datactpasien = data.ctByMedrec.slice();
                var ctpasien = _this.parseDataJSON(datactpasien);
                if (datactpasien) {
                    _this.integratedNote = ctpasien;
                }
            });
        }
        this.formFilter = this.formBuilder.group({
            'dateFilter': ['']
        });
    }
    History.prototype.ngOnInit = function () {
        this.myDate = moment().format();
    };
    /**
     * template parse array from array string
     *
     * @param {any} obj
     * @returns
     * @memberof History
     */
    History.prototype.generateArray = function (obj) {
        if (obj) {
            return JSON.parse(obj);
        }
        else {
            return null;
        }
    };
    /**
     * template mapping date with moment
     *
     * @private
     * @param {string} date
     * @returns {string}
     * @memberof History
     */
    History.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    /**
     * template parse array from array string
     *
     * @private
     * @param {any} data
     * @returns
     * @memberof History
     */
    History.prototype.parseDataJSON = function (data) {
        var strData = JSON.stringify(data);
        var parsData = JSON.parse(strData);
        return parsData;
    };
    /**
     * template loading
     *
     * @private
     * @memberof History
     */
    History.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Mohon Tunggu...",
            duration: 2000
        });
        loader.present();
    };
    /**
     * dismiss history page
     *
     * @memberof History
     */
    History.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * filter data based on date from datepicker
     *
     * @param {any} formData
     * @memberof History
     */
    History.prototype.filterDate = function (formData) {
        var _this = this;
        this.presentLoading();
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        var date = new Date(formData.dateFilter);
        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.dt = date.getDate();
        this.day = date.getDay();
        if (this.month < 10) {
            this.month = '0' + this.month;
        }
        if (this.dt < 10) {
            this.dt = '0' + this.dt;
        }
        var datefilter = this.year + '-' + this.month + '-' + this.dt;
        this.dokterService.getIntegratedNoteByDateQuery(this.params.get('personalNoRM'), datefilter)
            .subscribe(function (_a) {
            var data = _a.data;
            var datactpasien = data.ctByDate.slice();
            var ctparse = _this.parseDataJSON(datactpasien);
            if (datactpasien) {
                _this.datact = true;
                _this.integratedNote = ctparse;
            }
            else {
                _this.datact = false;
            }
        });
    };
    /**
     * reset data to general
     *
     * @memberof History
     */
    History.prototype.resetData = function () {
        var _this = this;
        this.presentLoading();
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        this.datact = true;
        this.dokterService.getDataIntegratedNote(this.params.get('personalNoRM')).subscribe(function (_a) {
            var data = _a.data;
            var datactpasien = data.ctByMedrec.slice();
            var ctparse = _this.parseDataJSON(datactpasien);
            if (datactpasien) {
                _this.integratedNote = ctparse;
            }
        });
    };
    /**
     * template function stroke stylus
     *
     * @param {*} index
     * @param {*} strokedata
     * @param {*} status
     * @memberof History
     */
    History.prototype.drawcanvas = function (index, strokedata, status) {
        this.components = [];
        if (status == "s") {
            this.styluss[index] = !this.styluss[index];
        }
        else if (status == "o") {
            this.styluso[index] = !this.styluso[index];
        }
        else if (status == "a") {
            this.stylusa[index] = !this.stylusa[index];
        }
        else if (status == "po") {
            this.styluspo[index] = !this.styluspo[index];
        }
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById(index);
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            this.inkGrabber = new InkGrabber(ctx);
            this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    var stroke_1 = this.inkGrabber.getStroke();
                    this.components.push(stroke_1);
                    this.inkGrabber = new InkGrabber(ctx);
                    this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            var stroke = this.inkGrabber.getStroke();
            this.components.push(stroke);
            this.inkGrabberFinal = new InkGrabber(ctx);
            for (var i = 0; i < this.components.length; i++) {
                this.inkGrabberFinal.drawComponent(this.components[i]);
            }
            var image = new Image();
            image.id = "pic" + index;
            var content = this.element;
            image.src = content.toDataURL();
            this.integratedNote[index]["image" + status] = image.src;
        }
        else {
            this.integratedNote[index]["image" + status] = "assets/img/no_image.png";
        }
    };
    History = __decorate([
        IonicPage(),
        Component({
            selector: 'modal-history',
            templateUrl: 'history.component.html'
        }),
        __metadata("design:paramtypes", [ViewController,
            DokterService,
            NavParams,
            FormBuilder,
            LoadingController])
    ], History);
    return History;
}());
export { History };
//# sourceMappingURL=history.component.js.map