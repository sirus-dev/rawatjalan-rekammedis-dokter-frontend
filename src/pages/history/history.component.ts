import { InkGrabber, AbstractComponent } from '@sirus/stylus';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams, LoadingController, IonicPage } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'modal-history',
  templateUrl: 'history.component.html'
})
export class History implements OnInit {

  datapersonal: Array<any>;
  icdfix: Array<any> = [];
  obatfix: Array<any> = [];
  integratedNote: Array<any>;
  doctorname: string;
  myDate: String = new Date().toISOString();

  dt: any;
  month: any;
  year: any;
  hour: any;
  day: any;
  personalTglLahir: string;
  datact = true;

  private inkGrabber: InkGrabber;
  private inkGrabberFinal: InkGrabber;
  private element: HTMLElement;
  private components: AbstractComponent[] = [];
  arrayX: number[] = [];
  arrayY: number[] = [];
  arrayT: number[] = [];

  width: number = 1000;
  height: number = 400;
  timeout: number = 1;
  imgSource: any;

  styluss: boolean[] = [false];
  styluso: boolean[] = [false];
  stylusa: boolean[] = [false];
  styluspo: boolean[] = [false];

  private formFilter: FormGroup;

  constructor(public viewCtrl: ViewController,
    private dokterService: DokterService,
    public params: NavParams,
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController) {
    if (this.params.get('personalNoRM')) {
      this.styluss      = [false];
      this.styluso      = [false];
      this.stylusa      = [false];
      this.styluspo     = [false];
      this.datapersonal = this.params.get('dataPatient');
      this.doctorname   = this.params.get('doctorName');
      this.dokterService.getDataIntegratedNote(params.get('personalNoRM')).subscribe(({ data }) => {
        let datactpasien  = [...data.ctByMedrec];
        let ctpasien      = this.parseDataJSON(datactpasien);
        if (datactpasien) {
          this.integratedNote = ctpasien;
        }
      });
    }
    this.formFilter = this.formBuilder.group({
      'dateFilter': ['']
    });
  }

  ngOnInit() {
    this.myDate = moment().format();
  }

  /**
   * template parse array from array string
   * 
   * @param {any} obj 
   * @returns 
   * @memberof History
   */
  generateArray(obj) {
    if (obj) {
      return JSON.parse(obj);
    } else {
      return null;
    }
  }

  /**
   * template mapping date with moment
   * 
   * @private
   * @param {string} date 
   * @returns {string} 
   * @memberof History
   */
  private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
  }

  /**
   * template parse array from array string
   * 
   * @private
   * @param {any} data 
   * @returns 
   * @memberof History
   */
  private parseDataJSON(data) {
    const strData  = JSON.stringify(data);
    const parsData = JSON.parse(strData);
    return parsData;
  }

  /**
   * template loading
   * 
   * @private
   * @memberof History
   */
  private presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Mohon Tunggu...",
      duration: 2000
    });
    loader.present();
  }

  /**
   * dismiss history page
   * 
   * @memberof History
   */
  dismiss() {
    this.viewCtrl.dismiss();
  }

  /**
   * filter data based on date from datepicker
   * 
   * @param {any} formData 
   * @memberof History
   */
  filterDate(formData) {
    this.presentLoading();
    this.styluss  = [false];
    this.styluso  = [false];
    this.stylusa  = [false];
    this.styluspo = [false];
    let date      = new Date(formData.dateFilter);
    this.year     = date.getFullYear();
    this.month    = date.getMonth() + 1;
    this.dt       = date.getDate();
    this.day      = date.getDay();
    if (this.month < 10) {
      this.month  = '0' + this.month
    }
    if (this.dt < 10) {
      this.dt     = '0' + this.dt
    }
    let datefilter = this.year + '-' + this.month + '-' + this.dt;
    this.dokterService.getIntegratedNoteByDateQuery(this.params.get('personalNoRM'), datefilter)
    .subscribe(({ data }) => {
      let datactpasien = [...data.ctByDate];
      var ctparse      = this.parseDataJSON(datactpasien);
      if (datactpasien) {
        this.datact = true;
        this.integratedNote = ctparse;
      } else {
        this.datact = false;
      }
    });
  }

  /**
   * reset data to general
   * 
   * @memberof History
   */
  resetData() {
    this.presentLoading();
    this.styluss  = [false];
    this.styluso  = [false];
    this.stylusa  = [false];
    this.styluspo = [false];
    this.datact   = true;
    this.dokterService.getDataIntegratedNote(this.params.get('personalNoRM')).subscribe(({ data }) => {
      let datactpasien = [...data.ctByMedrec];
      var ctparse      = this.parseDataJSON(datactpasien);
      if (datactpasien) {
        this.integratedNote = ctparse;
      }
    });
  }
  /**
   * template function stroke stylus
   * 
   * @param {*} index 
   * @param {*} strokedata 
   * @param {*} status 
   * @memberof History
   */
  drawcanvas(index: any, strokedata: any, status: any) {
    this.components = [];
    if (status == "s") {
      this.styluss[index] = !this.styluss[index];
    } else if (status == "o") {
      this.styluso[index] = !this.styluso[index];
    } else if (status == "a") {
      this.stylusa[index] = !this.stylusa[index];
    } else if (status == "po") {
      this.styluspo[index] = !this.styluspo[index];
    }
    let x: number[] = [];
    let y: number[] = [];
    let t: number[] = [];

    if (strokedata !== null) {
      let datastroke = JSON.parse(strokedata);

      x = x.concat(datastroke[0]);
      y = y.concat(datastroke[1]);
      t = t.concat(datastroke[2]);

      x = x.concat(null);
      y = y.concat(null);
      t = t.concat(null);

      this.element = document.getElementById(index);
      let imageRenderingCanvas = <HTMLCanvasElement>this.element;
      let ctx = imageRenderingCanvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      this.inkGrabber = new InkGrabber(ctx);

      this.inkGrabber.startCapture(x[0], y[0], t[0]);
      for (let i = 1; i < x.length - 1; i++) {

        if (x[i] == null) {
          this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
          let stroke = this.inkGrabber.getStroke();
          this.components.push(stroke);
          this.inkGrabber = new InkGrabber(ctx);
          this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
        }
        else {
          this.inkGrabber.continueCapture(x[i], y[i], t[i]);
        }
      }
      this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);

      let stroke = this.inkGrabber.getStroke();
      this.components.push(stroke);
      this.inkGrabberFinal = new InkGrabber(ctx);
      for (let i = 0; i < this.components.length; i++) {
        this.inkGrabberFinal.drawComponent(this.components[i]);
      }

      let image = new Image();
      image.id = "pic" + index;
      let content = <HTMLCanvasElement>this.element;
      image.src = content.toDataURL();
      this.integratedNote[index]["image" + status] = image.src;
    } else {
      this.integratedNote[index]["image" + status] = "assets/img/no_image.png";
    }

  }

}