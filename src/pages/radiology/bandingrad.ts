import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'bandingrad-modal',
    templateUrl: 'bandingrad.html'
})

export class BandingRad {
    
 datapersonal:Array<any>;
 tanggal1: string;
 tanggal2: string;
 myDate: String = new Date().toISOString();

 constructor(public viewCtrl: ViewController) {

    this.datapersonal = [
        {name:'Jajang Jaenudin',dokter:'Dr. Asep Surasep',norm:'101011',kelas:'RJ',jaminan:'BPJS',tgl:'22 Mei 1997',umur:'20 tahun',gender:'Pria'}
    ];

 }

 dismiss() {

   this.viewCtrl.dismiss();

 }

}