var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';
import groupBy from 'lodash.groupby';
var Radiology = /** @class */ (function () {
    function Radiology(viewCtrl, toastCtrl, dokterService, params) {
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.params = params;
        this.datapersonal = [];
        this.myDate = new Date().toISOString();
        this.diagnosa = false;
        this.listAddRadio = [];
        this.listRequestRadio = [];
        if (params.get('icdx').length > 0) {
            this.listicd = params.get('icdx');
            this.diagnosa = true;
        }
        this.datapersonal = this.params.get('dataPatient');
        this.doctorname = this.params.get('doctorName');
    }
    Radiology.prototype.ngOnInit = function () {
        var _this = this;
        this.dokterService.loadDataMasterRadiology().subscribe(function (_a) {
            var data = _a.data;
            var mapDataRad = groupBy(data.masterRadiologiAll, 'kelompok');
            _this.listDataRad = mapDataRad;
            _this.keyDataRad = Object.keys(mapDataRad);
        });
    };
    Radiology.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
        this.viewCtrl.dismiss();
    };
    Radiology.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    Radiology.prototype.addRadio = function (data, id, i) {
        var isChecked = false;
        if (this.listAddRadio.length > 0) {
            var findarray = this.listAddRadio.indexOf(data) > -1;
            var indexarray = this.listAddRadio.findIndex(function (x) { return x.id_lab == id; });
            if (findarray == true) {
                this.listAddRadio.splice(indexarray, 1);
            }
            else {
                this.listAddRadio.push(data);
            }
        }
        else {
            this.listAddRadio.push(data);
        }
    };
    Radiology.prototype.submitRad = function () {
        var _this = this;
        var datenow = moment().format('DD-MM-YYYY');
        var datenowstr = moment().format('DDMMYYYY');
        if (this.indikasi) {
            this.listRequestRadio = [{
                    id_integrasi: this.datapersonal[0].no_medrec + datenowstr,
                    no_medrec: this.datapersonal[0].no_medrec,
                    indikasi: this.indikasi,
                    diagnosis: '',
                    lab: JSON.stringify(this.listAddRadio),
                    tgl_permintaan: datenow
                }];
        }
        else if (this.listicd) {
            this.listRequestRadio = [{
                    id_integrasi: this.datapersonal[0].no_medrec + datenowstr,
                    no_medrec: this.datapersonal[0].no_medrec,
                    indikasi: '',
                    diagnosis: JSON.stringify(this.listicd),
                    lab: JSON.stringify(this.listAddRadio),
                    tgl_permintaan: datenow
                }];
        }
        console.log(this.listRequestRadio);
        this.dokterService.addRadio(this.listRequestRadio[0]).subscribe(function (_a) {
            var data = _a.data;
            console.log(data);
            _this.toastMessage('Permintaan Radiologi berhasil terkirim', 4000, 'top');
        });
    };
    Radiology.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Radiology = __decorate([
        IonicPage(),
        Component({
            selector: 'rad-modal',
            templateUrl: 'radiology.component.html'
        }),
        __metadata("design:paramtypes", [ViewController, ToastController, DokterService, NavParams])
    ], Radiology);
    return Radiology;
}());
export { Radiology };
//# sourceMappingURL=radiology.component.js.map