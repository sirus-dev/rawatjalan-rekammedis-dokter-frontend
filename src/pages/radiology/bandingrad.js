var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
var BandingRad = /** @class */ (function () {
    function BandingRad(viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.myDate = new Date().toISOString();
        this.datapersonal = [
            { name: 'Jajang Jaenudin', dokter: 'Dr. Asep Surasep', norm: '101011', kelas: 'RJ', jaminan: 'BPJS', tgl: '22 Mei 1997', umur: '20 tahun', gender: 'Pria' }
        ];
    }
    BandingRad.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    BandingRad = __decorate([
        Component({
            selector: 'bandingrad-modal',
            templateUrl: 'bandingrad.html'
        }),
        __metadata("design:paramtypes", [ViewController])
    ], BandingRad);
    return BandingRad;
}());
export { BandingRad };
//# sourceMappingURL=bandingrad.js.map