import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';
import groupBy from 'lodash.groupby';

@IonicPage()
@Component({
  selector: 'rad-modal',
  templateUrl: 'radiology.component.html'
})

export class Radiology {

  datapersonal: Array<any> = [];
  doctorname: any;
  myDate: String = new Date().toISOString();
  personalTglLahir: string;
  listicd: any;
  diagnosa = false;
  indikasi: string;

  keyDataRad: any;
  listDataRad: any;

  listAddRadio: Array<any> = [];

  listRequestRadio: Array<any> = [];

  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController, private dokterService: DokterService, public params: NavParams) {

    if (params.get('icdx').length > 0) {
      this.listicd = params.get('icdx');
      this.diagnosa = true;
    }
    this.datapersonal = this.params.get('dataPatient');
    this.doctorname = this.params.get('doctorName');

  }

  ngOnInit() {
    this.dokterService.loadDataMasterRadiology().subscribe(({ data }) => {
      const mapDataRad = groupBy(data.masterRadiologiAll, 'kelompok');
      this.listDataRad = mapDataRad;
      this.keyDataRad = Object.keys(mapDataRad);
    });
  }

  toastMessage(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });
    toast.present(toast);
    this.viewCtrl.dismiss();
  }

  private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
  }

  addRadio(data, id, i: number) {
    let isChecked = false;
    if (this.listAddRadio.length > 0) {
      let findarray = this.listAddRadio.indexOf(data) > -1;
      let indexarray = this.listAddRadio.findIndex(x => x.id_lab == id);
      if (findarray == true) {
        this.listAddRadio.splice(indexarray, 1);
      } else {
        this.listAddRadio.push(data);
      }
    } else {
      this.listAddRadio.push(data);
    }
  }

  submitRad() {
    const datenow = moment().format('DD-MM-YYYY');
    const datenowstr = moment().format('DDMMYYYY');
    if (this.indikasi) {
      this.listRequestRadio = [{
        id_integrasi: this.datapersonal[0].no_medrec + datenowstr,
        no_medrec: this.datapersonal[0].no_medrec,
        indikasi: this.indikasi,
        diagnosis: '',
        lab: JSON.stringify(this.listAddRadio),
        tgl_permintaan: datenow
      }];
    } else if (this.listicd) {
      this.listRequestRadio = [{
        id_integrasi: this.datapersonal[0].no_medrec + datenowstr,
        no_medrec: this.datapersonal[0].no_medrec,
        indikasi: '',
        diagnosis: JSON.stringify(this.listicd),
        lab: JSON.stringify(this.listAddRadio),
        tgl_permintaan: datenow
      }];
    }

    console.log(this.listRequestRadio)
    this.dokterService.addRadio(this.listRequestRadio[0]).subscribe(({ data }) => {
      console.log(data);
      this.toastMessage('Permintaan Radiologi berhasil terkirim', 4000, 'top');
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}