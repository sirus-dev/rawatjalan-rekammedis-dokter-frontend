var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Radiology } from './radiology.component';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { BandingRad } from './bandingrad';
var RadiologyModule = /** @class */ (function () {
    function RadiologyModule() {
    }
    RadiologyModule = __decorate([
        NgModule({
            declarations: [
                Radiology,
                BandingRad
            ],
            imports: [
                IonicModule,
                IonicPageModule.forChild(Radiology)
            ]
        })
    ], RadiologyModule);
    return RadiologyModule;
}());
export { RadiologyModule };
//# sourceMappingURL=radiology.component.module.js.map