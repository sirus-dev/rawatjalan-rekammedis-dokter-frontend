import { Radiology } from './radiology.component';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { BandingRad } from './bandingrad';

@NgModule({
  declarations: [
    Radiology,
    BandingRad
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Radiology)
  ]
})
export class RadiologyModule {}
