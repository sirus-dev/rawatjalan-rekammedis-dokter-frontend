var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Inform } from './../../models/inform.model';
import { AlertController, IonicPage } from 'ionic-angular';
import { DokterService } from '../../services/providers/dokter-service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams } from 'ionic-angular';
import moment from 'moment';
var InformConsent = /** @class */ (function () {
    function InformConsent(viewCtrl, toastCtrl, formBuilder, dokterService, navParams, alertCtrl) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.dokterService = dokterService;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.ttdMenyatakan = false;
        this.ttdDokter = false;
        this.ttdSaksi1 = false;
        this.ttdSaksi2 = false;
        this.ttdMenyatakanNolak = false;
        this.ttdSaksi1Nolak = false;
        this.ttdSaksi2Nolak = false;
        this.ttdInfoDokter = false;
        this.ttdInfoPasien = false;
        this.ttdInfoSaksi1 = false;
        this.ttdInfoSaksi2 = false;
        this.statusTag = false;
        this.datapersonal = [];
        this.myDate = new Date().toISOString();
        this.inform = "pemberian";
        this.selection = "PERSETUJUAN";
        this.selectedAll = false;
        this.selectedAllChecklist = false;
        this.selectedAllAnestesi = false;
        this.stylusmode = true;
        this.submitPersetujuan = true;
        this.submitInfo = true;
        this.form_isi_informasi = false;
        this.form_checklist_informasi = false;
        this.form_anestesi = false;
        this.informData = new Inform();
        if (this.navParams.get('specialist') == "Spesialis Bedah Umum" || this.navParams.get('specialist') == "Bedah Orthopedi" || this.navParams.get('specialist') == "Spesialis Mata" || this.navParams.get('specialist') == "Spesialis THT") {
            this.form_isi_informasi = false;
            this.form_checklist_informasi = false;
            this.form_anestesi = true;
            this.information_inform = "isi_informasi";
        }
        else if (this.navParams.get('specialist') == "Spesialis Kebidanan dan K") {
            this.form_isi_informasi = true;
            this.form_checklist_informasi = false;
            this.form_anestesi = true;
            this.information_inform = "checklist_informasi";
        }
        else if (this.navParams.get('specialist') == "Spesialis Anasthesi") {
            this.form_isi_informasi = true;
            this.form_checklist_informasi = true;
            this.form_anestesi = false;
            this.information_inform = "anestesi";
        }
        if (this.navParams.get('personalNoRM')) {
            this.datapersonal = this.navParams.get('dataPatient');
            if (this.datapersonal[0].kelamin == "L") {
                this.informData.penandaan_operasi = "pria";
            }
            else {
                this.informData.penandaan_operasi = "wanita";
            }
            this.dokterService.getInformConsentQuery(this.navParams.get('personalNoRM')).subscribe(function (_a) {
                var data = _a.data;
                var strinform = JSON.stringify(data);
                var parsinform = JSON.parse(strinform);
                var datainform = parsinform.informConsent0;
                if (datainform.length == 0) {
                    _this.dataInform = [{
                            no_inform: '',
                            no_medrec: '',
                            nama_ortu: '',
                            umur_ortu: '',
                            kelamin_ortu: '',
                            alamat: '',
                            ktp: '',
                            tgl_inform: '',
                            jam_inform: '',
                            yg_menyatakan: '',
                            file_yg_menyatakan: '',
                            dokter_operator: '',
                            file_dokter_operator: '',
                            saksi1: '',
                            file_saksi1: '',
                            saksi2: '',
                            file_saksi2: '',
                            tindakan_operasi: '',
                            tipe_tindakan: '',
                            pelaksana_tindakan: '',
                            pemberi_info: '',
                            penerima_info: '',
                            info_diag_kerja: '',
                            checklist_info_diag_kerja: '',
                            dasar_diag: '',
                            checklist_dasar_diag: '',
                            tindakan_dokter: '',
                            checklist_tindakan_dokter: '',
                            indikasi_tindakan: '',
                            checklist_indikasi_tindakan: '',
                            tatacara: '',
                            checklist_tatacara: '',
                            tujuan: '',
                            checklist_tujuan: '',
                            risiko: '',
                            checklist_risiko: '',
                            komplikasi: '',
                            checklist_komplikasi: '',
                            prognosis: '',
                            checklist_prognosis: '',
                            alternatif: '',
                            checklist_alternatif: '',
                            lain: '',
                            checklist_lain: '',
                            penandaan_operasi: '',
                            a_u_tindakan_dokter: '',
                            a_u_tatacara: '',
                            a_u_tujuan: '',
                            a_u_risiko: '',
                            a_u_komplikasi: '',
                            a_r_tindakan_dokter: '',
                            a_r_tatacara: '',
                            a_r_tujuan: '',
                            a_r_risiko: '',
                            a_r_komplikasi: '',
                            a_r_alternatif: '',
                            a_l_tindakan_dokter: '',
                            a_l_tatacara: '',
                            a_l_tujuan: '',
                            a_l_risiko: '',
                            a_l_komplikasi: '',
                            a_l_alternatif: '',
                            sedasi_tindakan_dokter: '',
                            sedasi_tatacara: '',
                            sedasi_tujuan: '',
                            sedasi_risiko: '',
                            sedasi_komplikasi: '',
                            sedasi_prognosis: '',
                            sedasi_lain: '',
                            flag: ''
                        }];
                }
                else {
                    _this.dataInform = [datainform[0]];
                    _this.informData.no_inform_pasien = datainform[0].no_inform;
                    _this.informData.checklist_info_diag_kerja = datainform[0].checklist_info_diag_kerja;
                    _this.informData.checklist_dasar_diag = datainform[0].checklist_dasar_diag;
                    _this.informData.checklist_tindakan_dokter = datainform[0].checklist_tindakan_dokter;
                    _this.informData.checklist_indikasi_tindakan = datainform[0].checklist_indikasi_tindakan;
                    _this.informData.checklist_tatacara = datainform[0].checklist_tatacara;
                    _this.informData.checklist_tujuan = datainform[0].checklist_tujuan;
                    _this.informData.checklist_risiko = datainform[0].checklist_risiko;
                    _this.informData.checklist_komplikasi = datainform[0].checklist_komplikasi;
                    _this.informData.checklist_prognosis = datainform[0].checklist_prognosis;
                    _this.informData.checklist_alternatif = datainform[0].checklist_alternatif;
                    _this.informData.checklist_lain = datainform[0].checklist_lain;
                    _this.informData.a_u_tindakan_dokter = datainform[0].a_u_tindakan_dokter;
                    _this.informData.a_u_tatacara = datainform[0].a_u_tatacara;
                    _this.informData.a_u_tujuan = datainform[0].a_u_tujuan;
                    _this.informData.a_u_risiko = datainform[0].a_u_risiko;
                    _this.informData.a_u_komplikasi = datainform[0].a_u_komplikasi;
                    _this.informData.a_r_tindakan_dokter = datainform[0].a_r_tindakan_dokter;
                    _this.informData.a_r_tatacara = datainform[0].a_r_tatacara;
                    _this.informData.a_r_tujuan = datainform[0].a_r_tujuan;
                    _this.informData.a_r_risiko = datainform[0].a_r_risiko;
                    _this.informData.a_r_komplikasi = datainform[0].a_r_komplikasi;
                    _this.informData.a_r_alternatif = datainform[0].a_r_alternatif;
                    _this.informData.a_l_tindakan_dokter = datainform[0].a_l_tindakan_dokter;
                    _this.informData.a_l_tatacara = datainform[0].a_l_tatacara;
                    _this.informData.a_l_tujuan = datainform[0].a_l_tujuan;
                    _this.informData.a_l_risiko = datainform[0].a_l_risiko;
                    _this.informData.a_l_komplikasi = datainform[0].a_l_komplikasi;
                    _this.informData.a_l_alternatif = datainform[0].a_l_alternatif;
                    _this.informData.sedasi_tindakan_dokter = datainform[0].sedasi_tindakan_dokter;
                    _this.informData.sedasi_tatacara = datainform[0].sedasi_tatacara;
                    _this.informData.sedasi_tujuan = datainform[0].sedasi_tujuan;
                    _this.informData.sedasi_risiko = datainform[0].sedasi_risiko;
                    _this.informData.sedasi_komplikasi = datainform[0].sedasi_komplikasi;
                    _this.informData.sedasi_prognosis = datainform[0].sedasi_prognosis;
                    _this.informData.anestesi_lain = datainform[0].sedasi_lain;
                    _this.informData.tindakan_operasi = datainform[0].tindakan_operasi;
                    if (_this.informData.no_inform_pasien) {
                        _this.submitInfo = false;
                    }
                }
            });
        }
    }
    InformConsent.prototype.ngOnInit = function () {
        var _this = this;
        this.patientName = this.datapersonal[0].nama_pasien;
        this.doctorName = this.navParams.get('doctorName');
        this.myDate = moment().format();
        var salt = Math.floor(Math.random() * 10000);
        var no_inform = salt + this.navParams.get('personalNoRM');
        this.dokterService.getPatientById(this.navParams.get('personalNoRM')).subscribe(function (_a) {
            var data = _a.data;
            var date = new Date();
            var year_now = date.getFullYear();
            var year_now_str = year_now.toString();
            var dataguarantor = data.patientByNorm;
            _this.informData.nama_ortu = dataguarantor.nama_penjamin;
            _this.informData.umur_ortu = Number(year_now_str) - Number(dataguarantor.tahun2);
            _this.informData.alamat = dataguarantor.jln2 + " No. " + dataguarantor.no2 + ", RT/RW. " + dataguarantor.rt2 + "/" + dataguarantor.rw2 + ", Desa. " + dataguarantor.desa2 + ", Kec. " + dataguarantor.kecamatan2 + ", " + dataguarantor.kota2;
            _this.informData.ktp = dataguarantor.no_ktp2;
        });
        this.formInformConsentInfo = this.formBuilder.group({
            'no_inform': [no_inform, Validators.required],
            'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
            'pelaksana_tindakan': [this.doctorName, Validators.required],
            'pemberi_info': [this.doctorName, Validators.required],
            'penerima_info': [this.patientName, Validators.required],
            'info_diag_kerja': [''],
            'checklist_info_diag_kerja': [],
            'dasar_diag': [''],
            'checklist_dasar_diag': [],
            'tindakan_dokter': [''],
            'checklist_tindakan_dokter': [],
            'indikasi_tindakan': [''],
            'checklist_indikasi_tindakan': [],
            'tatacara': [''],
            'checklist_tatacara': [],
            'tujuan': [''],
            'checklist_tujuan': [],
            'risiko': [''],
            'checklist_risiko': [],
            'komplikasi': [''],
            'checklist_komplikasi': [],
            'prognosis': [''],
            'checklist_prognosis': [],
            'alternatif': [''],
            'checklist_alternatif': [],
            'checklist_lain': [],
            'tt_dokter': [],
            'tt_pasien': [],
            'tt_saksi1': [],
            'tt_saksi2': []
        });
        this.formInformConsentInfoChecklist = this.formBuilder.group({
            'no_inform': [no_inform, Validators.required],
            'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
            'pelaksana_tindakan': [this.doctorName, Validators.required],
            'pemberi_info': [this.doctorName, Validators.required],
            'penerima_info': [this.patientName, Validators.required],
            'info_diag_kerja': [''],
            'checklist_info_diag_kerja': [],
            'dasar_diag': [''],
            'checklist_dasar_diag': [],
            'tindakan_dokter': [''],
            'checklist_tindakan_dokter': [],
            'indikasi_tindakan': [''],
            'checklist_indikasi_tindakan': [],
            'tatacara': [''],
            'checklist_tatacara': [],
            'tujuan': [''],
            'checklist_tujuan': [],
            'risiko': [''],
            'checklist_risiko': [],
            'komplikasi': [''],
            'checklist_komplikasi': [],
            'prognosis': [''],
            'checklist_prognosis': [],
            'alternatif': [''],
            'checklist_alternatif': [],
            'checklist_lain': [],
            'tt_dokter': [],
            'tt_pasien': [],
            'tt_saksi1': [],
            'tt_saksi2': []
        });
        this.formInformConsentAnestesi = this.formBuilder.group({
            'no_inform': [no_inform, Validators.required],
            'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
            'pelaksana_tindakan': [this.doctorName, Validators.required],
            'pemberi_info': [this.doctorName, Validators.required],
            'penerima_info': [this.patientName, Validators.required],
            'a_u_tindakan_dokter': [],
            'a_u_tatacara': [],
            'a_u_tujuan': [],
            'a_u_risiko': [],
            'a_u_komplikasi': [],
            'a_r_tindakan_dokter': [],
            'a_r_tatacara': [],
            'a_r_tujuan': [],
            'a_r_risiko': [],
            'a_r_komplikasi': [],
            'a_r_alternatif': [],
            'a_l_tindakan_dokter': [],
            'a_l_tatacara': [],
            'a_l_tujuan': [],
            'a_l_risiko': [],
            'a_l_komplikasi': [],
            'a_l_alternatif': [],
            'sedasi_tindakan_dokter': [],
            'sedasi_tatacara': [],
            'sedasi_tujuan': [],
            'sedasi_risiko': [],
            'sedasi_komplikasi': [],
            'sedasi_prognosis': [],
            'sedasi_lain': [],
            'tt_dokter': [],
            'tt_pasien': [],
            'tt_saksi1': [],
            'tt_saksi2': []
        });
        this.formInformConsentApproval = this.formBuilder.group({
            'no_inform': [''],
            'nama_ortu': [this.informData.nama_ortu],
            'umur_ortu': [this.informData.umur_ortu],
            'kelamin_ortu': [''],
            'alamat': [this.informData.alamat],
            'ktp': [this.informData.ktp],
            'tgl_inform': [''],
            'jam_inform': [''],
            'yg_menyatakan': [''],
            'dokter_operator': [],
            'saksi1': [''],
            'saksi2': [''],
            'tindakan_operasi': ['', Validators.required],
            'tipe_tindakan': ['']
        });
        this.formInformConsentNotApproval = this.formBuilder.group({
            'no_inform': [''],
            'nama_ortu': [this.informData.nama_ortu],
            'umur_ortu': [this.informData.umur_ortu],
            'kelamin_ortu': [''],
            'alamat': [this.informData.alamat],
            'ktp': [this.informData.ktp],
            'tgl_inform': [''],
            'jam_inform': [''],
            'yg_menyatakan': [''],
            'dokter_operator': [],
            'saksi1': [''],
            'saksi2': [''],
            'tindakan_operasi': ['', Validators.required],
            'tipe_tindakan': ['']
        });
        this.formInformConsentTagging = this.formBuilder.group({
            'no_inform': [''],
            'penandaan_operasi': []
        });
    };
    InformConsent.prototype.getInfromType = function (tipe_inform) {
        var _this = this;
        this.dokterService.getTypeInform(tipe_inform).subscribe(function (_a) {
            var data = _a.data;
            _this.anestesi = [data.informTypeByName];
        });
    };
    InformConsent.prototype.dismiss = function () {
        var _this = this;
        var datainfrom = this.formInformConsentInfo.value;
        if (datainfrom.pelaksana_tindakan == "" && datainfrom.penerima_info == "" && datainfrom.pemberi_info == "") {
            this.viewCtrl.dismiss();
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Data Inform Consent',
                subTitle: 'Anda yakin untuk menutup formulir Inform Consent? Informasi yang anda masukkan akan hilang',
                buttons: [{
                        text: 'YA',
                        role: 'YA',
                        handler: function () {
                            _this.viewCtrl.dismiss();
                        }
                    }, {
                        text: 'TIDAK',
                        role: 'TIDAK',
                    }]
            });
            alert_1.present();
        }
    };
    InformConsent.prototype.toastMessage = function (message, duration, position, flag) {
        if (flag == 0) {
            var toast = this.toastCtrl.create({
                message: message,
                duration: duration,
                position: position
            });
            toast.present(toast);
        }
        else {
            var toast = this.toastCtrl.create({
                message: message,
                duration: duration,
                position: position
            });
            toast.present(toast);
            this.viewCtrl.dismiss();
        }
    };
    InformConsent.prototype.stylusChangeYangMenyatakan = function ($event) {
        this.informData.yg_menyatakan = $event;
    };
    InformConsent.prototype.stylusChangeDokter = function ($event) {
        this.informData.dokter_operator = $event;
    };
    InformConsent.prototype.stylusChangeSaksi1 = function ($event) {
        this.informData.saksi1 = $event;
    };
    InformConsent.prototype.stylusChangeSaksi2 = function ($event) {
        this.informData.saksi2 = $event;
    };
    InformConsent.prototype.stylusChangePenandaan = function ($event) {
        this.informData.penandaan = $event;
    };
    InformConsent.prototype.stylusChangeInfoDokter = function ($event) {
        this.informData.tt_dokter = $event;
    };
    InformConsent.prototype.stylusChangeInfoPasien = function ($event) {
        this.informData.tt_pasien = $event;
    };
    InformConsent.prototype.stylusChangeInfoSaksi1 = function ($event) {
        this.informData.tt_saksi1 = $event;
    };
    InformConsent.prototype.stylusChangeInfoSaksi2 = function ($event) {
        this.informData.tt_saksi2 = $event;
    };
    InformConsent.prototype.submitPenandaan = function (formData) {
        var _this = this;
        formData.no_inform = this.informData.no_inform_pasien;
        formData.penandaan_operasi = this.informData.penandaan;
        if (this.formInformConsentTagging.valid) {
            this.dokterService.updateTaggingOperation(formData).subscribe(function (_a) {
                var data = _a.data;
                _this.toastMessage('Penandaan Operasi Berhasil', 3000, 'top', 1);
            });
        }
        else {
            this.toastMessage('Penandaan Operasi Gagal', 3000, 'top', 1);
        }
    };
    InformConsent.prototype.submitInformasi = function (formData) {
        var _this = this;
        formData.tt_dokter = this.informData.tt_dokter;
        formData.tt_pasien = this.informData.tt_pasien;
        formData.tt_saksi1 = this.informData.tt_saksi1;
        formData.tt_saksi2 = this.informData.tt_saksi2;
        if (this.formInformConsentInfo.valid) {
            if (this.informData.no_inform_pasien == null) {
                this.toastMessage('informasi Inform Consent Berhasil disimpan', 3000, 'top', 0);
                this.dokterService.saveInformConsentInfo(formData).subscribe(function (_a) {
                    var data = _a.data;
                    _this.submitInfo = false;
                });
            }
            else {
                this.toastMessage('Anda sudah mengisi informasi Inform Consent', 3000, 'top', 0);
            }
        }
        else {
            this.toastMessage('informasi Inform Consent Gagal disimpan. Mohon lengkapi Formulir Pemberian Info', 3000, 'top', 0);
        }
    };
    InformConsent.prototype.submitAnestesi = function (formData) {
        var _this = this;
        formData.tt_dokter = this.informData.tt_dokter;
        formData.tt_pasien = this.informData.tt_pasien;
        formData.tt_saksi1 = this.informData.tt_saksi1;
        formData.tt_saksi2 = this.informData.tt_saksi2;
        if (this.formInformConsentInfo.valid) {
            if (this.informData.no_inform_pasien == null) {
                this.toastMessage('informasi Inform Consent Berhasil disimpan', 3000, 'top', 0);
                this.statusTag = true;
                this.dokterService.saveInformConsentAnestesi(formData).subscribe(function (_a) {
                    var data = _a.data;
                    _this.submitInfo = false;
                });
            }
            else {
                this.toastMessage('Anda sudah mengisi informasi Inform Consent', 3000, 'top', 0);
            }
        }
        else {
            this.toastMessage('informasi Inform Consent Gagal disimpan. Mohon lengkapi Formulir Pemberian Info', 3000, 'top', 0);
        }
    };
    InformConsent.prototype.submitApproval = function (formData) {
        var _this = this;
        formData.nama_ortu = this.informData.nama_ortu;
        formData.umur_ortu = this.informData.umur_ortu;
        formData.alamat = this.informData.alamat;
        formData.ktp = this.informData.ktp;
        formData.no_inform = this.informData.no_inform_pasien;
        formData.yg_menyatakan = this.informData.yg_menyatakan;
        formData.dokter_operator = this.informData.dokter_operator;
        formData.saksi1 = this.informData.saksi1;
        formData.saksi2 = this.informData.saksi2;
        formData.tipe_tindakan = this.selection;
        var date = new Date(formData.tgl_inform);
        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.dt = date.getDate();
        this.day = date.getDay();
        var hour = new Date(formData.jam_inform);
        this.hour = hour.getHours();
        if (this.month < 10) {
            this.month = '0' + this.month;
        }
        if (this.dt < 10) {
            this.dt = '0' + this.dt;
        }
        formData.tgl_inform = this.year + "-" + this.month + "-" + this.dt;
        formData.jam_inform = this.hour;
        if (this.formInformConsentApproval.valid) {
            this.dokterService.updateInformConsentApproval(formData).subscribe(function (_a) {
                var data = _a.data;
                if (data) {
                    _this.toastMessage('Pasien Menyetujui Tindakan Operasi, Silahkan Klik Tab Penandaan Operasi', 3000, 'top', 0);
                    _this.submitPersetujuan = false;
                }
                else {
                    _this.toastMessage('Formulir Persetujuan gagal disimpan', 3000, 'top', 0);
                }
            });
        }
        else {
            this.toastMessage('Mohon Lengkapi Formulir Persetujuan ini', 3000, 'top', 0);
            this.submitPersetujuan = true;
        }
    };
    InformConsent.prototype.submitNotApproval = function (formData) {
        var _this = this;
        formData.no_inform = this.informData.no_inform_pasien;
        formData.yg_menyatakan = this.informData.yg_menyatakan;
        formData.saksi1 = this.informData.saksi1;
        formData.saksi2 = this.informData.saksi2;
        var date = new Date(formData.tgl_inform);
        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.dt = date.getDate();
        this.day = date.getDay();
        var hour = new Date(formData.jam_inform);
        this.hour = hour.getHours();
        if (this.month < 10) {
            this.month = '0' + this.month;
        }
        if (this.dt < 10) {
            this.dt = '0' + this.dt;
        }
        formData.tgl_inform = this.year + "-" + this.month + "-" + this.dt;
        formData.jam_inform = this.hour;
        if (this.formInformConsentNotApproval.valid) {
            this.dokterService.updateInformConsentApproval(formData).subscribe(function (_a) {
                var data = _a.data;
                if (data) {
                    _this.toastMessage('Pasien Menolak Tindakan Operasi', 3000, 'top', 0);
                    _this.submitPersetujuan = false;
                }
                else {
                    _this.toastMessage('Formulir Penolakan gagal disimpan', 3000, 'top', 0);
                }
            });
        }
        else {
            this.toastMessage('Mohon Lengkapi Formulir Penolakan ini', 3000, 'top', 0);
            this.submitPersetujuan = true;
        }
    };
    InformConsent.prototype.checkAllTanda = function () {
        this.selectedAll = !this.selectedAll;
    };
    InformConsent.prototype.checkAllTandaChecklist = function () {
        this.selectedAllChecklist = !this.selectedAllChecklist;
    };
    InformConsent.prototype.checkAllTandaAnestesi = function () {
        this.selectedAllAnestesi = !this.selectedAllAnestesi;
    };
    InformConsent.prototype.openTTD = function (val) {
        switch (val) {
            case "ttdMenyatakan":
                return this.ttdMenyatakan = true;
            case "ttdDokter":
                return this.ttdDokter = true;
            case "ttdSaksi1":
                return this.ttdSaksi1 = true;
            case "ttdSaksi2":
                return this.ttdSaksi2 = true;
            case "ttdMenyatakanNolak":
                return this.ttdMenyatakanNolak = true;
            case "ttdSaksi1Nolak":
                return this.ttdSaksi1Nolak = true;
            case "ttdSaksi2Nolak":
                return this.ttdSaksi2Nolak = true;
            case "ttdInfoDokter":
                return this.ttdInfoDokter = true;
            case "ttdInfoPasien":
                return this.ttdInfoPasien = true;
            case "ttdInfoSaksi1":
                return this.ttdInfoSaksi1 = true;
            case "ttdInfoSaksi2":
                return this.ttdInfoSaksi2 = true;
        }
    };
    InformConsent = __decorate([
        IonicPage(),
        Component({
            selector: 'inform-modal',
            templateUrl: 'inform.component.html'
        }),
        __metadata("design:paramtypes", [ViewController,
            ToastController,
            FormBuilder,
            DokterService,
            NavParams,
            AlertController])
    ], InformConsent);
    return InformConsent;
}());
export { InformConsent };
//# sourceMappingURL=inform.component.js.map