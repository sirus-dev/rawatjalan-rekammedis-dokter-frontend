import { Inform } from './../../models/inform.model';
import { AlertController, IonicPage } from 'ionic-angular';
import { DokterService } from '../../services/providers/dokter-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ViewController, ToastController, NavParams } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
    selector: 'inform-modal',
    templateUrl: 'inform.component.html'
})

export class InformConsent implements OnInit { 

    ttdMenyatakan:boolean      = false;
    ttdDokter:boolean          = false;
    ttdSaksi1:boolean          = false;
    ttdSaksi2:boolean          = false;
    ttdMenyatakanNolak:boolean = false;
    ttdSaksi1Nolak:boolean     = false;
    ttdSaksi2Nolak:boolean     = false;
    ttdInfoDokter:boolean      = false;
    ttdInfoPasien:boolean      = false;
    ttdInfoSaksi1:boolean      = false;
    ttdInfoSaksi2:boolean      = false;

    statusTag: boolean = false;

    private formInformConsentInfo : FormGroup;
    private formInformConsentInfoChecklist : FormGroup;
    private formInformConsentApproval : FormGroup;
    private formInformConsentNotApproval : FormGroup;
    private formInformConsentTagging : FormGroup;
    private formInformConsentAnestesi : FormGroup;

    anestesi:Array<any>;
    datapersonal:Array<any> = [];
    dataInform:Array<any>;
    myDate: String = new Date().toISOString();
    inform: string = "pemberian";
    information_inform: string;
    selection: string = "PERSETUJUAN";
    patientName: string;
    doctorName: string;
    selection_inform: string;

    selectedAll               = false;
    selectedAllChecklist      = false;
    selectedAllAnestesi       = false;
    stylusmode:boolean        = true;
    submitPersetujuan:boolean = true;
    submitInfo:boolean        = true;

    year:any;
    month:any;
    dt:any;
    day:any;
    hour:any;
    
    informData : Inform;

    form_isi_informasi: boolean = false;
    form_checklist_informasi: boolean = false;
    form_anestesi:boolean = false;

 constructor(public viewCtrl: ViewController, 
             public toastCtrl: ToastController,  
             private formBuilder: FormBuilder, 
             private dokterService: DokterService, 
             private navParams: NavParams, 
             private alertCtrl: AlertController) {

  this.informData = new Inform();

  if(this.navParams.get('specialist') == "Spesialis Bedah Umum" || this.navParams.get('specialist') == "Bedah Orthopedi" || this.navParams.get('specialist') == "Spesialis Mata" || this.navParams.get('specialist') == "Spesialis THT") {
      this.form_isi_informasi = false;
      this.form_checklist_informasi = false;
      this.form_anestesi = true;
      this.information_inform = "isi_informasi"
    } else if(this.navParams.get('specialist') == "Spesialis Kebidanan dan K") {
      this.form_isi_informasi = true;
      this.form_checklist_informasi = false;
      this.form_anestesi = true;
      this.information_inform = "checklist_informasi"
    } else if(this.navParams.get('specialist') == "Spesialis Anasthesi") {
      this.form_isi_informasi = true;
      this.form_checklist_informasi = true;
      this.form_anestesi = false;
      this.information_inform = "anestesi"
    }

  if(this.navParams.get('personalNoRM')){
        this.datapersonal = this.navParams.get('dataPatient');
        if(this.datapersonal[0].kelamin == "L"){
          this.informData.penandaan_operasi = "pria";
        } else {
          this.informData.penandaan_operasi = "wanita";
        }
        this.dokterService.getInformConsentQuery(this.navParams.get('personalNoRM')).subscribe(({data}) => {
          var strinform = JSON.stringify(data);
          var parsinform = JSON.parse(strinform);
          let datainform = parsinform.informConsent0;
          if(datainform.length == 0){
            this.dataInform = [{
              no_inform: '',
              no_medrec: '',
              nama_ortu: '',
              umur_ortu: '',
              kelamin_ortu: '',
              alamat:  '',
              ktp: '',
              tgl_inform: '',
              jam_inform: '',
              yg_menyatakan:  '',
              file_yg_menyatakan:  '',
              dokter_operator: '',
              file_dokter_operator:  '',
              saksi1:  '',
              file_saksi1:  '',
              saksi2:  '',
              file_saksi2:  '',
              tindakan_operasi:  '',
              tipe_tindakan: '',
              pelaksana_tindakan:  '',
              pemberi_info:  '',
              penerima_info:  '',
              info_diag_kerja:  '',
              checklist_info_diag_kerja: '',
              dasar_diag:  '',
              checklist_dasar_diag: '',
              tindakan_dokter:  '',
              checklist_tindakan_dokter: '',
              indikasi_tindakan:  '',
              checklist_indikasi_tindakan: '',
              tatacara:  '',
              checklist_tatacara: '',
              tujuan:  '',
              checklist_tujuan: '',
              risiko:  '',
              checklist_risiko: '',
              komplikasi:  '',
              checklist_komplikasi: '',
              prognosis:  '',
              checklist_prognosis: '',
              alternatif:  '',
              checklist_alternatif: '',
              lain:  '',
              checklist_lain: '',
              penandaan_operasi:  '',
              a_u_tindakan_dokter:  '',
              a_u_tatacara: '',
              a_u_tujuan:  '',
              a_u_risiko: '',
              a_u_komplikasi:  '',
              a_r_tindakan_dokter: '',
              a_r_tatacara:  '',
              a_r_tujuan: '',
              a_r_risiko:  '',
              a_r_komplikasi: '',
              a_r_alternatif:  '',
              a_l_tindakan_dokter: '',
              a_l_tatacara:  '',
              a_l_tujuan: '',
              a_l_risiko:  '',
              a_l_komplikasi: '',
              a_l_alternatif:  '',
              sedasi_tindakan_dokter: '',
              sedasi_tatacara:  '',
              sedasi_tujuan: '',
              sedasi_risiko:  '',
              sedasi_komplikasi: '',
              sedasi_prognosis: '',
              sedasi_lain: '',
              flag: ''
            }];
          }else{
            this.dataInform = [datainform[0]];
            this.informData.no_inform_pasien = datainform[0].no_inform;
            this.informData.checklist_info_diag_kerja = datainform[0].checklist_info_diag_kerja;
            this.informData.checklist_dasar_diag = datainform[0].checklist_dasar_diag;
            this.informData.checklist_tindakan_dokter = datainform[0].checklist_tindakan_dokter;
            this.informData.checklist_indikasi_tindakan = datainform[0].checklist_indikasi_tindakan;
            this.informData.checklist_tatacara = datainform[0].checklist_tatacara;
            this.informData.checklist_tujuan = datainform[0].checklist_tujuan;
            this.informData.checklist_risiko = datainform[0].checklist_risiko;
            this.informData.checklist_komplikasi = datainform[0].checklist_komplikasi;
            this.informData.checklist_prognosis = datainform[0].checklist_prognosis;
            this.informData.checklist_alternatif = datainform[0].checklist_alternatif;
            this.informData.checklist_lain = datainform[0].checklist_lain;
            
            this.informData.a_u_tindakan_dokter = datainform[0].a_u_tindakan_dokter;
            this.informData.a_u_tatacara = datainform[0].a_u_tatacara;
            this.informData.a_u_tujuan = datainform[0].a_u_tujuan;
            this.informData.a_u_risiko = datainform[0].a_u_risiko;
            this.informData.a_u_komplikasi = datainform[0].a_u_komplikasi;
            this.informData.a_r_tindakan_dokter = datainform[0].a_r_tindakan_dokter;
            this.informData.a_r_tatacara = datainform[0].a_r_tatacara;
            this.informData.a_r_tujuan = datainform[0].a_r_tujuan;
            this.informData.a_r_risiko = datainform[0].a_r_risiko;
            this.informData.a_r_komplikasi = datainform[0].a_r_komplikasi;
            this.informData.a_r_alternatif = datainform[0].a_r_alternatif;
            this.informData.a_l_tindakan_dokter = datainform[0].a_l_tindakan_dokter;
            this.informData.a_l_tatacara = datainform[0].a_l_tatacara;
            this.informData.a_l_tujuan = datainform[0].a_l_tujuan;
            this.informData.a_l_risiko = datainform[0].a_l_risiko;
            this.informData.a_l_komplikasi = datainform[0].a_l_komplikasi;
            this.informData.a_l_alternatif = datainform[0].a_l_alternatif;
            this.informData.sedasi_tindakan_dokter = datainform[0].sedasi_tindakan_dokter;
            this.informData.sedasi_tatacara = datainform[0].sedasi_tatacara;
            this.informData.sedasi_tujuan = datainform[0].sedasi_tujuan;
            this.informData.sedasi_risiko = datainform[0].sedasi_risiko;
            this.informData.sedasi_komplikasi = datainform[0].sedasi_komplikasi;
            this.informData.sedasi_prognosis = datainform[0].sedasi_prognosis;
            this.informData.anestesi_lain = datainform[0].sedasi_lain;
            this.informData.tindakan_operasi = datainform[0].tindakan_operasi;
            if(this.informData.no_inform_pasien){
              this.submitInfo = false;
            }
          }
        });
  }

 }
 

 ngOnInit(){
      this.patientName = this.datapersonal[0].nama_pasien;
      this.doctorName = this.navParams.get('doctorName');
      this.myDate = moment().format();
      var salt = Math.floor(Math.random() * 10000);
      var no_inform = salt+this.navParams.get('personalNoRM');

      this.dokterService.getPatientById(this.navParams.get('personalNoRM')).subscribe(({data})=>{
        let date = new Date();
        let year_now = date.getFullYear();
        let year_now_str = year_now.toString();
        let dataguarantor = data.patientByNorm;
        this.informData.nama_ortu = dataguarantor.nama_penjamin;
        this.informData.umur_ortu = Number(year_now_str) - Number(dataguarantor.tahun2);
        this.informData.alamat = dataguarantor.jln2+" No. "+dataguarantor.no2+", RT/RW. "+dataguarantor.rt2+"/"+dataguarantor.rw2+", Desa. "+dataguarantor.desa2+", Kec. "+dataguarantor.kecamatan2+", "+dataguarantor.kota2;
        this.informData.ktp = dataguarantor.no_ktp2;
      });

      this.formInformConsentInfo = this.formBuilder.group({
        'no_inform': [no_inform, Validators.required],
        'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
        'pelaksana_tindakan': [this.doctorName, Validators.required],
        'pemberi_info': [this.doctorName, Validators.required],
        'penerima_info': [this.patientName, Validators.required],
        'info_diag_kerja': [''],
        'checklist_info_diag_kerja': [],
        'dasar_diag': [''],
        'checklist_dasar_diag': [],
        'tindakan_dokter': [''],
        'checklist_tindakan_dokter': [],
        'indikasi_tindakan': [''],
        'checklist_indikasi_tindakan': [],
        'tatacara': [''],
        'checklist_tatacara': [],
        'tujuan':[''],
        'checklist_tujuan':[],
        'risiko':[''],
        'checklist_risiko':[],
        'komplikasi':[''],
        'checklist_komplikasi':[],
        'prognosis':[''],
        'checklist_prognosis':[],
        'alternatif':[''],
        'checklist_alternatif':[],
        'checklist_lain':[],
        'tt_dokter': [],
        'tt_pasien': [],
        'tt_saksi1': [],
        'tt_saksi2': []
    });

    this.formInformConsentInfoChecklist = this.formBuilder.group({
      'no_inform': [no_inform, Validators.required],
      'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
      'pelaksana_tindakan': [this.doctorName, Validators.required],
      'pemberi_info': [this.doctorName, Validators.required],
      'penerima_info': [this.patientName, Validators.required],
      'info_diag_kerja': [''],
      'checklist_info_diag_kerja': [],
      'dasar_diag': [''],
      'checklist_dasar_diag': [],
      'tindakan_dokter': [''],
      'checklist_tindakan_dokter': [],
      'indikasi_tindakan': [''],
      'checklist_indikasi_tindakan': [],
      'tatacara': [''],
      'checklist_tatacara': [],
      'tujuan':[''],
      'checklist_tujuan':[],
      'risiko':[''],
      'checklist_risiko':[],
      'komplikasi':[''],
      'checklist_komplikasi':[],
      'prognosis':[''],
      'checklist_prognosis':[],
      'alternatif':[''],
      'checklist_alternatif':[],
      'checklist_lain':[],
      'tt_dokter': [],
      'tt_pasien': [],
      'tt_saksi1': [],
      'tt_saksi2': []
  });

  this.formInformConsentAnestesi = this.formBuilder.group({
    'no_inform': [no_inform, Validators.required],
    'no_medrec': [this.navParams.get('personalNoRM'), Validators.required],
    'pelaksana_tindakan': [this.doctorName, Validators.required],
    'pemberi_info': [this.doctorName, Validators.required],
    'penerima_info': [this.patientName, Validators.required],
    'a_u_tindakan_dokter': [],
    'a_u_tatacara':[],
    'a_u_tujuan':[],
    'a_u_risiko':[],
    'a_u_komplikasi':[],
    'a_r_tindakan_dokter':[],
    'a_r_tatacara':[],
    'a_r_tujuan':[],
    'a_r_risiko':[],
    'a_r_komplikasi':[],
    'a_r_alternatif':[],
    'a_l_tindakan_dokter':[],
    'a_l_tatacara':[],
    'a_l_tujuan':[],
    'a_l_risiko':[],
    'a_l_komplikasi':[],
    'a_l_alternatif':[],
    'sedasi_tindakan_dokter':[],
    'sedasi_tatacara':[],
    'sedasi_tujuan':[],
    'sedasi_risiko':[],
    'sedasi_komplikasi':[],
    'sedasi_prognosis':[],
    'sedasi_lain':[],
    'tt_dokter': [],
    'tt_pasien': [],
    'tt_saksi1': [],
    'tt_saksi2': []
});


    this.formInformConsentApproval = this.formBuilder.group({
      'no_inform': [''],
      'nama_ortu': [this.informData.nama_ortu],
      'umur_ortu': [this.informData.umur_ortu],
      'kelamin_ortu': [''],
      'alamat': [this.informData.alamat],
      'ktp': [this.informData.ktp],
      'tgl_inform': [''],
      'jam_inform': [''],
      'yg_menyatakan': [''],
      'dokter_operator': [],
      'saksi1': [''],
      'saksi2': [''],
      'tindakan_operasi': ['', Validators.required],
      'tipe_tindakan': ['']
    });

    this.formInformConsentNotApproval = this.formBuilder.group({
      'no_inform': [''],
      'nama_ortu': [this.informData.nama_ortu],
      'umur_ortu': [this.informData.umur_ortu],
      'kelamin_ortu': [''],
      'alamat': [this.informData.alamat],
      'ktp': [this.informData.ktp],
      'tgl_inform': [''],
      'jam_inform': [''],
      'yg_menyatakan': [''],
      'dokter_operator': [],
      'saksi1': [''],
      'saksi2': [''],
      'tindakan_operasi': ['', Validators.required],
      'tipe_tindakan': ['']
    });

    this.formInformConsentTagging = this.formBuilder.group({
      'no_inform': [''],
      'penandaan_operasi': []
    });
 }

 getInfromType(tipe_inform: any) {
  this.dokterService.getTypeInform(tipe_inform).subscribe(({data})=>{
    this.anestesi = [data.informTypeByName];
  });
 }

 dismiss() {
   let datainfrom = this.formInformConsentInfo.value;
  if(datainfrom.pelaksana_tindakan == "" && datainfrom.penerima_info == "" && datainfrom.pemberi_info == ""){
    this.viewCtrl.dismiss();
  }else{
    let alert = this.alertCtrl.create({
      title: 'Data Inform Consent',
      subTitle: 'Anda yakin untuk menutup formulir Inform Consent? Informasi yang anda masukkan akan hilang',
      buttons: [{
          text: 'YA',
          role: 'YA',
          handler: () => {
            this.viewCtrl.dismiss();
          }
      },{
        text: 'TIDAK',
        role: 'TIDAK',
      }]
    });
    alert.present();
  }

 }

 toastMessage(message:string,duration:number,position:string,flag:number){
    if(flag == 0){
      let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        position: position
      });
      toast.present(toast);
    }else{
      let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        position: position
      });
      toast.present(toast);
      this.viewCtrl.dismiss();
    }
  }

  stylusChangeYangMenyatakan($event){
    this.informData.yg_menyatakan = $event;
  }

  stylusChangeDokter($event){
    this.informData.dokter_operator = $event;
  }

  stylusChangeSaksi1($event){
    this.informData.saksi1 = $event;
  }

  stylusChangeSaksi2($event){
    this.informData.saksi2 = $event;
  }

  stylusChangePenandaan($event){
    this.informData.penandaan = $event;
  }

  stylusChangeInfoDokter($event){
    this.informData.tt_dokter = $event;
  }

  stylusChangeInfoPasien($event){
    this.informData.tt_pasien = $event;
  }

  stylusChangeInfoSaksi1($event){
    this.informData.tt_saksi1 = $event;
  }

  stylusChangeInfoSaksi2($event){
    this.informData.tt_saksi2 = $event;
  }

 submitPenandaan(formData){
    formData.no_inform = this.informData.no_inform_pasien;
    formData.penandaan_operasi = this.informData.penandaan;
   if(this.formInformConsentTagging.valid){
      this.dokterService.updateTaggingOperation(formData).subscribe(({data})=>{
        this.toastMessage('Penandaan Operasi Berhasil',3000,'top',1);
      });
   }else{
      this.toastMessage('Penandaan Operasi Gagal',3000,'top',1);
   }
 }

  submitInformasi(formData){
    formData.tt_dokter = this.informData.tt_dokter;
    formData.tt_pasien = this.informData.tt_pasien;
    formData.tt_saksi1 = this.informData.tt_saksi1;
    formData.tt_saksi2 = this.informData.tt_saksi2;
   if(this.formInformConsentInfo.valid){
    if(this.informData.no_inform_pasien == null){
      this.toastMessage('informasi Inform Consent Berhasil disimpan',3000,'top',0);
      this.dokterService.saveInformConsentInfo(formData).subscribe(({data})=>{
        this.submitInfo = false;
      });
    }else{
      this.toastMessage('Anda sudah mengisi informasi Inform Consent',3000,'top',0);
    }
   }else{
    this.toastMessage('informasi Inform Consent Gagal disimpan. Mohon lengkapi Formulir Pemberian Info',3000,'top',0);
   }

 }

 submitAnestesi(formData){
  formData.tt_dokter = this.informData.tt_dokter;
  formData.tt_pasien = this.informData.tt_pasien;
  formData.tt_saksi1 = this.informData.tt_saksi1;
  formData.tt_saksi2 = this.informData.tt_saksi2;
 if(this.formInformConsentInfo.valid){
  if(this.informData.no_inform_pasien == null){
    this.toastMessage('informasi Inform Consent Berhasil disimpan',3000,'top',0);
    this.statusTag = true;
    this.dokterService.saveInformConsentAnestesi(formData).subscribe(({data}) => {
      this.submitInfo = false;
    });
  }else{
    this.toastMessage('Anda sudah mengisi informasi Inform Consent',3000,'top',0);
  }
 }else{
  this.toastMessage('informasi Inform Consent Gagal disimpan. Mohon lengkapi Formulir Pemberian Info',3000,'top',0);
 }

}
 

 submitApproval(formData){
   formData.nama_ortu = this.informData.nama_ortu;
   formData.umur_ortu = this.informData.umur_ortu;
   formData.alamat = this.informData.alamat;
   formData.ktp = this.informData.ktp;
   formData.no_inform = this.informData.no_inform_pasien;
   formData.yg_menyatakan = this.informData.yg_menyatakan;
   formData.dokter_operator = this.informData.dokter_operator;
   formData.saksi1 = this.informData.saksi1;
   formData.saksi2 = this.informData.saksi2;
   formData.tipe_tindakan = this.selection;
   let date = new Date(formData.tgl_inform);
   this.year = date.getFullYear();
   this.month = date.getMonth()+1;
   this.dt = date.getDate();
   this.day = date.getDay();

   let hour = new Date(formData.jam_inform);
   this.hour = hour.getHours();

   if (this.month < 10) {
    this.month = '0' + this.month;
   }
   if (this.dt < 10) {
    this.dt = '0' + this.dt;
   }
   formData.tgl_inform = this.year + "-" + this.month + "-" + this.dt;
   formData.jam_inform = this.hour;
  if(this.formInformConsentApproval.valid){
        this.dokterService.updateInformConsentApproval(formData).subscribe(({data}) => {
            if(data){
              this.toastMessage('Pasien Menyetujui Tindakan Operasi, Silahkan Klik Tab Penandaan Operasi',3000,'top',0);
              this.submitPersetujuan = false;
            }else{
              this.toastMessage('Formulir Persetujuan gagal disimpan',3000,'top',0);
            }
        });
   }else{
      this.toastMessage('Mohon Lengkapi Formulir Persetujuan ini',3000,'top',0);
      this.submitPersetujuan = true;
   }

 }

 submitNotApproval(formData){formData.no_inform = this.informData.no_inform_pasien;
  formData.yg_menyatakan = this.informData.yg_menyatakan;
  formData.saksi1 = this.informData.saksi1;
  formData.saksi2 = this.informData.saksi2;
  let date = new Date(formData.tgl_inform);
  this.year = date.getFullYear();
  this.month = date.getMonth()+1;
  this.dt = date.getDate();
  this.day = date.getDay();

  let hour = new Date(formData.jam_inform);
  this.hour = hour.getHours();

  if (this.month < 10) {
   this.month = '0' + this.month;
  }
  if (this.dt < 10) {
   this.dt = '0' + this.dt;
  }
  formData.tgl_inform = this.year + "-" + this.month + "-" + this.dt;
  formData.jam_inform = this.hour;

  if(this.formInformConsentNotApproval.valid){
      this.dokterService.updateInformConsentApproval(formData).subscribe(({data}) => {
        if(data){
          this.toastMessage('Pasien Menolak Tindakan Operasi',3000,'top',0);
          this.submitPersetujuan = false;
        }else{
          this.toastMessage('Formulir Penolakan gagal disimpan',3000,'top',0);
        }
    });
   }else{
      this.toastMessage('Mohon Lengkapi Formulir Penolakan ini',3000,'top',0);
      this.submitPersetujuan = true;
   }
 }

 checkAllTanda(){
    this.selectedAll = !this.selectedAll;
 }

 checkAllTandaChecklist(){
    this.selectedAllChecklist = !this.selectedAllChecklist;
 }

 checkAllTandaAnestesi(){
    this.selectedAllAnestesi = !this.selectedAllAnestesi;
 }

 openTTD(val:string){
    switch(val){
      case "ttdMenyatakan":
      return this.ttdMenyatakan = true;
      case "ttdDokter":
      return this.ttdDokter = true;
      case "ttdSaksi1":
      return this.ttdSaksi1 = true;
      case "ttdSaksi2":
      return this.ttdSaksi2 = true;
      case "ttdMenyatakanNolak":
      return this.ttdMenyatakanNolak = true;
      case "ttdSaksi1Nolak":
      return this.ttdSaksi1Nolak = true;
      case "ttdSaksi2Nolak":
      return this.ttdSaksi2Nolak = true;
      case "ttdInfoDokter":
      return this.ttdInfoDokter = true;
      case "ttdInfoPasien":
      return this.ttdInfoPasien = true;
      case "ttdInfoSaksi1":
      return this.ttdInfoSaksi1 = true;
      case "ttdInfoSaksi2":
      return this.ttdInfoSaksi2 = true;
    }
 }

}