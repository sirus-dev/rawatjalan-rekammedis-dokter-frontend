import { HomeModule } from './../home/home.component.module';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { InformConsent } from './inform.component';

@NgModule({
  declarations: [
    InformConsent
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(InformConsent),
    HomeModule
  ]
})
export class InformConsentModule {}
