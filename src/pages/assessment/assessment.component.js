var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { InkGrabber } from '@sirus/stylus';
import { Validators, FormBuilder } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';
var Assessment = /** @class */ (function () {
    function Assessment(viewCtrl, toastCtrl, dokterService, params, formBuilder) {
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.params = params;
        this.formBuilder = formBuilder;
        this.datapersonal = [];
        this.dataReview = [];
        this.myDate = new Date().toISOString();
        this.resiko_jatuh = false;
        this.s_nyeri = false;
        this.s_resiko_jatuh = false;
        this.s_gizi = false;
        this.s_fungsional = false;
        this.as_dokter = false;
        this.tt_perawat = [false];
        this.components = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.width = 1000;
        this.height = 400;
        this.datapersonal = this.params.get('dataPatient');
        this.doctorname = this.params.get('doctorName');
        this.nodok = this.params.get('nodok');
        this.dataReview = this.params.get('dataReview');
        this.as_dokter = true;
        this.assessmentDoctorForm = this.formBuilder.group({
            'no_kaji': [this.dataReview[0].no_kaji, Validators.required],
            'nama_dok': [this.doctorname],
            'nodok': [this.nodok],
            'anamnesa_keluhan': [''],
            'anamnesa_riwayat_penyakit': [''],
            'fisik_kepala': [''],
            'fisik_leher': [''],
            'fisik_mulut': [''],
            'fisik_paru': [''],
            'fisik_genitalia': [''],
            'fisik_mata': [''],
            'fisik_tht': [''],
            'fisik_jantung': [''],
            'fisik_abdomen': [''],
            'fisik_ekstremitas': [''],
            'status_gizi': [''],
            'pemeriksaan_gigi': [''],
            'pemeriksaan_gigi_ket': [''],
            'status_lokalis': [''],
            'pemeriksaan_penunjang': [''],
            'diagnosis': [''],
            'rencana': [''],
            'dirujuk': [''],
            'dirujuk_lainnya': [''],
            'tt_dokter': ['']
        });
        if (this.dataReview) {
            if (this.dataReview[0].nyeri !== "0") {
                this.nyeri = "ada";
                this.nyeri_score = this.dataReview[0].nyeri;
            }
            else {
                this.nyeri = "0";
                this.nyeri_score = this.dataReview[0].nyeri;
            }
            this.resiko_jatuh_a = this.dataReview[0].resiko_jatuh_a;
            this.resiko_jatuh_b = this.dataReview[0].resiko_jatuh_b;
            this.gizi1 = this.dataReview[0].gizi1;
            this.gizi2 = this.dataReview[0].gizi2;
            this.gizi3 = this.dataReview[0].gizi3;
            this.resikoA1 = this.dataReview[0].resikoA1;
            this.resikoA2 = this.dataReview[0].resikoA2;
            this.resikoB1 = this.dataReview[0].resikoB1;
            this.resikoTindakan1action = this.dataReview[0].resikoTindakan1action;
            this.resikoTindakan2action = this.dataReview[0].resikoTindakan2action;
            this.resikoTindakan3action = this.dataReview[0].resikoTindakan3action;
            this.status_fungsional = this.dataReview[0].status_fungsional;
            this.fisik_kepala = this.dataReview[0].fisik_kepala;
            this.fisik_leher = this.dataReview[0].fisik_leher;
            this.fisik_mulut = this.dataReview[0].fisik_mulut;
            this.fisik_paru = this.dataReview[0].fisik_paru;
            this.fisik_genitalia = this.dataReview[0].fisik_genitalia;
            this.fisik_mata = this.dataReview[0].fisik_mata;
            this.fisik_tht = this.dataReview[0].fisik_tht;
            this.fisik_jantung = this.dataReview[0].fisik_jantung;
            this.fisik_abdomen = this.dataReview[0].fisik_abdomen;
            this.fisik_ekstremitas = this.dataReview[0].fisik_ekstremitas;
            this.status_gizi = this.dataReview[0].status_gizi;
            this.pemeriksaan_gigi = this.dataReview[0].pemeriksaan_gigi;
            this.pemeriksaan_gigi_ket = this.dataReview[0].pemeriksaan_gigi_ket;
            this.dirujuk = this.dataReview[0].dirujuk;
            this.dirujuk_lainnya = this.dataReview[0].dirujuk_lainnya;
        }
    }
    Assessment.prototype.toastMessage = function (message, duration, position) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });
        toast.present(toast);
        this.viewCtrl.dismiss();
    };
    Assessment.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    Assessment.prototype.stylusChange = function (event) {
        this.tt_dokter = event;
    };
    Assessment.prototype.submitAssessment = function (formData) {
        var _this = this;
        formData.tt_dokter = this.tt_dokter;
        this.dokterService.updateDoctorReview(formData).subscribe(function (_a) {
            var data = _a.data;
            if (data) {
                _this.toastMessage('Permintaan Lab berhasil terkirim', 4000, 'top');
            }
        });
    };
    Assessment.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    Assessment.prototype.showForm = function (sub) {
        switch (sub) {
            case "resiko_jatuh":
                return this.resiko_jatuh = !this.resiko_jatuh;
            case "s_nyeri":
                return this.s_nyeri = !this.s_nyeri;
            case "s_resiko_jatuh":
                return this.s_resiko_jatuh = !this.s_resiko_jatuh;
            case "s_gizi":
                return this.s_gizi = !this.s_gizi;
            case "s_fungsional":
                return this.s_fungsional = !this.s_fungsional;
            case "as_dokter":
                return this.as_dokter = !this.as_dokter;
        }
    };
    Assessment.prototype.drawcanvas = function (index, strokedata, status) {
        this.components = [];
        if (status == "tt_perawat") {
            this.tt_perawat[index] = !this.tt_perawat[index];
        }
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById(index);
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            this.inkGrabber = new InkGrabber(ctx);
            this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    var stroke_1 = this.inkGrabber.getStroke();
                    this.components.push(stroke_1);
                    this.inkGrabber = new InkGrabber(ctx);
                    this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            var stroke = this.inkGrabber.getStroke();
            this.components.push(stroke);
            this.inkGrabberFinal = new InkGrabber(ctx);
            for (var i = 0; i < this.components.length; i++) {
                this.inkGrabberFinal.drawComponent(this.components[i]);
            }
            var image = new Image();
            image.id = "pic" + index;
            var content = this.element;
            image.src = content.toDataURL();
            this.dataReview[index]["image"] = image.src;
        }
        else {
            this.dataReview[index]["image"] = "assets/img/no_image.png";
        }
    };
    Assessment = __decorate([
        IonicPage(),
        Component({
            selector: 'assessment-modal',
            templateUrl: 'assessment.component.html'
        }),
        __metadata("design:paramtypes", [ViewController, ToastController, DokterService, NavParams, FormBuilder])
    ], Assessment);
    return Assessment;
}());
export { Assessment };
//# sourceMappingURL=assessment.component.js.map