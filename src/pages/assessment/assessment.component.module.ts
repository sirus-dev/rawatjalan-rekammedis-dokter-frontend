import { HomeModule } from './../home/home.component.module';
import { Assessment } from './assessment.component';
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    Assessment
  ],
  imports: [
    IonicModule,
    IonicPageModule.forChild(Assessment),
    HomeModule
  ]
})
export class AssessmentModule {}
