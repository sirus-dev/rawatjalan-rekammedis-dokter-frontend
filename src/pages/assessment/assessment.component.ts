import { InkGrabber, AbstractComponent } from '@sirus/stylus';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { ViewController, ToastController, NavParams, IonicPage } from 'ionic-angular';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'assessment-modal',
  templateUrl: 'assessment.component.html'
})

export class Assessment {
  datapersonal: Array<any> = [];
  dataReview: Array<any> = [];
  doctorname: any;
  nodok: any;
  myDate: String = new Date().toISOString();
  personalTglLahir: string;
  resiko_jatuh: boolean = false;
  s_nyeri: boolean = false;
  s_resiko_jatuh: boolean = false;
  s_gizi: boolean = false;
  s_fungsional: boolean = false;
  as_dokter: boolean = false;

  tt_perawat: boolean[] = [false];
  tt_dokter: any;

  private inkGrabber: InkGrabber;
  private inkGrabberFinal: InkGrabber;
  private element: HTMLElement;
  private components: AbstractComponent[] = [];
  arrayX: number[] = [];
  arrayY: number[] = [];
  arrayT: number[] = [];

  width: number = 1000;
  height: number = 400;

  private assessmentDoctorForm: FormGroup;

  resikoA1: any;
  resikoA2: any;
  resikoB1: any;
  resikoTindakan1action: any;
  resikoTindakan2action: any;
  resikoTindakan3action: any;
  nyeri: any;
  nyeri_score: any;
  resiko_jatuh_a: any;
  resiko_jatuh_b: any;
  gizi1: any;
  gizi2: any;
  gizi3: any;
  gizi3_lainnya: any;
  status_fungsional: any;
  status_fungsional_ket: any;
  anamnesa_keluhan: any;
  anamnesa_riwayat_penyakit: any;
  fisik_kepala: any;
  fisik_leher: any;
  fisik_mulut: any;
  fisik_paru: any;
  fisik_genitalia: any;
  fisik_mata: any;
  fisik_tht: any;
  fisik_jantung: any;
  fisik_abdomen: any;
  fisik_ekstremitas: any;
  status_gizi: any;
  pemeriksaan_gigi: any;
  pemeriksaan_gigi_ket: any;
  status_lokalis: any;
  pemeriksaan_penunjang: any;
  diagnosis: any;
  rencana: any;
  dirujuk: any;
  dirujuk_lainnya: any;

  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController, private dokterService: DokterService, public params: NavParams, public formBuilder: FormBuilder) {

    this.datapersonal = this.params.get('dataPatient');
    this.doctorname = this.params.get('doctorName');
    this.nodok = this.params.get('nodok');
    this.dataReview = this.params.get('dataReview');
    this.as_dokter = true;

    this.assessmentDoctorForm = this.formBuilder.group({
      'no_kaji': [this.dataReview[0].no_kaji, Validators.required],
      'nama_dok': [this.doctorname],
      'nodok': [this.nodok],
      'anamnesa_keluhan': [''],
      'anamnesa_riwayat_penyakit': [''],
      'fisik_kepala': [''],
      'fisik_leher': [''],
      'fisik_mulut': [''],
      'fisik_paru': [''],
      'fisik_genitalia': [''],
      'fisik_mata': [''],
      'fisik_tht': [''],
      'fisik_jantung': [''],
      'fisik_abdomen': [''],
      'fisik_ekstremitas': [''],
      'status_gizi': [''],
      'pemeriksaan_gigi': [''],
      'pemeriksaan_gigi_ket': [''],
      'status_lokalis': [''],
      'pemeriksaan_penunjang': [''],
      'diagnosis': [''],
      'rencana': [''],
      'dirujuk': [''],
      'dirujuk_lainnya': [''],
      'tt_dokter': ['']
    });

    if (this.dataReview) {
      if (this.dataReview[0].nyeri !== "0") {
        this.nyeri = "ada";
        this.nyeri_score = this.dataReview[0].nyeri;
      } else {
        this.nyeri = "0";
        this.nyeri_score = this.dataReview[0].nyeri;
      }
      this.resiko_jatuh_a = this.dataReview[0].resiko_jatuh_a;
      this.resiko_jatuh_b = this.dataReview[0].resiko_jatuh_b;
      this.gizi1 = this.dataReview[0].gizi1;
      this.gizi2 = this.dataReview[0].gizi2;
      this.gizi3 = this.dataReview[0].gizi3;
      this.resikoA1 = this.dataReview[0].resikoA1;
      this.resikoA2 = this.dataReview[0].resikoA2;
      this.resikoB1 = this.dataReview[0].resikoB1;
      this.resikoTindakan1action = this.dataReview[0].resikoTindakan1action;
      this.resikoTindakan2action = this.dataReview[0].resikoTindakan2action;
      this.resikoTindakan3action = this.dataReview[0].resikoTindakan3action;
      this.status_fungsional = this.dataReview[0].status_fungsional;
      this.fisik_kepala = this.dataReview[0].fisik_kepala;
      this.fisik_leher = this.dataReview[0].fisik_leher;
      this.fisik_mulut = this.dataReview[0].fisik_mulut;
      this.fisik_paru = this.dataReview[0].fisik_paru;
      this.fisik_genitalia = this.dataReview[0].fisik_genitalia;
      this.fisik_mata = this.dataReview[0].fisik_mata;
      this.fisik_tht = this.dataReview[0].fisik_tht;
      this.fisik_jantung = this.dataReview[0].fisik_jantung;
      this.fisik_abdomen = this.dataReview[0].fisik_abdomen;
      this.fisik_ekstremitas = this.dataReview[0].fisik_ekstremitas;
      this.status_gizi = this.dataReview[0].status_gizi;
      this.pemeriksaan_gigi = this.dataReview[0].pemeriksaan_gigi;
      this.pemeriksaan_gigi_ket = this.dataReview[0].pemeriksaan_gigi_ket;
      this.dirujuk = this.dataReview[0].dirujuk;
      this.dirujuk_lainnya = this.dataReview[0].dirujuk_lainnya;

    }
  }

  toastMessage(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });
    toast.present(toast);
    this.viewCtrl.dismiss();
  }

  private mapToMoment(date: string): string {
    if (moment(date).isValid()) {
      return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
    }
    return "-";
  }

  stylusChange(event) {
    this.tt_dokter = event;
  }

  submitAssessment(formData) {
    formData.tt_dokter = this.tt_dokter;
    this.dokterService.updateDoctorReview(formData).subscribe(({ data }) => {
      if (data) {
        this.toastMessage('Permintaan Lab berhasil terkirim', 4000, 'top');
      }
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  showForm(sub: string) {
    switch (sub) {

      case "resiko_jatuh":
        return this.resiko_jatuh = !this.resiko_jatuh;

      case "s_nyeri":
        return this.s_nyeri = !this.s_nyeri;

      case "s_resiko_jatuh":
        return this.s_resiko_jatuh = !this.s_resiko_jatuh;

      case "s_gizi":
        return this.s_gizi = !this.s_gizi;

      case "s_fungsional":
        return this.s_fungsional = !this.s_fungsional;

      case "as_dokter":
        return this.as_dokter = !this.as_dokter;
    }
  }


  drawcanvas(index: any, strokedata: any, status: any) {
    this.components = [];
    if (status == "tt_perawat") {
      this.tt_perawat[index] = !this.tt_perawat[index];
    }
    let x: number[] = [];
    let y: number[] = [];
    let t: number[] = [];

    if (strokedata !== null) {
      let datastroke = JSON.parse(strokedata);

      x = x.concat(datastroke[0]);
      y = y.concat(datastroke[1]);
      t = t.concat(datastroke[2]);

      x = x.concat(null);
      y = y.concat(null);
      t = t.concat(null);

      this.element = document.getElementById(index);
      let imageRenderingCanvas = <HTMLCanvasElement>this.element;
      let ctx = imageRenderingCanvas.getContext('2d');
      ctx.clearRect(0, 0, this.width, this.height);
      this.inkGrabber = new InkGrabber(ctx);

      this.inkGrabber.startCapture(x[0], y[0], t[0]);
      for (let i = 1; i < x.length - 1; i++) {

        if (x[i] == null) {
          this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
          let stroke = this.inkGrabber.getStroke();
          this.components.push(stroke);
          this.inkGrabber = new InkGrabber(ctx);
          this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
        }
        else {
          this.inkGrabber.continueCapture(x[i], y[i], t[i]);
        }
      }
      this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);

      let stroke = this.inkGrabber.getStroke();
      this.components.push(stroke);
      this.inkGrabberFinal = new InkGrabber(ctx);
      for (let i = 0; i < this.components.length; i++) {
        this.inkGrabberFinal.drawComponent(this.components[i]);
      }

      let image = new Image();
      image.id = "pic" + index;
      let content = <HTMLCanvasElement>this.element;
      image.src = content.toDataURL();
      this.dataReview[index]["image"] = image.src;
    } else {
      this.dataReview[index]["image"] = "assets/img/no_image.png";
    }

  }

}
