import { Patient } from '../../models/patient.model';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, ToastController } from 'ionic-angular';
import { AlertController, ModalController, PopoverController, LoadingController } from 'ionic-angular';
import moment from 'moment';

@Component({
  selector: 'soap-section',
  templateUrl: 'soap.component.html'
})
export class SOAPPage {

  @ViewChild(Slides) slides: Slides;

  showExamine = true;
  private formPemeriksaan: FormGroup;

  @Input()
  dataPatient: Array<any>;

  @Input()
  doctorName: string;

  @Input()
  penyakit: string;

  @Input()
  specialist: string;

  icdx: Array<any> = [];
  icd9: Array<any> = [];
  medicine: Array<any> = [];

  listicd: Array<any> = [];
  listicd9: Array<any> = [];
  listmedicine: Array<any> = [];

  integratedNote: Array<any> = [];

  autocompleteStatusDiagnosis = false;
  searchQueryICDX: any;
  autocompleteStatusAction = false;
  searchQueryICD9: any;
  autocompletestatusmedicine = false;
  searchQueryCategoryMedicine: any;


  statusSaveSoap = false;
  statusReview = false;
  medicine_desc: any;

  // property showhide soap
  showS = true;
  showO = false;
  showA = false;
  showPLab = false;
  showPRad = false;
  showPObat = false;
  showPFisio = false;
  showPTindakan = false;

  // property showhide stylus or keyboard
  stylusModeS = false;
  stylusModeO = false;
  stylusModeA = false;
  stylusModePLab = false;
  stylusModePRad = false;
  stylusModePObat = false;
  stylusModePFisio = false;
  stylusModePTindakan = false;

  // property stroke stylus
  styluss: any;
  styluso: any;
  stylusa: any;
  styluspo: any;
  styluspl: any;
  styluspr: any;
  styluspf: any;
  styluspt: any;

  @Input()
  buttonsubmit: boolean;

  @Input()
  buttonsave: boolean;

  @Input()
  listallergymedicine: Array<any> = [];

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private dokterService: DokterService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage) {

    this.formPemeriksaan = this.formBuilder.group({
      'id_ct': [''],
      'no_medrec': [''],
      'nama_pasien': [''],
      'nodok': [''],
      'nama_dok': ['', Validators.required],
      'no_periksa': [''],
      'jaminan': [''],
      'tgl_lahir': [''],
      'umur': [''],
      'kelamin': [''],
      'kelas': ['RJ'],
      'tgl_ct': [''],
      's_description': [''],
      's_stroke': [''],
      'o_description': [''],
      'o_stroke': [''],
      'a_description': [''],
      'a_stroke': [''],
      'a_icd9': [''],
      'a_icdx': [''],
      'p_lab_description': [''],
      'p_lab_stroke': [''],
      'p_rad_description': [''],
      'p_rad_stroke': [''],
      'p_fisio_description': [''],
      'p_fisio_stroke': [''],
      'p_tindakan_description': [''],
      'p_tindakan_stroke': [''],
      'p_obat_description': [],
      'p_obat_stroke': [''],
      'p_obat_nama': [''],
      'p_obat_alergi': [''],
    });

  }

  ngAfterViewInit() {
    this.slides.lockSwipes(true);
  }

  slidesChanged() {
    let currentIndex = this.slides.getActiveIndex();
    if (currentIndex !== null) {
      this.slides.lockSwipes(true);
    }
  }

  showInformConsent(): boolean {
    if (this.specialist == "Spesialis Bedah Umum" || this.specialist == "Spesialis Anasthesi" ||
      this.specialist == "Bedah Orthopedi" || this.specialist == "Spesialis Mata" ||
      this.specialist == "Spesialis THT" || this.specialist == "Spesialis Kebidanan dan K") {
      return true;
    } else {
      return false;
    }
  }

  /**
   * show hide panel periksa
   *
   * @memberof SOAPPage
   */
  showHideExamine() {
    this.showExamine = !this.showExamine;
  }

  /**
   * show hide card SOAP
   *
   * @param {*} ev
   * @returns
   * @memberof SOAPPage
   */
  showCard(ev: any) {
    switch (ev) {
      case 'S':
        this.showS = true;
        this.showO = false;
        this.showA = false;
        this.showPObat = false;
        this.showPLab = false;
        this.showPRad = false;
        this.showPFisio = false;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(0, 1000);
      case 'O':
        this.showS = false;
        this.showO = true;
        this.showA = false;
        this.showPObat = false;
        this.showPLab = false;
        this.showPRad = false;
        this.showPFisio = false;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(1, 1000);
      case 'A':
        this.showS = false;
        this.showO = false;
        this.showA = true;
        this.showPObat = false;
        this.showPLab = false;
        this.showPRad = false;
        this.showPFisio = false;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(2, 1000);
      case 'P.OBAT':
        this.showS = false;
        this.showO = false;
        this.showA = false;
        this.showPObat = true;
        this.showPLab = false;
        this.showPRad = false;
        this.showPFisio = false;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(3, 1000);
      case 'P.LAB':
        this.showS = false;
        this.showO = false;
        this.showA = false;
        this.showPObat = false;
        this.showPLab = true;
        this.showPRad = false;
        this.showPFisio = false;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(4, 1000);
      case 'P.RAD':
        this.showS = false;
        this.showO = false;
        this.showA = false;
        this.showPObat = false;
        this.showPLab = false;
        this.showPRad = true;
        this.showPFisio = false;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(5, 1000);
      case 'P.FISIO':
        this.showS = false;
        this.showO = false;
        this.showA = false;
        this.showPObat = false;
        this.showPLab = false;
        this.showPRad = false;
        this.showPFisio = true;
        this.showPTindakan = false;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(6, 1000);
      case 'P.TINDAKAN':
        this.showS = false;
        this.showO = false;
        this.showA = false;
        this.showPObat = false;
        this.showPLab = false;
        this.showPRad = false;
        this.showPFisio = false;
        this.showPTindakan = true;
        this.slides.lockSwipes(false);
        return this.slides.slideTo(7, 1000);
    }
  }

  /**
   * template alert message
   *
   * @param {string} title
   * @param {string} message
   * @param {number} flag
   * @memberof SOAPPage
   */
  async alertMessage(title: string, message: string, flag: number) {
    switch (flag) {
      case 0:
        const alert1 = await this.alertCtrl.create({
          title: title,
          subTitle: message,
          buttons: ['OK'],
          enableBackdropDismiss: false
        });
        alert1.present();
        break;
      case 1:
        const alert3 = await this.alertCtrl.create({
          title: title,
          subTitle: message,
          buttons: [{
            text: 'OK',
            handler: () => {
              this.loadNextPatient();
            }
          }],
          enableBackdropDismiss: false
        });
        alert3.present();
        break;
    }
  }

  /**
   * function search data icdx, icd9, obat, alergi
   *
   * @param {any} value
   * @param {any} type
   * @memberof SOAPPage
   */
  searchData(value: any, type: any) {
    const val = value.target.value;
    switch (type) {
      case 'ICDX':
        if (this.searchQueryICDX !== '') {
          this.autocompleteStatusDiagnosis = true;
          this.dokterService.getDataICDX(val, 0, 10).subscribe(({ data }) => {
            this.icdx = [...data.icdxPagination];
          });
          if (val && val.trim() !== '') {
            this.icdx = this.icdx.filter((item) => {
              return (item.penyakit.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
          }
        } else {
          this.autocompleteStatusDiagnosis = false;
        }
        break;
      case 'ICD9':
        if (this.searchQueryICD9 !== '') {
          this.autocompleteStatusAction = true;
          this.dokterService.getDataICD9(val, 0, 10).subscribe(({ data }) => {
            this.icd9 = [...data.icd9Pagination];
          });
          if (val && val.trim() !== '') {
            this.icd9 = this.icd9.filter((item) => {
              return (item.deskripsi.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
          }
        } else {
          this.autocompleteStatusAction = false;
        }
        break;
      case 'OBAT':
        if (this.searchQueryCategoryMedicine !== '') {
          this.autocompletestatusmedicine = true;
          this.dokterService.getDataObat(val, 0, 10).subscribe(({ data }) => {
            this.medicine = [...data.tb001Pagination];
          });
          if (val && val.trim() !== '') {
            this.medicine = this.medicine.filter((item) => {
              return (item.nama.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
          }
        } else {
          this.autocompletestatusmedicine = false;
        }
        break;
    }
  }


  /**
   * add item icdx, icd9, obat, alergi
   *
   * @param {*} val
   * @param {string} status
   * @memberof SOAPPage
   */
  addItem(val: any, status: string) {
    switch (status) {
      case 'ICDX':
        const valicdx = this.icdx[val];
        this.listicd.push(valicdx);
        this.searchQueryICDX = '';
        this.autocompleteStatusDiagnosis = false;
        break;
      case 'ICD9':
        const valicd9 = this.icd9[val];
        this.listicd9.push(valicd9);
        this.searchQueryICD9 = '';
        this.autocompleteStatusAction = false;
        break;
      case 'OBAT':
        const valobat = this.medicine[val];
        this.listmedicine.push(valobat);
        this.searchQueryCategoryMedicine = '';
        this.autocompletestatusmedicine = false;
        break;
    }
  }

  /**
   * remove item icdx, icd9, obat, alergi
   *
   * @param {*} ev
   * @param {string} status
   * @memberof SOAPPage
   */
  removeItem(ev: any, status: string) {
    switch (status) {
      case 'ICDX':
        this.listicd.splice(ev, 1);
        break;
      case 'ICD9':
        this.listicd9.splice(ev, 1);
        break;
      case 'OBAT':
        this.listmedicine.splice(ev, 1);
        this.medicine_desc = 'R/';
        break;
    }
  }

  /**
   * template modal with param icdx
   *
   * @param {*} page
   * @param {*} icdx
   * @memberof SOAPPage
   */
  createModalWithParamsDiag(page: any, icdx: any) {
    const modal = this.modalCtrl.create(page,
      {
        personalNoRM: this.dataPatient[0].no_medrec,
        personalNodok: this.dataPatient[0].nodok,
        doctorName: this.doctorName,
        dataPatient: this.dataPatient,
        icdx: icdx
      }, { enableBackdropDismiss: false });
    modal.present();
  }

  /**
   * switch case modal lab, radiology, physio, inform
   *
   * @param {*} page
   * @returns
   * @memberof SOAPPage
   */
  openModal(page: any) {
    switch (page) {
      case 'Lab':
        return this.createModalWithParamsDiag('Lab', this.listicd);
      case 'Radiology':
        return this.createModalWithParamsDiag('Radiology', this.listicd);
      case 'Physiotherapy':
        return this.createModalWithParamsDiag('Physiotherapy', this.listicd);
      case 'InformConsent':
        if (this.dataPatient[0].no_medrec !== '') {
          return this.navCtrl.push('InformConsent',
            {
              personalNoRM: this.dataPatient[0].no_medrec,
              personalNodok: this.dataPatient[0].nodok,
              doctorName: this.doctorName,
              specialist: this.specialist,
              dataPatient: this.dataPatient
            });
        }
        return this.toastCtrl.create({
          message: 'Belum ada Pasien yang masuk',
          duration: 4000,
          position: 'top'
        }).present();
      case 'Consultation':
        return this.navCtrl.push('Consultation',
          {
            personalNoRM: this.dataPatient[0].no_medrec,
            personalNodok: this.dataPatient[0].nodok,
            dataPatient: this.dataPatient,
            doctorName: this.doctorName
          });
    }
  }

  stylussChange($event: any) {
    this.styluss = $event;
  }

  stylusoChange($event: any) {
    this.styluso = $event;
  }

  stylusaChange($event: any) {
    this.stylusa = $event;
  }

  styluspoChange($event: any) {
    this.styluspo = $event;
  }

  stylusplChange($event: any) {
    this.styluspl = $event;
  }

  stylusprChange($event: any) {
    this.styluspr = $event;
  }

  styluspfChange($event: any) {
    this.styluspf = $event;
  }

  stylusptChange($event: any) {
    this.styluspt = $event;
  }

  addObattoDesc(val: any) {
    this.medicine_desc += ' ' + val.nama + ' ';
  }

  /**
   * append char pattern obat
   *
   * @param {any} val
   * @memberof SOAPPage
   */
  appendChar(val: any) {
    if (this.medicine_desc) {
      if (val === 'R/') {
        this.medicine_desc += '\n' + val + ' ';
      } else if (val === 'S.') {
        this.medicine_desc += '\n     ' + val + ' ';
      } else {
        this.medicine_desc += ' ' + val + ' ';
      }
    } else {
      this.medicine_desc = val + ' ';
    }
  }

  /**
   * submit examination
   *
   * @param {any} formData
   * @param {string} type
   * @memberof SOAPPage
   */
  submitPemeriksaan(formData: any, type: string) {
    console.log(this.dataPatient);
    const loader = this.loadingCtrl.create({
      content: 'Mohon Tunggu...'
    });
    loader.present();
    const datemoment = moment().format();
    const date = new Date(datemoment);
    const year = date.getFullYear();
    let month: any = date.getMonth() + 1;
    let dt: any = date.getDate();
    const hour = date.getHours();
    const min = date.getMinutes();
    const sec = date.getSeconds();
    if (month < 10) {
      month = '0' + month;
    }
    if (dt < 10) {
      dt = '0' + dt;
    }
    const id_ct = year + '' + month + '' + dt + '' + hour + '' + min + '' + sec + '' + this.dataPatient[0].no_medrec;
    const tglct = year + '-' + month + '-' + dt;

    formData.id_ct = id_ct;
    formData.tgl_ct = tglct;
    formData.no_medrec = this.dataPatient[0].no_medrec;
    formData.nodok = this.dataPatient[0].nodok;
    formData.nama_dok = this.dataPatient[0].nama_dok;
    formData.nama_pasien = this.dataPatient[0].nama_pasien;
    formData.no_periksa = this.dataPatient[0].no_periksa;
    formData.jaminan = this.dataPatient[0].jaminan;
    formData.umur = this.dataPatient[0].umur;
    formData.tgl_lahir = this.dataPatient[0].tgl_lahir;
    formData.kelamin = this.dataPatient[0].kelamin;
    formData.s_stroke = this.styluss;
    formData.o_stroke = this.styluso;
    formData.a_stroke = this.stylusa;
    formData.p_obat_stroke = this.styluspo;
    formData.p_lab_stroke = this.styluspl;
    formData.p_rad_stroke = this.styluspr;
    formData.p_fisio_stroke = this.styluspf;
    formData.p_tindakan_stroke = this.styluspt;
    if (this.medicine_desc) {
      formData.p_obat_description = this.medicine_desc;
    } else {
      formData.p_obat_description = '';
    }
    const icdstr = JSON.stringify(this.listicd);
    const icd9str = JSON.stringify(this.listicd9);
    const obatstr = JSON.stringify(this.listmedicine);
    const obatalergistr = JSON.stringify(this.listallergymedicine);
    const parsicdstr = JSON.parse(icdstr);

    formData.a_icdx = icdstr;
    formData.a_icd9 = icd9str;
    formData.p_obat_nama = obatstr;
    formData.p_obat_alergi = obatalergistr;
    formData.penyakit = this.penyakit;

    switch (type) {
      case 'SIMPAN':
        this.dokterService.getCheckICD10(this.dataPatient[0].no_medrec, icdstr).subscribe(({ data }) => {
          const icdxcheck = this.parseDataJSON(data);
          const status = icdxcheck.checkICD10;
          let kasus = 'Baru';
          if (status.length > 0) {
            if (status[0].a_icdx.icd === parsicdstr.icd) {
              kasus = 'Lama';
            } else {
              kasus = 'Baru';
            }
          }
          if (this.statusSaveSoap) {
            this.alertMessage('Simpan Pemeriksaan', 'Anda sudah menyimpan Catatan Terintegrasi '
              + this.dataPatient[0].nama_pasien, 0);
          } else {
            if (this.stylusModeS && this.styluss == null || this.stylusModeO && this.styluso == null ||
              this.stylusModeA && this.stylusa == null || this.stylusModePLab && this.styluspl == null ||
              this.stylusModePRad && this.styluspr == null || this.stylusModePFisio && this.styluspf == null ||
              this.stylusModePObat && this.styluspo == null || this.stylusModePTindakan && this.styluspt == null) {
              let stylusMode = "";
              if (this.stylusModeS && this.styluss == null) { stylusMode = "S" }
              else if (this.stylusModeO && this.styluso == null) { stylusMode = "O" }
              else if (this.stylusModeA && this.stylusa == null) { stylusMode = "A" }
              else if (this.stylusModePLab && this.styluspl == null) { stylusMode = "P.Lab" }
              else if (this.stylusModePRad && this.styluspr == null) { stylusMode = "P.Rad" }
              else if (this.stylusModePFisio && this.styluspf == null) { stylusMode = "P.Fisio" }
              else if (this.stylusModePObat && this.styluspo == null) { stylusMode = "P.Obat" }
              else if (this.stylusModePTindakan && this.styluspt == null) { stylusMode = "P.Tindakan" }
              loader.dismiss();
              this.alertMessage('Stylus SOAP',
                'Mohon untuk klik "Simpan Stylus" terlebih dahulu pada Stylus ' + stylusMode.bold() + ' sebelum menyimpan pemeriksaan', 0);
            } else {
              this.dokterService.saveIntegratedNote(formData).subscribe(({ data }) => {
                this.dokterService.setFlagExamination(this.dataPatient[0].no_medrec, 0).subscribe(({ data }) => {
                  if (this.statusReview === false) {
                    this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SIMPAN');
                  } else {
                    this.updateFlagDiag(formData, kasus, loader, 'KID', 'SIMPAN');
                  }
                });
              });
            }
          }
        });

        break;

      case 'SELESAI':
        const loadData = this.dokterService.loadPatientExamination1(this.dataPatient[0].nodok).subscribe(({ data }) => {
          const loadexamine1 = this.parseDataJSON(data);
          const dataperiksa = loadexamine1.examineLast1;
          if (dataperiksa) {
            loader.dismiss();
            const alert = this.alertCtrl.create({
              title: 'Submit Pemeriksaan ditunda',
              subTitle: 'Hubungi perawat untuk menyelesaikan pemeriksaan pasien sebelumnya',
              buttons: [{
                text: 'OK',
                handler: () => {
                  loadData.unsubscribe();
                }
              }],
              enableBackdropDismiss: false
            });
            alert.present();
          } else {
            if (this.statusSaveSoap) {
              this.dokterService.setFlagExamination(this.dataPatient[0].no_medrec, 1).subscribe(({ data }) => {
                this.dokterService.setFlagPatient(this.dataPatient[0].no_medrec, 4).subscribe(({ data }) => {
                  if (this.statusReview === false) {
                    this.updateInform(loader, 'ADULT');
                  } else {
                    this.updateInform(loader, 'KID');
                  }
                });
              });
            } else {
              this.dokterService.getCheckICD10(this.dataPatient[0].no_medrec, icdstr).subscribe(({ data }) => {
                const icdxcheck = this.parseDataJSON(data);
                const status = icdxcheck.checkICD10;
                let kasus = 'Baru';
                if (status.length > 0) {
                  if (status[0].a_icdx.icd === parsicdstr.icd) {
                    kasus = 'Lama';
                  } else {
                    kasus = 'Baru';
                  }
                }
                if (this.stylusModeS && this.styluss == null || this.stylusModeO && this.styluso == null ||
                  this.stylusModeA && this.stylusa == null || this.stylusModePLab && this.styluspl == null ||
                  this.stylusModePRad && this.styluspr == null || this.stylusModePFisio && this.styluspf == null ||
                  this.stylusModePObat && this.styluspo == null || this.stylusModePTindakan && this.styluspt == null) {
                  let stylusMode = "";
                  if (this.stylusModeS && this.styluss == null) { stylusMode = "S" }
                  else if (this.stylusModeO && this.styluso == null) { stylusMode = "O" }
                  else if (this.stylusModeA && this.stylusa == null) { stylusMode = "A" }
                  else if (this.stylusModePLab && this.styluspl == null) { stylusMode = "P.Lab" }
                  else if (this.stylusModePRad && this.styluspr == null) { stylusMode = "P.Rad" }
                  else if (this.stylusModePFisio && this.styluspf == null) { stylusMode = "P.Fisio" }
                  else if (this.stylusModePObat && this.styluspo == null) { stylusMode = "P.Obat" }
                  else if (this.stylusModePTindakan && this.styluspt == null) { stylusMode = "P.Tindakan" }
                  loader.dismiss();
                  this.alertMessage('Stylus SOAP',
                    'Mohon untuk klik "Simpan Stylus" terlebih dahulu pada Stylus ' + stylusMode.bold() + ' sebelum menyimpan pemeriksaan', 0);
                } else {
                  this.storage.get('simpandata').then((data) => {
                    if (!data) {
                      this.dokterService.saveIntegratedNote(formData).subscribe();
                    }
                    try {
                      this.dokterService.setFlagExamination(this.dataPatient[0].no_medrec, 1).subscribe(({ data }) => {
                        this.storage.remove('simpandata');
                        this.dokterService.setFlagPatient(this.dataPatient[0].no_medrec, 4).subscribe(({ data }) => {
                          if (this.statusReview === false) {
                            this.dokterService.getInformConsent0Query(this.dataPatient[0].no_medrec)
                              .subscribe(({ data }) => {
                                const strinform = this.parseDataJSON(data);
                                const datainform = strinform.informConsent0;
                                if (datainform.length > 0) {
                                  this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(({ data }) => {
                                    this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SELESAI');
                                  });
                                } else {
                                  this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SELESAI');
                                }
                              });
                          } else {
                            this.dokterService.getInformConsent0Query(this.dataPatient[0].no_medrec)
                              .subscribe(({ data }) => {
                                const strinform = this.parseDataJSON(data);
                                const datainform = strinform.informConsent0;
                                if (datainform.length > 0) {
                                  this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(({ data }) => {
                                    this.updateFlagDiag(formData, kasus, loader, 'KID', 'SELESAI');
                                  });
                                } else {
                                  this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SELESAI');
                                }
                              });
                          }
                        });
                      });
                    } catch (e) {
                      // this.navCtrl.setRoot('HomePage');
                    }
                  });
                }
              });
            }
          }
        });
        break;
    }
  }

  /**
   * update flag review and diag examiantion
   *
   * @private
   * @param {*} a_desc
   * @param {*} a_stroke
   * @param {*} kasus
   * @param {*} loader
   * @param {string} type
   * @param {string} status
   * @memberof SOAPPage
   */
  private updateFlagDiag(form: any, kasus: any, loader: any, type: string, status: string) {
    switch (type) {
      case 'ADULT':
        this.dokterService.updateFlagReview(this.dataPatient[0].no_medrec, 0)
          .subscribe(({ data }) => {
            this.dokterService.updateDiagExam(this.dataPatient[0].no_periksa,
              form.a_description, form.a_stroke, kasus).subscribe(({ data }) => {
                if (data) {
                  loader.dismiss();
                  this.statusSaveSoap = true;
                  if (status === 'SELESAI') {
                    this.alertMessage('Pasien Selesai', this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                  } else {
                    this.alertMessage('Simpan Pemeriksaan', 'Catatan Terintegrasi '
                      + this.dataPatient[0].nama_pasien + ' telah disimpan', 0);
                    this.storage.set('simpandata', form);
                  }

                }
              });
          });
        break;
      case 'KID':
        this.dokterService.updateKidFlagReview(this.dataPatient[0].no_medrec, '1').subscribe(({ data }) => {
          this.dokterService.updateDiagExam(this.dataPatient[0].no_medrec,
            form.a_description, form.a_stroke, kasus).subscribe(({ data }) => {
              if (data) {
                loader.dismiss();
                if (status === 'SELESAI') {
                  this.alertMessage('Pasien Selesai', this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                } else {
                  this.alertMessage('Simpan Pemeriksaan', 'Catatan Terintegrasi '
                    + this.dataPatient[0].nama_pasien + ' telah disimpan', 0);
                }
              }
            });
        });
        break;
    }
  }

  /**
   * update flag inform and review
   *
   * @private
   * @param {*} loader
   * @param {string} type
   * @memberof SOAPPage
   */
  private updateInform(loader: any, type: string) {
    switch (type) {
      case 'ADULT':
        this.dokterService.getInformConsent0Query(this.dataPatient[0].no_medrec).subscribe(({ data }) => {
          const strinform = this.parseDataJSON(data);
          const datainform = strinform.informConsent0;
          if (datainform.length > 0) {
            this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(({ data }) => {
              if (data) {
                this.dokterService.updateFlagReview(this.dataPatient[0].no_medrec, 1).subscribe(({ data }) => {
                  if (data) {
                    this.alertMessage('Pasien Selesai', this.dataPatient[0].nama_pasien
                      + ' telah selesai diperiksa', 1);
                    loader.dismiss();
                  }
                });
              }
            });
          } else {
            this.dokterService.updateFlagReview(this.dataPatient[0].no_medrec, 1).subscribe(({ data }) => {
              if (data) {
                this.alertMessage('Pasien Selesai', this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                loader.dismiss();
              }
            });
          }
        });
        break;
      case 'KID':
        this.dokterService.getInformConsent0Query(this.dataPatient[0].no_medrec).subscribe(({ data }) => {
          const strinform = this.parseDataJSON(data);
          const datainform = strinform.informConsent0;
          if (datainform.length > 0) {
            this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(({ data }) => {
              if (data) {
                this.dokterService.updateKidFlagReview(this.dataPatient[0].no_medrec, '1').subscribe(({ data }) => {
                  if (data) {
                    this.alertMessage('Pasien Selesai', this.dataPatient[0].nama_pasien
                      + ' telah selesai diperiksa', 1);
                    loader.dismiss();
                  }
                });
              }
            });
          } else {
            this.dokterService.updateKidFlagReview(this.dataPatient[0].no_medrec, '1').subscribe(({ data }) => {
              if (data) {
                this.alertMessage('Pasien Selesai', this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                loader.dismiss();
              }
            });
          }
        });
        break;
    }
  }

  /**
   * loading for load the next patient
   *
   * @memberof SOAPPage
   */
  async loadNextPatient() {
    const loading = await this.loadingCtrl.create({
      content: '<h3>Silahkan Tunggu Pasien Selanjutnya</h3>',
      duration: 5000
    });
    await loading.onDidDismiss(() => {
      setTimeout(() => {
        this.dataPatient = [];
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
      }, 5000);
    });
    loading.present();
  }

  /**
   * templete generate string to array
   *
   * @param {any} obj
   * @returns
   * @memberof SOAPPage
   */
  generateArray(obj: any) {
    if (obj) {
      return JSON.parse(obj);
    } else {
      return null;
    }
  }

  /**
   * template for parse JSON
   *
   * @private
   * @param {any} data
   * @returns
   * @memberof SOAPPage
   */
  private parseDataJSON(data: any) {
    const strData = JSON.stringify(data);
    const parsData = JSON.parse(strData);
    return parsData;
  }
}
