var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators } from '@angular/forms';
import { DokterService } from './../../services/providers/dokter-service';
import { Component, Input, ViewChild } from '@angular/core';
import { NavController, Slides, ToastController } from 'ionic-angular';
import { AlertController, ModalController, LoadingController } from 'ionic-angular';
import moment from 'moment';
var SOAPPage = /** @class */ (function () {
    function SOAPPage(navCtrl, alertCtrl, modalCtrl, formBuilder, dokterService, loadingCtrl, toastCtrl, storage) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.dokterService = dokterService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.showExamine = true;
        this.icdx = [];
        this.icd9 = [];
        this.medicine = [];
        this.listicd = [];
        this.listicd9 = [];
        this.listmedicine = [];
        this.integratedNote = [];
        this.autocompleteStatusDiagnosis = false;
        this.autocompleteStatusAction = false;
        this.autocompletestatusmedicine = false;
        this.statusSaveSoap = false;
        this.statusReview = false;
        // property showhide soap
        this.showS = true;
        this.showO = false;
        this.showA = false;
        this.showPLab = false;
        this.showPRad = false;
        this.showPObat = false;
        this.showPFisio = false;
        this.showPTindakan = false;
        // property showhide stylus or keyboard
        this.stylusModeS = false;
        this.stylusModeO = false;
        this.stylusModeA = false;
        this.stylusModePLab = false;
        this.stylusModePRad = false;
        this.stylusModePObat = false;
        this.stylusModePFisio = false;
        this.stylusModePTindakan = false;
        this.listallergymedicine = [];
        this.formPemeriksaan = this.formBuilder.group({
            'id_ct': [''],
            'no_medrec': [''],
            'nama_pasien': [''],
            'nodok': [''],
            'nama_dok': ['', Validators.required],
            'no_periksa': [''],
            'jaminan': [''],
            'tgl_lahir': [''],
            'umur': [''],
            'kelamin': [''],
            'kelas': ['RJ'],
            'tgl_ct': [''],
            's_description': [''],
            's_stroke': [''],
            'o_description': [''],
            'o_stroke': [''],
            'a_description': [''],
            'a_stroke': [''],
            'a_icd9': [''],
            'a_icdx': [''],
            'p_lab_description': [''],
            'p_lab_stroke': [''],
            'p_rad_description': [''],
            'p_rad_stroke': [''],
            'p_fisio_description': [''],
            'p_fisio_stroke': [''],
            'p_tindakan_description': [''],
            'p_tindakan_stroke': [''],
            'p_obat_description': [],
            'p_obat_stroke': [''],
            'p_obat_nama': [''],
            'p_obat_alergi': [''],
        });
    }
    SOAPPage.prototype.ngAfterViewInit = function () {
        this.slides.lockSwipes(true);
    };
    SOAPPage.prototype.slidesChanged = function () {
        var currentIndex = this.slides.getActiveIndex();
        if (currentIndex !== null) {
            this.slides.lockSwipes(true);
        }
    };
    SOAPPage.prototype.showInformConsent = function () {
        if (this.specialist == "Spesialis Bedah Umum" || this.specialist == "Spesialis Anasthesi" ||
            this.specialist == "Bedah Orthopedi" || this.specialist == "Spesialis Mata" ||
            this.specialist == "Spesialis THT" || this.specialist == "Spesialis Kebidanan dan K") {
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * show hide panel periksa
     *
     * @memberof SOAPPage
     */
    SOAPPage.prototype.showHideExamine = function () {
        this.showExamine = !this.showExamine;
    };
    /**
     * show hide card SOAP
     *
     * @param {*} ev
     * @returns
     * @memberof SOAPPage
     */
    SOAPPage.prototype.showCard = function (ev) {
        switch (ev) {
            case 'S':
                this.showS = true;
                this.showO = false;
                this.showA = false;
                this.showPObat = false;
                this.showPLab = false;
                this.showPRad = false;
                this.showPFisio = false;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(0, 1000);
            case 'O':
                this.showS = false;
                this.showO = true;
                this.showA = false;
                this.showPObat = false;
                this.showPLab = false;
                this.showPRad = false;
                this.showPFisio = false;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(1, 1000);
            case 'A':
                this.showS = false;
                this.showO = false;
                this.showA = true;
                this.showPObat = false;
                this.showPLab = false;
                this.showPRad = false;
                this.showPFisio = false;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(2, 1000);
            case 'P.OBAT':
                this.showS = false;
                this.showO = false;
                this.showA = false;
                this.showPObat = true;
                this.showPLab = false;
                this.showPRad = false;
                this.showPFisio = false;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(3, 1000);
            case 'P.LAB':
                this.showS = false;
                this.showO = false;
                this.showA = false;
                this.showPObat = false;
                this.showPLab = true;
                this.showPRad = false;
                this.showPFisio = false;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(4, 1000);
            case 'P.RAD':
                this.showS = false;
                this.showO = false;
                this.showA = false;
                this.showPObat = false;
                this.showPLab = false;
                this.showPRad = true;
                this.showPFisio = false;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(5, 1000);
            case 'P.FISIO':
                this.showS = false;
                this.showO = false;
                this.showA = false;
                this.showPObat = false;
                this.showPLab = false;
                this.showPRad = false;
                this.showPFisio = true;
                this.showPTindakan = false;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(6, 1000);
            case 'P.TINDAKAN':
                this.showS = false;
                this.showO = false;
                this.showA = false;
                this.showPObat = false;
                this.showPLab = false;
                this.showPRad = false;
                this.showPFisio = false;
                this.showPTindakan = true;
                this.slides.lockSwipes(false);
                return this.slides.slideTo(7, 1000);
        }
    };
    /**
     * template alert message
     *
     * @param {string} title
     * @param {string} message
     * @param {number} flag
     * @memberof SOAPPage
     */
    SOAPPage.prototype.alertMessage = function (title, message, flag) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, alert1, alert3;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = flag;
                        switch (_a) {
                            case 0: return [3 /*break*/, 1];
                            case 1: return [3 /*break*/, 3];
                        }
                        return [3 /*break*/, 5];
                    case 1: return [4 /*yield*/, this.alertCtrl.create({
                            title: title,
                            subTitle: message,
                            buttons: ['OK'],
                            enableBackdropDismiss: false
                        })];
                    case 2:
                        alert1 = _b.sent();
                        alert1.present();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.alertCtrl.create({
                            title: title,
                            subTitle: message,
                            buttons: [{
                                    text: 'OK',
                                    handler: function () {
                                        _this.loadNextPatient();
                                    }
                                }],
                            enableBackdropDismiss: false
                        })];
                    case 4:
                        alert3 = _b.sent();
                        alert3.present();
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * function search data icdx, icd9, obat, alergi
     *
     * @param {any} value
     * @param {any} type
     * @memberof SOAPPage
     */
    SOAPPage.prototype.searchData = function (value, type) {
        var _this = this;
        var val = value.target.value;
        switch (type) {
            case 'ICDX':
                if (this.searchQueryICDX !== '') {
                    this.autocompleteStatusDiagnosis = true;
                    this.dokterService.getDataICDX(val, 0, 10).subscribe(function (_a) {
                        var data = _a.data;
                        _this.icdx = data.icdxPagination.slice();
                    });
                    if (val && val.trim() !== '') {
                        this.icdx = this.icdx.filter(function (item) {
                            return (item.penyakit.toLowerCase().indexOf(val.toLowerCase()) > -1);
                        });
                    }
                }
                else {
                    this.autocompleteStatusDiagnosis = false;
                }
                break;
            case 'ICD9':
                if (this.searchQueryICD9 !== '') {
                    this.autocompleteStatusAction = true;
                    this.dokterService.getDataICD9(val, 0, 10).subscribe(function (_a) {
                        var data = _a.data;
                        _this.icd9 = data.icd9Pagination.slice();
                    });
                    if (val && val.trim() !== '') {
                        this.icd9 = this.icd9.filter(function (item) {
                            return (item.deskripsi.toLowerCase().indexOf(val.toLowerCase()) > -1);
                        });
                    }
                }
                else {
                    this.autocompleteStatusAction = false;
                }
                break;
            case 'OBAT':
                if (this.searchQueryCategoryMedicine !== '') {
                    this.autocompletestatusmedicine = true;
                    this.dokterService.getDataObat(val, 0, 10).subscribe(function (_a) {
                        var data = _a.data;
                        _this.medicine = data.tb001Pagination.slice();
                    });
                    if (val && val.trim() !== '') {
                        this.medicine = this.medicine.filter(function (item) {
                            return (item.nama.toLowerCase().indexOf(val.toLowerCase()) > -1);
                        });
                    }
                }
                else {
                    this.autocompletestatusmedicine = false;
                }
                break;
        }
    };
    /**
     * add item icdx, icd9, obat, alergi
     *
     * @param {*} val
     * @param {string} status
     * @memberof SOAPPage
     */
    SOAPPage.prototype.addItem = function (val, status) {
        switch (status) {
            case 'ICDX':
                var valicdx = this.icdx[val];
                this.listicd.push(valicdx);
                this.searchQueryICDX = '';
                this.autocompleteStatusDiagnosis = false;
                break;
            case 'ICD9':
                var valicd9 = this.icd9[val];
                this.listicd9.push(valicd9);
                this.searchQueryICD9 = '';
                this.autocompleteStatusAction = false;
                break;
            case 'OBAT':
                var valobat = this.medicine[val];
                this.listmedicine.push(valobat);
                this.searchQueryCategoryMedicine = '';
                this.autocompletestatusmedicine = false;
                break;
        }
    };
    /**
     * remove item icdx, icd9, obat, alergi
     *
     * @param {*} ev
     * @param {string} status
     * @memberof SOAPPage
     */
    SOAPPage.prototype.removeItem = function (ev, status) {
        switch (status) {
            case 'ICDX':
                this.listicd.splice(ev, 1);
                break;
            case 'ICD9':
                this.listicd9.splice(ev, 1);
                break;
            case 'OBAT':
                this.listmedicine.splice(ev, 1);
                this.medicine_desc = 'R/';
                break;
        }
    };
    /**
     * template modal with param icdx
     *
     * @param {*} page
     * @param {*} icdx
     * @memberof SOAPPage
     */
    SOAPPage.prototype.createModalWithParamsDiag = function (page, icdx) {
        var modal = this.modalCtrl.create(page, {
            personalNoRM: this.dataPatient[0].no_medrec,
            personalNodok: this.dataPatient[0].nodok,
            doctorName: this.doctorName,
            dataPatient: this.dataPatient,
            icdx: icdx
        }, { enableBackdropDismiss: false });
        modal.present();
    };
    /**
     * switch case modal lab, radiology, physio, inform
     *
     * @param {*} page
     * @returns
     * @memberof SOAPPage
     */
    SOAPPage.prototype.openModal = function (page) {
        switch (page) {
            case 'Lab':
                return this.createModalWithParamsDiag('Lab', this.listicd);
            case 'Radiology':
                return this.createModalWithParamsDiag('Radiology', this.listicd);
            case 'Physiotherapy':
                return this.createModalWithParamsDiag('Physiotherapy', this.listicd);
            case 'InformConsent':
                if (this.dataPatient[0].no_medrec !== '') {
                    return this.navCtrl.push('InformConsent', {
                        personalNoRM: this.dataPatient[0].no_medrec,
                        personalNodok: this.dataPatient[0].nodok,
                        doctorName: this.doctorName,
                        specialist: this.specialist,
                        dataPatient: this.dataPatient
                    });
                }
                return this.toastCtrl.create({
                    message: 'Belum ada Pasien yang masuk',
                    duration: 4000,
                    position: 'top'
                }).present();
            case 'Consultation':
                return this.navCtrl.push('Consultation', {
                    personalNoRM: this.dataPatient[0].no_medrec,
                    personalNodok: this.dataPatient[0].nodok,
                    dataPatient: this.dataPatient,
                    doctorName: this.doctorName
                });
        }
    };
    SOAPPage.prototype.stylussChange = function ($event) {
        this.styluss = $event;
    };
    SOAPPage.prototype.stylusoChange = function ($event) {
        this.styluso = $event;
    };
    SOAPPage.prototype.stylusaChange = function ($event) {
        this.stylusa = $event;
    };
    SOAPPage.prototype.styluspoChange = function ($event) {
        this.styluspo = $event;
    };
    SOAPPage.prototype.stylusplChange = function ($event) {
        this.styluspl = $event;
    };
    SOAPPage.prototype.stylusprChange = function ($event) {
        this.styluspr = $event;
    };
    SOAPPage.prototype.styluspfChange = function ($event) {
        this.styluspf = $event;
    };
    SOAPPage.prototype.stylusptChange = function ($event) {
        this.styluspt = $event;
    };
    SOAPPage.prototype.addObattoDesc = function (val) {
        this.medicine_desc += ' ' + val.nama + ' ';
    };
    /**
     * append char pattern obat
     *
     * @param {any} val
     * @memberof SOAPPage
     */
    SOAPPage.prototype.appendChar = function (val) {
        if (this.medicine_desc) {
            if (val === 'R/') {
                this.medicine_desc += '\n' + val + ' ';
            }
            else if (val === 'S.') {
                this.medicine_desc += '\n     ' + val + ' ';
            }
            else {
                this.medicine_desc += ' ' + val + ' ';
            }
        }
        else {
            this.medicine_desc = val + ' ';
        }
    };
    /**
     * submit examination
     *
     * @param {any} formData
     * @param {string} type
     * @memberof SOAPPage
     */
    SOAPPage.prototype.submitPemeriksaan = function (formData, type) {
        var _this = this;
        console.log(this.dataPatient);
        var loader = this.loadingCtrl.create({
            content: 'Mohon Tunggu...'
        });
        loader.present();
        var datemoment = moment().format();
        var date = new Date(datemoment);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var dt = date.getDate();
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();
        if (month < 10) {
            month = '0' + month;
        }
        if (dt < 10) {
            dt = '0' + dt;
        }
        var id_ct = year + '' + month + '' + dt + '' + hour + '' + min + '' + sec + '' + this.dataPatient[0].no_medrec;
        var tglct = year + '-' + month + '-' + dt;
        formData.id_ct = id_ct;
        formData.tgl_ct = tglct;
        formData.no_medrec = this.dataPatient[0].no_medrec;
        formData.nodok = this.dataPatient[0].nodok;
        formData.nama_dok = this.dataPatient[0].nama_dok;
        formData.nama_pasien = this.dataPatient[0].nama_pasien;
        formData.no_periksa = this.dataPatient[0].no_periksa;
        formData.jaminan = this.dataPatient[0].jaminan;
        formData.umur = this.dataPatient[0].umur;
        formData.tgl_lahir = this.dataPatient[0].tgl_lahir;
        formData.kelamin = this.dataPatient[0].kelamin;
        formData.s_stroke = this.styluss;
        formData.o_stroke = this.styluso;
        formData.a_stroke = this.stylusa;
        formData.p_obat_stroke = this.styluspo;
        formData.p_lab_stroke = this.styluspl;
        formData.p_rad_stroke = this.styluspr;
        formData.p_fisio_stroke = this.styluspf;
        formData.p_tindakan_stroke = this.styluspt;
        if (this.medicine_desc) {
            formData.p_obat_description = this.medicine_desc;
        }
        else {
            formData.p_obat_description = '';
        }
        var icdstr = JSON.stringify(this.listicd);
        var icd9str = JSON.stringify(this.listicd9);
        var obatstr = JSON.stringify(this.listmedicine);
        var obatalergistr = JSON.stringify(this.listallergymedicine);
        var parsicdstr = JSON.parse(icdstr);
        formData.a_icdx = icdstr;
        formData.a_icd9 = icd9str;
        formData.p_obat_nama = obatstr;
        formData.p_obat_alergi = obatalergistr;
        formData.penyakit = this.penyakit;
        switch (type) {
            case 'SIMPAN':
                this.dokterService.getCheckICD10(this.dataPatient[0].no_medrec, icdstr).subscribe(function (_a) {
                    var data = _a.data;
                    var icdxcheck = _this.parseDataJSON(data);
                    var status = icdxcheck.checkICD10;
                    var kasus = 'Baru';
                    if (status.length > 0) {
                        if (status[0].a_icdx.icd === parsicdstr.icd) {
                            kasus = 'Lama';
                        }
                        else {
                            kasus = 'Baru';
                        }
                    }
                    if (_this.statusSaveSoap) {
                        _this.alertMessage('Simpan Pemeriksaan', 'Anda sudah menyimpan Catatan Terintegrasi '
                            + _this.dataPatient[0].nama_pasien, 0);
                    }
                    else {
                        if (_this.stylusModeS && _this.styluss == null || _this.stylusModeO && _this.styluso == null ||
                            _this.stylusModeA && _this.stylusa == null || _this.stylusModePLab && _this.styluspl == null ||
                            _this.stylusModePRad && _this.styluspr == null || _this.stylusModePFisio && _this.styluspf == null ||
                            _this.stylusModePObat && _this.styluspo == null || _this.stylusModePTindakan && _this.styluspt == null) {
                            var stylusMode = "";
                            if (_this.stylusModeS && _this.styluss == null) {
                                stylusMode = "S";
                            }
                            else if (_this.stylusModeO && _this.styluso == null) {
                                stylusMode = "O";
                            }
                            else if (_this.stylusModeA && _this.stylusa == null) {
                                stylusMode = "A";
                            }
                            else if (_this.stylusModePLab && _this.styluspl == null) {
                                stylusMode = "P.Lab";
                            }
                            else if (_this.stylusModePRad && _this.styluspr == null) {
                                stylusMode = "P.Rad";
                            }
                            else if (_this.stylusModePFisio && _this.styluspf == null) {
                                stylusMode = "P.Fisio";
                            }
                            else if (_this.stylusModePObat && _this.styluspo == null) {
                                stylusMode = "P.Obat";
                            }
                            else if (_this.stylusModePTindakan && _this.styluspt == null) {
                                stylusMode = "P.Tindakan";
                            }
                            loader.dismiss();
                            _this.alertMessage('Stylus SOAP', 'Mohon untuk klik "Simpan Stylus" terlebih dahulu pada Stylus ' + stylusMode.bold() + ' sebelum menyimpan pemeriksaan', 0);
                        }
                        else {
                            _this.dokterService.saveIntegratedNote(formData).subscribe(function (_a) {
                                var data = _a.data;
                                _this.dokterService.setFlagExamination(_this.dataPatient[0].no_medrec, 0).subscribe(function (_a) {
                                    var data = _a.data;
                                    if (_this.statusReview === false) {
                                        _this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SIMPAN');
                                    }
                                    else {
                                        _this.updateFlagDiag(formData, kasus, loader, 'KID', 'SIMPAN');
                                    }
                                });
                            });
                        }
                    }
                });
                break;
            case 'SELESAI':
                var loadData_1 = this.dokterService.loadPatientExamination1(this.dataPatient[0].nodok).subscribe(function (_a) {
                    var data = _a.data;
                    var loadexamine1 = _this.parseDataJSON(data);
                    var dataperiksa = loadexamine1.examineLast1;
                    if (dataperiksa) {
                        loader.dismiss();
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Submit Pemeriksaan ditunda',
                            subTitle: 'Hubungi perawat untuk menyelesaikan pemeriksaan pasien sebelumnya',
                            buttons: [{
                                    text: 'OK',
                                    handler: function () {
                                        loadData_1.unsubscribe();
                                    }
                                }],
                            enableBackdropDismiss: false
                        });
                        alert_1.present();
                    }
                    else {
                        if (_this.statusSaveSoap) {
                            _this.dokterService.setFlagExamination(_this.dataPatient[0].no_medrec, 1).subscribe(function (_a) {
                                var data = _a.data;
                                _this.dokterService.setFlagPatient(_this.dataPatient[0].no_medrec, 4).subscribe(function (_a) {
                                    var data = _a.data;
                                    if (_this.statusReview === false) {
                                        _this.updateInform(loader, 'ADULT');
                                    }
                                    else {
                                        _this.updateInform(loader, 'KID');
                                    }
                                });
                            });
                        }
                        else {
                            _this.dokterService.getCheckICD10(_this.dataPatient[0].no_medrec, icdstr).subscribe(function (_a) {
                                var data = _a.data;
                                var icdxcheck = _this.parseDataJSON(data);
                                var status = icdxcheck.checkICD10;
                                var kasus = 'Baru';
                                if (status.length > 0) {
                                    if (status[0].a_icdx.icd === parsicdstr.icd) {
                                        kasus = 'Lama';
                                    }
                                    else {
                                        kasus = 'Baru';
                                    }
                                }
                                if (_this.stylusModeS && _this.styluss == null || _this.stylusModeO && _this.styluso == null ||
                                    _this.stylusModeA && _this.stylusa == null || _this.stylusModePLab && _this.styluspl == null ||
                                    _this.stylusModePRad && _this.styluspr == null || _this.stylusModePFisio && _this.styluspf == null ||
                                    _this.stylusModePObat && _this.styluspo == null || _this.stylusModePTindakan && _this.styluspt == null) {
                                    var stylusMode = "";
                                    if (_this.stylusModeS && _this.styluss == null) {
                                        stylusMode = "S";
                                    }
                                    else if (_this.stylusModeO && _this.styluso == null) {
                                        stylusMode = "O";
                                    }
                                    else if (_this.stylusModeA && _this.stylusa == null) {
                                        stylusMode = "A";
                                    }
                                    else if (_this.stylusModePLab && _this.styluspl == null) {
                                        stylusMode = "P.Lab";
                                    }
                                    else if (_this.stylusModePRad && _this.styluspr == null) {
                                        stylusMode = "P.Rad";
                                    }
                                    else if (_this.stylusModePFisio && _this.styluspf == null) {
                                        stylusMode = "P.Fisio";
                                    }
                                    else if (_this.stylusModePObat && _this.styluspo == null) {
                                        stylusMode = "P.Obat";
                                    }
                                    else if (_this.stylusModePTindakan && _this.styluspt == null) {
                                        stylusMode = "P.Tindakan";
                                    }
                                    loader.dismiss();
                                    _this.alertMessage('Stylus SOAP', 'Mohon untuk klik "Simpan Stylus" terlebih dahulu pada Stylus ' + stylusMode.bold() + ' sebelum menyimpan pemeriksaan', 0);
                                }
                                else {
                                    _this.storage.get('simpandata').then(function (data) {
                                        if (!data) {
                                            _this.dokterService.saveIntegratedNote(formData).subscribe();
                                        }
                                        try {
                                            _this.dokterService.setFlagExamination(_this.dataPatient[0].no_medrec, 1).subscribe(function (_a) {
                                                var data = _a.data;
                                                _this.storage.remove('simpandata');
                                                _this.dokterService.setFlagPatient(_this.dataPatient[0].no_medrec, 4).subscribe(function (_a) {
                                                    var data = _a.data;
                                                    if (_this.statusReview === false) {
                                                        _this.dokterService.getInformConsent0Query(_this.dataPatient[0].no_medrec)
                                                            .subscribe(function (_a) {
                                                            var data = _a.data;
                                                            var strinform = _this.parseDataJSON(data);
                                                            var datainform = strinform.informConsent0;
                                                            if (datainform.length > 0) {
                                                                _this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(function (_a) {
                                                                    var data = _a.data;
                                                                    _this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SELESAI');
                                                                });
                                                            }
                                                            else {
                                                                _this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SELESAI');
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        _this.dokterService.getInformConsent0Query(_this.dataPatient[0].no_medrec)
                                                            .subscribe(function (_a) {
                                                            var data = _a.data;
                                                            var strinform = _this.parseDataJSON(data);
                                                            var datainform = strinform.informConsent0;
                                                            if (datainform.length > 0) {
                                                                _this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(function (_a) {
                                                                    var data = _a.data;
                                                                    _this.updateFlagDiag(formData, kasus, loader, 'KID', 'SELESAI');
                                                                });
                                                            }
                                                            else {
                                                                _this.updateFlagDiag(formData, kasus, loader, 'ADULT', 'SELESAI');
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                        catch (e) {
                                            // this.navCtrl.setRoot('HomePage');
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
                break;
        }
    };
    /**
     * update flag review and diag examiantion
     *
     * @private
     * @param {*} a_desc
     * @param {*} a_stroke
     * @param {*} kasus
     * @param {*} loader
     * @param {string} type
     * @param {string} status
     * @memberof SOAPPage
     */
    SOAPPage.prototype.updateFlagDiag = function (form, kasus, loader, type, status) {
        var _this = this;
        switch (type) {
            case 'ADULT':
                this.dokterService.updateFlagReview(this.dataPatient[0].no_medrec, 0)
                    .subscribe(function (_a) {
                    var data = _a.data;
                    _this.dokterService.updateDiagExam(_this.dataPatient[0].no_periksa, form.a_description, form.a_stroke, kasus).subscribe(function (_a) {
                        var data = _a.data;
                        if (data) {
                            loader.dismiss();
                            _this.statusSaveSoap = true;
                            if (status === 'SELESAI') {
                                _this.alertMessage('Pasien Selesai', _this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                            }
                            else {
                                _this.alertMessage('Simpan Pemeriksaan', 'Catatan Terintegrasi '
                                    + _this.dataPatient[0].nama_pasien + ' telah disimpan', 0);
                                _this.storage.set('simpandata', form);
                            }
                        }
                    });
                });
                break;
            case 'KID':
                this.dokterService.updateKidFlagReview(this.dataPatient[0].no_medrec, '1').subscribe(function (_a) {
                    var data = _a.data;
                    _this.dokterService.updateDiagExam(_this.dataPatient[0].no_medrec, form.a_description, form.a_stroke, kasus).subscribe(function (_a) {
                        var data = _a.data;
                        if (data) {
                            loader.dismiss();
                            if (status === 'SELESAI') {
                                _this.alertMessage('Pasien Selesai', _this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                            }
                            else {
                                _this.alertMessage('Simpan Pemeriksaan', 'Catatan Terintegrasi '
                                    + _this.dataPatient[0].nama_pasien + ' telah disimpan', 0);
                            }
                        }
                    });
                });
                break;
        }
    };
    /**
     * update flag inform and review
     *
     * @private
     * @param {*} loader
     * @param {string} type
     * @memberof SOAPPage
     */
    SOAPPage.prototype.updateInform = function (loader, type) {
        var _this = this;
        switch (type) {
            case 'ADULT':
                this.dokterService.getInformConsent0Query(this.dataPatient[0].no_medrec).subscribe(function (_a) {
                    var data = _a.data;
                    var strinform = _this.parseDataJSON(data);
                    var datainform = strinform.informConsent0;
                    if (datainform.length > 0) {
                        _this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(function (_a) {
                            var data = _a.data;
                            if (data) {
                                _this.dokterService.updateFlagReview(_this.dataPatient[0].no_medrec, 1).subscribe(function (_a) {
                                    var data = _a.data;
                                    if (data) {
                                        _this.alertMessage('Pasien Selesai', _this.dataPatient[0].nama_pasien
                                            + ' telah selesai diperiksa', 1);
                                        loader.dismiss();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        _this.dokterService.updateFlagReview(_this.dataPatient[0].no_medrec, 1).subscribe(function (_a) {
                            var data = _a.data;
                            if (data) {
                                _this.alertMessage('Pasien Selesai', _this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                                loader.dismiss();
                            }
                        });
                    }
                });
                break;
            case 'KID':
                this.dokterService.getInformConsent0Query(this.dataPatient[0].no_medrec).subscribe(function (_a) {
                    var data = _a.data;
                    var strinform = _this.parseDataJSON(data);
                    var datainform = strinform.informConsent0;
                    if (datainform.length > 0) {
                        _this.dokterService.updateFlagInform(datainform[0].no_inform, 1).subscribe(function (_a) {
                            var data = _a.data;
                            if (data) {
                                _this.dokterService.updateKidFlagReview(_this.dataPatient[0].no_medrec, '1').subscribe(function (_a) {
                                    var data = _a.data;
                                    if (data) {
                                        _this.alertMessage('Pasien Selesai', _this.dataPatient[0].nama_pasien
                                            + ' telah selesai diperiksa', 1);
                                        loader.dismiss();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        _this.dokterService.updateKidFlagReview(_this.dataPatient[0].no_medrec, '1').subscribe(function (_a) {
                            var data = _a.data;
                            if (data) {
                                _this.alertMessage('Pasien Selesai', _this.dataPatient[0].nama_pasien + ' telah selesai diperiksa', 1);
                                loader.dismiss();
                            }
                        });
                    }
                });
                break;
        }
    };
    /**
     * loading for load the next patient
     *
     * @memberof SOAPPage
     */
    SOAPPage.prototype.loadNextPatient = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            content: '<h3>Silahkan Tunggu Pasien Selanjutnya</h3>',
                            duration: 5000
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.onDidDismiss(function () {
                                setTimeout(function () {
                                    _this.dataPatient = [];
                                    _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                                }, 5000);
                            })];
                    case 2:
                        _a.sent();
                        loading.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * templete generate string to array
     *
     * @param {any} obj
     * @returns
     * @memberof SOAPPage
     */
    SOAPPage.prototype.generateArray = function (obj) {
        if (obj) {
            return JSON.parse(obj);
        }
        else {
            return null;
        }
    };
    /**
     * template for parse JSON
     *
     * @private
     * @param {any} data
     * @returns
     * @memberof SOAPPage
     */
    SOAPPage.prototype.parseDataJSON = function (data) {
        var strData = JSON.stringify(data);
        var parsData = JSON.parse(strData);
        return parsData;
    };
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], SOAPPage.prototype, "slides", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], SOAPPage.prototype, "dataPatient", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], SOAPPage.prototype, "doctorName", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], SOAPPage.prototype, "penyakit", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], SOAPPage.prototype, "specialist", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], SOAPPage.prototype, "buttonsubmit", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Boolean)
    ], SOAPPage.prototype, "buttonsave", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], SOAPPage.prototype, "listallergymedicine", void 0);
    SOAPPage = __decorate([
        Component({
            selector: 'soap-section',
            templateUrl: 'soap.component.html'
        }),
        __metadata("design:paramtypes", [NavController,
            AlertController,
            ModalController,
            FormBuilder,
            DokterService,
            LoadingController,
            ToastController,
            Storage])
    ], SOAPPage);
    return SOAPPage;
}());
export { SOAPPage };
//# sourceMappingURL=soap.component.js.map