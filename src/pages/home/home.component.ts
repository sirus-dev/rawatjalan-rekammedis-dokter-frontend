import { Storage } from '@ionic/storage';
import { DokterService } from './../../services/providers/dokter-service';
import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, IonicPage, LoadingController } from 'ionic-angular';
import { AlertController, PopoverController } from 'ionic-angular';
import moment from 'moment';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.component.html'
})
export class HomePage implements OnInit {

  dataPatient: Array<any> = [];
  dataReview: Array<any> = [];
  dataKidReview: Array<any> = [];
  integratedNoted: Array<any> = [];
  dieases: Array<any> = [];
  listallergymedicine: Array<any> = [];

  statusAlergi = false;
  statusDiseas = false;
  reviewStatus = false;

  doctorName: string;
  nodok: string;
  specialist: string;

  penyakit: string;

  alert = 0;
  toast = 0;

  buttonsubmit = false;
  buttonsave = false;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    private dokterService: DokterService,
    public toastCtrl: ToastController,
    private storage: Storage,
    private menuCtrl: MenuController) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.dataPatient = [];
    this.storage.get('nodok').then((data) => {
      this.nodok = data;
      this.dokterService.getNotificationPatientSub(data).subscribe((data) => {
        const datanotif = data.examineDataExchange;
        console.log(datanotif);
        this.loadData(datanotif.nodok);
        this.refreshData;
      });
      this.loadData(data);
    });
  }

  loadData(data: any) {
    const loadData = this.dokterService.notificationDataPatient(data)
      .subscribe(({ data }) => {
        // get doctor name
        const datadoctor = this.parseDataJSON(data);
        const doctorname = datadoctor.doctorByNodok;
        this.doctorName = doctorname.nama_dok;
        this.specialist = doctorname.specialis;
        // get notification patient
        const notifdata = [data.examineLast];
        if (notifdata[0] == null) {
          loadData.unsubscribe();
          if (this.alert === 0) {
            this.alert = 1;
            let loader = this.loadingCtrl.create({
              spinner: 'hide',
              content: "Belum ada Pasien. Silahkan klik Pembaruan Data diatas",
              duration: 6000
            });
            loader.present();
            this.dataPatient = [{ nama_pasien: '', no_medrec: '', jaminan: '', umur: '', tgl_lahir: '', kelamin: '' }];
          }
        } else {
          this.toastMessage(notifdata[0].nama_pasien);
          this.dataPatient = notifdata;
          if (this.dataPatient) {
            this.buttonsave = true;
            this.buttonsubmit = true;
          }
          this.dokterService.getDataInformationPatient(notifdata[0].no_medrec)
            .subscribe(({ data }) => {
              // get data ICDX and Alergi
              const datactpasien = [...data.ctByMedrec];
              const ctdata = this.parseDataJSON(datactpasien);
              this.integratedNoted = ctdata;
              this.dieases = ctdata;
              if (datactpasien) {
                this.statusAlergi = true;
                this.statusDiseas = true;
              }
              // get data review
              const datareview = this.parseDataJSON(data);
              const reviewdata = datareview.reviewByMedrec;
              if (reviewdata) {
                this.reviewStatus = false;
                this.dataReview = [reviewdata];
              } else {
                this.dokterService.getKidReviewByMedrec(notifdata[0].no_medrec)
                  .subscribe(({ data }) => {
                    const reviewstrkid = this.parseDataJSON(data);
                    const reviewdatakid = reviewstrkid.kidReviewByMedrec;
                    if (reviewdatakid) {
                      this.reviewStatus = true;
                      this.dataKidReview = [reviewdatakid];
                    }
                  });
              }
            });
        }
      });
  }

  /**
   * Distribute data allergy's patient to information component
   * 
   * @param {any} $event 
   * @memberof HomePage
   */
  alergyChange($event) {
    this.listallergymedicine = $event;
    console.log(this.listallergymedicine);
  }

  /**
   * Distribute data disaese's patient to information component
   * 
   * @param {any} $event 
   * @memberof HomePage
   */
  diseaseChange($event) {
    this.penyakit = $event;
  }

  /**
   * show consultation modal
   *
   * @param {*} page
   * @returns
   * @memberof HomePage
   */
  openModal(page: any) {
    if (this.dataPatient[0].no_medrec !== '') {
      return this.navCtrl.push(page,
        {
          personalNoRM: this.dataPatient[0].no_medrec,
          personalNodok: this.dataPatient[0].nodok,
          dataPatient: this.dataPatient,
          doctorName: this.doctorName
        });
    }
    return this.toastCtrl.create({
      message: 'Belum ada Pasien yang masuk',
      duration: 4000,
      position: 'top'
    }).present();
  }

  /**
   * template toast message
   *
   * @private
   * @param {string} message
   * @memberof HomePage
   */
  private toastMessage(message: string) {
    if (this.toast === 0) {
      this.toast = 1;
      this.toastCtrl.create({
        message: 'Pasien ' + message + ' akan segera diperiksa',
        duration: 6000,
        position: 'top'
      }).present();
    }
  }

  /**
   * template parse JSON
   *
   * @private
   * @param {*} data
   * @returns
   * @memberof HomePage
   */
  private parseDataJSON(data: any) {
    const strData = JSON.stringify(data);
    const parsData = JSON.parse(strData);
    return parsData;
  }

  /**
   * Show popover detail notification
   *
   * @private
   * @memberof HomePage
   */
  private presentPopover() {
    this.popoverCtrl.create(PopupInformationComponent).present();
  }

  /**
   * function refresh page
   *
   * @memberof HomePage
   */
  refreshData() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }
}

@Component({
  template: `
  <ion-list>
  <ion-list-header>
    Legend Notification
  </ion-list-header>
  <ion-item><ion-icon name="flask"></ion-icon> = Laboratorium</ion-item>
  <ion-item><ion-icon name="pulse"></ion-icon> = Radiologi</ion-item>
  <ion-item><ion-icon name="medkit"></ion-icon> = Resep dan Obat</ion-item>
  <ion-item><ion-icon name="body"></ion-icon> = Fisioterapi</ion-item>
  <ion-item><ion-icon name="nutrition"></ion-icon> = Gizi</ion-item>
  <ion-item><ion-icon name="medical"></ion-icon> = Dan Lain-lain</ion-item>
  <ion-item><ion-icon name="mail"></ion-icon> = Jawaban Konsultasi</ion-item>
  </ion-list>`
})

export class PopupInformationComponent { }
