var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { SOAPPage } from './soap.component';
import { InformationPage } from './information.component';
import { StylusSignature } from './../stylus-signature/stylus-signature';
import { HomePage } from './home.component';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SirusStylusModule } from '@sirus/stylus';
import { StylusSirus } from '../../pages/stylus/stylussirus';
var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        NgModule({
            declarations: [
                HomePage,
                StylusSirus,
                StylusSignature,
                InformationPage,
                SOAPPage
            ],
            imports: [
                IonicPageModule.forChild(HomePage),
                SirusStylusModule.forRoot('22eda92c-10af-40d8-abea-fd4093c17d81', 'a1fa759f-b3ce-4091-9fd4-d34bb870c601', 'webdemoapi.myscript.com')
            ],
            exports: [
                StylusSirus,
                StylusSignature
            ],
            entryComponents: [
                StylusSirus
            ]
        })
    ], HomeModule);
    return HomeModule;
}());
export { HomeModule };
//# sourceMappingURL=home.component.module.js.map