import { Storage } from '@ionic/storage';
import { DokterService } from './../../services/providers/dokter-service';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { NavController, ToastController, IonicPage } from 'ionic-angular';
import { AlertController, ModalController, PopoverController, LoadingController } from 'ionic-angular';
import moment from 'moment';
import { Patient } from '../../models/patient.model';
import { Review, KidReview } from '../../models/review.model';

interface Alergy {
  nama: string;
  satuan: string;
}

@Component({
  selector: 'information-section',
  templateUrl: 'information.component.html'
})
export class InformationPage {

  @Input()
  dataPatient: Array<any>;

  @Input()
  dataReview: Array<any>;

  @Input()
  dataKidReview: Array<any>;

  @Input()
  statusAlergi = false;

  @Input()
  statusDiseas = false;

  @Input()
  reviewStatus = false;

  @Input()
  doctorName: string;

  @Input()
  nodok: string;

  @Input()
  integratedNoted: Array<any>;

  @Input()
  dieases: Array<any>;

  showInfo = true;
  
  allergyMedicine: Array<any> = [];

  @Output() listallergy = new EventEmitter();

  @Output() penyakit = new EventEmitter();

  disease: any;
  
  listDisease: Array<any> = [];

  listallergymedicine: Array<any> = [];

  autocompletestatusallergymedicine = false;
  searchQueryCategoryAllergyMedicine: any;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              public toastCtrl: ToastController,
              private dokterService: DokterService) {}

  /**
   * show hide information panel
   *
   * @memberof InformationPage
   */
  showHideInfo(){
    this.showInfo = !this.showInfo;
  }

    /**
   * template parse array from array string
   * 
   * @param {any} obj 
   * @returns 
   * @memberof History
   */
  generateArray(obj) {
    if (obj) {
      return JSON.parse(obj);
    } else {
      return null;
    }
  }

  /**
   * template mapping date with moment
   *
   * @private
   * @param {string} date
   * @returns {string}
   * @memberof InformationPage
   */
  private mapToMoment(date: string) {
    if (moment(date).isValid()) {
      return moment(date).format('DD/MM/YYYY');
    }
    return '';
  }

  /**
   * tempalate modal
   *
   * @param {*} page
   * @returns
   * @memberof InformationPage
   */
  openModal(page: any) {
    if(this.dataPatient[0].no_medrec !== ''){
      return this.navCtrl.push(page,
        { personalNoRM: this.dataPatient[0].no_medrec,
          personalNodok: this.dataPatient[0].nodok,
          dataReview: this.dataReview,
          dataKidReview: this.dataKidReview,
          doctorName: this.doctorName,
          dataPatient: this.dataPatient,
          nodok: this.nodok });
    }
    return this.toastCtrl.create({
      message: 'Belum ada Pasien yang masuk',
      duration: 4000,
      position: 'top'
    }).present();
  }

  saveDisease(){
    this.toastCtrl.create({
      message: 'Riwayat Penyakit Berhasil Disimpan',
      duration: 3000,
      position: 'top'
    }).present();
    if(this.dieases == null){
      this.dieases = [{penyakit:[{riwayat:this.disease}]}];
    }
    let diseaseHistory = this.dieases;
    let newDisease = JSON.stringify([{riwayat:this.disease}]);
    diseaseHistory.push({penyakit:newDisease});
    this.dieases = diseaseHistory;
    this.listDisease.push({riwayat:this.disease});
    this.penyakit.emit(JSON.stringify(this.listDisease));

    this.disease = "";
  }

  searchData(value: any) {
    const val = value.target.value;
      if (this.searchQueryCategoryAllergyMedicine !== '') {
        this.autocompletestatusallergymedicine = true;
        this.dokterService.getDataObat(val, 0, 10).subscribe(({ data }) => {
          this.allergyMedicine = [...data.tb001Pagination];
        });
        if (val && val.trim() !== '') {
          this.allergyMedicine = this.allergyMedicine.filter((item) => {
            return (item.nama.toLowerCase().indexOf(val.toLowerCase()) > -1);
          });
        }
      } else {
        this.autocompletestatusallergymedicine = false;
      }
  }

  addItem(val: any) {
    const addAllergy = this.allergyMedicine[val];
    const valalergi = JSON.stringify([this.allergyMedicine[val]]);
    if(this.integratedNoted == null){
      this.integratedNoted = [];
    }
    this.listallergymedicine.push(addAllergy);
    this.searchQueryCategoryAllergyMedicine = '';
    this.autocompletestatusallergymedicine = false;
    this.listallergy.emit(this.listallergymedicine);
  }

  removeItem(ev: any) {
      console.log(this.listallergymedicine);
      let allergy = this.listallergymedicine;
      allergy.splice(ev, 1);
      this.listallergymedicine = allergy;
  }
}
