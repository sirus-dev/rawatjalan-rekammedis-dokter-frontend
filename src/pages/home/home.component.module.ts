import { SOAPPage } from './soap.component';
import { InformationPage } from './information.component';
import { StylusSignature } from './../stylus-signature/stylus-signature';
import { HomePage } from './home.component';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SirusStylusModule } from '@sirus/stylus';
import { StylusSirus } from '../../pages/stylus/stylussirus';

@NgModule({
  declarations: [
    HomePage,
    StylusSirus,
    StylusSignature,
    InformationPage,
    SOAPPage
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    SirusStylusModule.forRoot(
      '22eda92c-10af-40d8-abea-fd4093c17d81',
      'a1fa759f-b3ce-4091-9fd4-d34bb870c601',
       'webdemoapi.myscript.com'
    )
  ],
  exports: [
    StylusSirus,
    StylusSignature
  ],
  entryComponents: [
    StylusSirus
  ]
})
export class HomeModule {}
