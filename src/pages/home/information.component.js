var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DokterService } from './../../services/providers/dokter-service';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AlertController, ModalController } from 'ionic-angular';
import moment from 'moment';
var InformationPage = /** @class */ (function () {
    function InformationPage(navCtrl, alertCtrl, modalCtrl, toastCtrl, dokterService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.dokterService = dokterService;
        this.statusAlergi = false;
        this.statusDiseas = false;
        this.reviewStatus = false;
        this.showInfo = true;
        this.allergyMedicine = [];
        this.listallergy = new EventEmitter();
        this.penyakit = new EventEmitter();
        this.listDisease = [];
        this.listallergymedicine = [];
        this.autocompletestatusallergymedicine = false;
    }
    /**
     * show hide information panel
     *
     * @memberof InformationPage
     */
    InformationPage.prototype.showHideInfo = function () {
        this.showInfo = !this.showInfo;
    };
    /**
   * template parse array from array string
   *
   * @param {any} obj
   * @returns
   * @memberof History
   */
    InformationPage.prototype.generateArray = function (obj) {
        if (obj) {
            return JSON.parse(obj);
        }
        else {
            return null;
        }
    };
    /**
     * template mapping date with moment
     *
     * @private
     * @param {string} date
     * @returns {string}
     * @memberof InformationPage
     */
    InformationPage.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date).format('DD/MM/YYYY');
        }
        return '';
    };
    /**
     * tempalate modal
     *
     * @param {*} page
     * @returns
     * @memberof InformationPage
     */
    InformationPage.prototype.openModal = function (page) {
        if (this.dataPatient[0].no_medrec !== '') {
            return this.navCtrl.push(page, { personalNoRM: this.dataPatient[0].no_medrec,
                personalNodok: this.dataPatient[0].nodok,
                dataReview: this.dataReview,
                dataKidReview: this.dataKidReview,
                doctorName: this.doctorName,
                dataPatient: this.dataPatient,
                nodok: this.nodok });
        }
        return this.toastCtrl.create({
            message: 'Belum ada Pasien yang masuk',
            duration: 4000,
            position: 'top'
        }).present();
    };
    InformationPage.prototype.saveDisease = function () {
        this.toastCtrl.create({
            message: 'Riwayat Penyakit Berhasil Disimpan',
            duration: 3000,
            position: 'top'
        }).present();
        if (this.dieases == null) {
            this.dieases = [{ penyakit: [{ riwayat: this.disease }] }];
        }
        var diseaseHistory = this.dieases;
        var newDisease = JSON.stringify([{ riwayat: this.disease }]);
        diseaseHistory.push({ penyakit: newDisease });
        this.dieases = diseaseHistory;
        this.listDisease.push({ riwayat: this.disease });
        this.penyakit.emit(JSON.stringify(this.listDisease));
        this.disease = "";
    };
    InformationPage.prototype.searchData = function (value) {
        var _this = this;
        var val = value.target.value;
        if (this.searchQueryCategoryAllergyMedicine !== '') {
            this.autocompletestatusallergymedicine = true;
            this.dokterService.getDataObat(val, 0, 10).subscribe(function (_a) {
                var data = _a.data;
                _this.allergyMedicine = data.tb001Pagination.slice();
            });
            if (val && val.trim() !== '') {
                this.allergyMedicine = this.allergyMedicine.filter(function (item) {
                    return (item.nama.toLowerCase().indexOf(val.toLowerCase()) > -1);
                });
            }
        }
        else {
            this.autocompletestatusallergymedicine = false;
        }
    };
    InformationPage.prototype.addItem = function (val) {
        var addAllergy = this.allergyMedicine[val];
        var valalergi = JSON.stringify([this.allergyMedicine[val]]);
        if (this.integratedNoted == null) {
            this.integratedNoted = [];
        }
        this.listallergymedicine.push(addAllergy);
        this.searchQueryCategoryAllergyMedicine = '';
        this.autocompletestatusallergymedicine = false;
        this.listallergy.emit(this.listallergymedicine);
    };
    InformationPage.prototype.removeItem = function (ev) {
        console.log(this.listallergymedicine);
        var allergy = this.listallergymedicine;
        allergy.splice(ev, 1);
        this.listallergymedicine = allergy;
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], InformationPage.prototype, "dataPatient", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], InformationPage.prototype, "dataReview", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], InformationPage.prototype, "dataKidReview", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], InformationPage.prototype, "statusAlergi", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], InformationPage.prototype, "statusDiseas", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], InformationPage.prototype, "reviewStatus", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], InformationPage.prototype, "doctorName", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], InformationPage.prototype, "nodok", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], InformationPage.prototype, "integratedNoted", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], InformationPage.prototype, "dieases", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], InformationPage.prototype, "listallergy", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], InformationPage.prototype, "penyakit", void 0);
    InformationPage = __decorate([
        Component({
            selector: 'information-section',
            templateUrl: 'information.component.html'
        }),
        __metadata("design:paramtypes", [NavController,
            AlertController,
            ModalController,
            ToastController,
            DokterService])
    ], InformationPage);
    return InformationPage;
}());
export { InformationPage };
//# sourceMappingURL=information.component.js.map