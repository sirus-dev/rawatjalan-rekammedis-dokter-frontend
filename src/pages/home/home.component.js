var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Storage } from '@ionic/storage';
import { DokterService } from './../../services/providers/dokter-service';
import { Component } from '@angular/core';
import { NavController, ToastController, IonicPage, LoadingController } from 'ionic-angular';
import { AlertController, PopoverController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, alertCtrl, loadingCtrl, popoverCtrl, dokterService, toastCtrl, storage, menuCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.popoverCtrl = popoverCtrl;
        this.dokterService = dokterService;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.menuCtrl = menuCtrl;
        this.dataPatient = [];
        this.dataReview = [];
        this.dataKidReview = [];
        this.integratedNoted = [];
        this.dieases = [];
        this.listallergymedicine = [];
        this.statusAlergi = false;
        this.statusDiseas = false;
        this.reviewStatus = false;
        this.alert = 0;
        this.toast = 0;
        this.buttonsubmit = false;
        this.buttonsave = false;
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.menuCtrl.enable(true);
        this.dataPatient = [];
        this.storage.get('nodok').then(function (data) {
            _this.nodok = data;
            _this.dokterService.getNotificationPatientSub(data).subscribe(function (data) {
                var datanotif = data.examineDataExchange;
                console.log(datanotif);
                _this.loadData(datanotif.nodok);
                _this.refreshData;
            });
            _this.loadData(data);
        });
    };
    HomePage.prototype.loadData = function (data) {
        var _this = this;
        var loadData = this.dokterService.notificationDataPatient(data)
            .subscribe(function (_a) {
            var data = _a.data;
            // get doctor name
            var datadoctor = _this.parseDataJSON(data);
            var doctorname = datadoctor.doctorByNodok;
            _this.doctorName = doctorname.nama_dok;
            _this.specialist = doctorname.specialis;
            // get notification patient
            var notifdata = [data.examineLast];
            if (notifdata[0] == null) {
                loadData.unsubscribe();
                if (_this.alert === 0) {
                    _this.alert = 1;
                    var loader = _this.loadingCtrl.create({
                        spinner: 'hide',
                        content: "Belum ada Pasien. Silahkan klik Pembaruan Data diatas",
                        duration: 6000
                    });
                    loader.present();
                    _this.dataPatient = [{ nama_pasien: '', no_medrec: '', jaminan: '', umur: '', tgl_lahir: '', kelamin: '' }];
                }
            }
            else {
                _this.toastMessage(notifdata[0].nama_pasien);
                _this.dataPatient = notifdata;
                if (_this.dataPatient) {
                    _this.buttonsave = true;
                    _this.buttonsubmit = true;
                }
                _this.dokterService.getDataInformationPatient(notifdata[0].no_medrec)
                    .subscribe(function (_a) {
                    var data = _a.data;
                    // get data ICDX and Alergi
                    var datactpasien = data.ctByMedrec.slice();
                    var ctdata = _this.parseDataJSON(datactpasien);
                    _this.integratedNoted = ctdata;
                    _this.dieases = ctdata;
                    if (datactpasien) {
                        _this.statusAlergi = true;
                        _this.statusDiseas = true;
                    }
                    // get data review
                    var datareview = _this.parseDataJSON(data);
                    var reviewdata = datareview.reviewByMedrec;
                    if (reviewdata) {
                        _this.reviewStatus = false;
                        _this.dataReview = [reviewdata];
                    }
                    else {
                        _this.dokterService.getKidReviewByMedrec(notifdata[0].no_medrec)
                            .subscribe(function (_a) {
                            var data = _a.data;
                            var reviewstrkid = _this.parseDataJSON(data);
                            var reviewdatakid = reviewstrkid.kidReviewByMedrec;
                            if (reviewdatakid) {
                                _this.reviewStatus = true;
                                _this.dataKidReview = [reviewdatakid];
                            }
                        });
                    }
                });
            }
        });
    };
    /**
     * Distribute data allergy's patient to information component
     *
     * @param {any} $event
     * @memberof HomePage
     */
    HomePage.prototype.alergyChange = function ($event) {
        this.listallergymedicine = $event;
        console.log(this.listallergymedicine);
    };
    /**
     * Distribute data disaese's patient to information component
     *
     * @param {any} $event
     * @memberof HomePage
     */
    HomePage.prototype.diseaseChange = function ($event) {
        this.penyakit = $event;
    };
    /**
     * show consultation modal
     *
     * @param {*} page
     * @returns
     * @memberof HomePage
     */
    HomePage.prototype.openModal = function (page) {
        if (this.dataPatient[0].no_medrec !== '') {
            return this.navCtrl.push(page, {
                personalNoRM: this.dataPatient[0].no_medrec,
                personalNodok: this.dataPatient[0].nodok,
                dataPatient: this.dataPatient,
                doctorName: this.doctorName
            });
        }
        return this.toastCtrl.create({
            message: 'Belum ada Pasien yang masuk',
            duration: 4000,
            position: 'top'
        }).present();
    };
    /**
     * template toast message
     *
     * @private
     * @param {string} message
     * @memberof HomePage
     */
    HomePage.prototype.toastMessage = function (message) {
        if (this.toast === 0) {
            this.toast = 1;
            this.toastCtrl.create({
                message: 'Pasien ' + message + ' akan segera diperiksa',
                duration: 6000,
                position: 'top'
            }).present();
        }
    };
    /**
     * template parse JSON
     *
     * @private
     * @param {*} data
     * @returns
     * @memberof HomePage
     */
    HomePage.prototype.parseDataJSON = function (data) {
        var strData = JSON.stringify(data);
        var parsData = JSON.parse(strData);
        return parsData;
    };
    /**
     * Show popover detail notification
     *
     * @private
     * @memberof HomePage
     */
    HomePage.prototype.presentPopover = function () {
        this.popoverCtrl.create(PopupInformationComponent).present();
    };
    /**
     * function refresh page
     *
     * @memberof HomePage
     */
    HomePage.prototype.refreshData = function () {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
    };
    HomePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-home',
            templateUrl: 'home.component.html'
        }),
        __metadata("design:paramtypes", [NavController,
            AlertController,
            LoadingController,
            PopoverController,
            DokterService,
            ToastController,
            Storage,
            MenuController])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
var PopupInformationComponent = /** @class */ (function () {
    function PopupInformationComponent() {
    }
    PopupInformationComponent = __decorate([
        Component({
            template: "\n  <ion-list>\n  <ion-list-header>\n    Legend Notification\n  </ion-list-header>\n  <ion-item><ion-icon name=\"flask\"></ion-icon> = Laboratorium</ion-item>\n  <ion-item><ion-icon name=\"pulse\"></ion-icon> = Radiologi</ion-item>\n  <ion-item><ion-icon name=\"medkit\"></ion-icon> = Resep dan Obat</ion-item>\n  <ion-item><ion-icon name=\"body\"></ion-icon> = Fisioterapi</ion-item>\n  <ion-item><ion-icon name=\"nutrition\"></ion-icon> = Gizi</ion-item>\n  <ion-item><ion-icon name=\"medical\"></ion-icon> = Dan Lain-lain</ion-item>\n  <ion-item><ion-icon name=\"mail\"></ion-icon> = Jawaban Konsultasi</ion-item>\n  </ion-list>"
        })
    ], PopupInformationComponent);
    return PopupInformationComponent;
}());
export { PopupInformationComponent };
//# sourceMappingURL=home.component.js.map